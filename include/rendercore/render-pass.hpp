//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_render_pass_hpp_
#define _rendercore_render_pass_hpp_

#include <cstdint>
#include <rendercore/data-format.hpp>

namespace rendercore {

// Most of this is taken straight from the Vulkan 1.0 specification; I don't know
// of it makes any sense to go into these details.

enum class AttachmentLoadOp {
	Load,
	Clear,
	DontCare
};

enum class AttachmentStoreOp {
	Store,
	DontCare
};

struct AttachmentDescription {
	NumberFormat numFormat;
	PixelFormat pxFormat;
	AttachmentLoadOp loadOp;
	AttachmentStoreOp storeOp;
	AttachmentLoadOp stencilLoadOp;
	AttachmentStoreOp stencilStoreOp;
};

enum class PipelineBindPoint {
	Graphics,
	Compute
};

struct AttachmentReference {
	uint32_t attachment;
	ImageLayout layout;
};

struct SubpassDescription {
	PipelineBindPoint pipelineBindPoint;
	uint32_t inputAttachmentCount;
	const AttachmentReference* inputAttachments;
	uint32_t colorAttachmentCount;
	const AttachmentReference* colorAttachments;
	// TODO: resolve attachments?
	const AttachmentReference* depthStencilAttachment;
	// TODO: preserve attachments?
};

enum DependencyFlagBits {
	DEPENDENCY_BY_REGION_BIT = 0x00000001
};

enum PipelineStageFlagBits {
	PIPELINE_STAGE_TOP_OF_PIPE_BIT = 0x00000001,
	PIPELINE_STAGE_DRAW_INDIRECT_BIT = 0x00000002,
	PIPELINE_STAGE_VERTEX_INPUT_BIT = 0x00000004,
	PIPELINE_STAGE_VERTEX_SHADER_BIT = 0x00000008,
	PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT = 0x00000010,
	PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT = 0x00000020,
	PIPELINE_STAGE_GEOMETRY_SHADER_BIT = 0x00000040,
	PIPELINE_STAGE_FRAGMENT_SHADER_BIT = 0x00000080,
	PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT = 0x00000100,
	PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT = 0x00000200,
	PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT = 0x00000400,
	PIPELINE_STAGE_COMPUTE_SHADER_BIT = 0x00000800,
	PIPELINE_STAGE_TRANSFER_BIT = 0x00001000,
	PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT = 0x00002000,
	PIPELINE_STAGE_HOST_BIT = 0x00004000,
	PIPELINE_STAGE_ALL_GRAPHICS_BIT = 0x00008000,
	PIPELINE_STAGE_ALL_COMMANDS_BIT = 0x00010000,
};

enum AccessFlagBits {
	ACCESS_INDIRECT_COMMAND_READ_BIT = 0x00000001,
	ACCESS_INDEX_READ_BIT = 0x00000002,
	ACCESS_VERTEX_ATTRIBUTE_READ_BIT = 0x00000004,
	ACCESS_UNIFORM_READ_BIT = 0x00000008,
	ACCESS_INPUT_ATTACHMENT_READ_BIT = 0x00000010,
	ACCESS_SHADER_READ_BIT = 0x00000020,
	ACCESS_SHADER_WRITE_BIT = 0x00000040,
	ACCESS_COLOR_ATTACHMENT_READ_BIT = 0x00000080,
	ACCESS_COLOR_ATTACHMENT_WRITE_BIT = 0x00000100,
	ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT = 0x00000200,
	ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT = 0x00000400,
	ACCESS_TRANSFER_READ_BIT = 0x00000800,
	ACCESS_TRANSFER_WRITE_BIT = 0x00001000,
	ACCESS_HOST_READ_BIT = 0x00002000,
	ACCESS_HOST_WRITE_BIT = 0x00004000,
	ACCESS_MEMORY_READ_BIT = 0x00008000,
	ACCESS_MEMORY_WRITE_BIT = 0x00010000,
};

struct SubpassDependency {
	uint32_t srcSubpass;
	uint32_t dstSubpass;
	int srcStageMask;//!< Must be at least one of PipelineStageFlagBits.
	int dstStageMask; //!< Must be at least one of PipelineStageFlagBits.
	int srcAccessMask; //!< Must be a valid combination of AccessFlagBits.
	int dstAccessMask; //!< Must be a valid combination of AccessFlagBits.
	int dependencyFlags; //!< Must be a valid combination of DependencyFlagBits.
};

struct RenderPassCreateInfo {
	uint32_t attachmentCount;
	const AttachmentDescription* attachments;
	uint32_t subpassCount;
	const SubpassDescription* subpasses;
	uint32_t dependencyCount;
	const SubpassDependency* dependencies;
};

class RenderPass
{};

} // namespace rendercore

#endif // _rendercore_render_pass_hpp_
