//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_rendering_pipeline_hpp_
#define _rendercore_gl4_rendering_pipeline_hpp_

#include <rendercore/rendering-pipeline.hpp>
#include <string>
#include <stdexcept>
#include <GL/gl3w.h>
#include <rendercore/vertex-stream.hpp>

namespace rendercore {

class GL4ProgramLinkException : public std::runtime_error
{
public:
	GL4ProgramLinkException(const std::string& log);
	std::string getBuildLog() const;

private:
	std::string m_log;
};

class GL4RenderDispatcher;

class GL4RenderingPipeline : public RenderingPipeline
{
public:
	GL4RenderingPipeline();
	GL4RenderingPipeline(const RenderingPipelineCreateInfo& createInfo, GL4RenderDispatcher& dispatcher);
	~GL4RenderingPipeline();

	GL4RenderingPipeline(const GL4RenderingPipeline&) = delete;
	GL4RenderingPipeline& operator=(const GL4RenderingPipeline&) = delete;

	GL4RenderingPipeline(GL4RenderingPipeline&& other);
	GL4RenderingPipeline& operator=(GL4RenderingPipeline&& other);

	void release();

	void bind() const;
	void setDrawBuffers(bool targetIsDefault) const;

private:
	GLuint m_program;
	GL4RenderDispatcher* m_dispatcher;

public: // for access from inside rendercore
	struct GLBlendInfo {
		bool enable;
		GLenum srcBlendColor;
		GLenum destBlendColor;
		GLenum blendEqnColor;
		GLenum srcBlendAlpha;
		GLenum destBlendAlpha;
		GLenum blendEqnAlpha;
	};

	struct GLPipelineInfo {
		GLuint vao;
		GLenum logicOp;
		bool enableTarget[kPipelineMaxColorTargets];
		uint8_t colorMask[kPipelineMaxColorTargets];
		GLsizei bindPointStrides[kMaxVertexMemoryBindPoints];

		// tessellation state
		GLint patchControlPoints;

		// viewport state
		PipelineViewportStateCreateInfo viewportState;

		// rasterizer state
		GLenum fillMode;
		bool enableCulling;
		GLenum cullMode;
		GLenum frontFace;

		// multisample state
		bool enableMultisample;

		// depth-stencil state
		bool enableDepthTest;
		GLboolean enableDepthWriting;
		GLenum depthFunc;
		bool enableStencilTest;
		GLenum frontSFunc;
		GLint frontSRef;
		GLenum backSFunc;
		GLint backSRef;
		GLuint stencilReadMask;
		GLuint stencilWriteMask;
		GLenum frontSFail;
		GLenum frontDPFail;
		GLenum frontDPPass;
		GLenum backSFail;
		GLenum backDPFail;
		GLenum backDPPass;

		// color blend state
		float blendColor[4];
		GLBlendInfo blendInfo[kPipelineMaxColorTargets];

		GLenum mode;
	} m_pipelineInfo;

public:
	inline bool empty() const
	{
		return m_program == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_rendering_pipeline_hpp_
