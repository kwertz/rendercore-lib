//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_platform_surface_hpp_
#define _rendercore_platform_surface_hpp_

#ifdef RENDERCORE_USE_PLATFORM_XLIB
#include <X11/Xlib.h>
#endif // RENDERCORE_USE_PLATFORM_XLIB

#ifdef RENDERCORE_USE_PLATFORM_WIN32
#include <windows.h>
#endif // RENDERCORE_USE_PLATFORM_WIN32

namespace rendercore {

union PlatformSurfaceReference {
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	struct {
		Display* display;
		Window window;
	} xlib;
#endif // RENDERCORE_USE_PLATFORM_XLIB
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	struct {
		HINSTANCE hinstance;
		HWND hwnd;
	} win32;
#endif
	int pad; // can't have an empty union
};

} // namespace rendercore

#endif // _rendercore_platform_hpp_
