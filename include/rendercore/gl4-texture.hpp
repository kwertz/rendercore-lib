//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_texture_hpp_
#define _rendercore_gl4_texture_hpp_

#include <cstddef>
#include <rendercore/texture.hpp>
#include <GL/gl3w.h>

namespace rendercore {

class GL4RenderDispatcher;

GLenum translateCubemapFace(CubemapFace face);

class GL4Texture : public Texture
{
public:
	GL4Texture();
	GL4Texture(const TextureCreateInfo& createInfo, GL4RenderDispatcher& dispatcher);
	~GL4Texture();

	GL4Texture(const GL4Texture&) = delete;
	GL4Texture& operator=(const GL4Texture&) = delete;

	GL4Texture(GL4Texture&& other);
	GL4Texture& operator=(GL4Texture&& other);

	virtual void update1D(uint32_t mip, int offset, uint32_t size, size_t dataSize, const void* data, size_t rowStride) override;
	virtual void update1DArray(uint32_t mip, uint32_t arrayIndex, int offset, uint32_t size, size_t dataSize, const void* data, size_t rowStride) override;
	virtual void update2D(uint32_t mip, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) override;
	virtual void update2DArray(uint32_t mip, uint32_t arrayIndex, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) override;
	virtual void update3D(uint32_t mip, int offsetX, int offsetY, int offsetZ, uint32_t width, uint32_t height, uint32_t depth, size_t dataSize, const void* data, size_t rowStride) override;
	virtual void updateCubemapFace(uint32_t mip, CubemapFace face, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) override;
	virtual void updateCubemapArrayFace(uint32_t mip, uint32_t arrayIndex, CubemapFace face, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) override;

	void release();

	inline void bind(GLuint unit) const
	{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(m_bindInfo.target, m_texture);
	}

	inline GLuint getTexture() const
	{
		return m_texture;
	}

	inline GLenum getBindingTarget() const
	{
		return m_bindInfo.target;
	}

private:
	GLuint m_texture;
	GL4RenderDispatcher* m_dispatcher;

	struct GLTextureUpdateInfo {
		GLenum format;
		GLenum dataType;
		uint32_t mipLevels;
		TextureSize size;
		size_t pixelSizeBytes;
		bool isCompressed;
	} m_updateInfo;

	struct GLTextureBindInfo {
		GLenum target;
	} m_bindInfo;

public:
	inline bool empty() const
	{
		return m_texture == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_texture_hpp_
