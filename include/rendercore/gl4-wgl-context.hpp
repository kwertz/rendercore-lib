//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_wgl_context_hpp_
#define _rendercore_gl4_wgl_context_hpp_

#include <memory>
#include <rendercore/platform.hpp>
#include <rendercore/rendering-api.hpp>
#include <rendercore/gl4-context.hpp>
#include <m0tools/shared-library.hpp>

// winnt.h forward declarations
typedef struct HDC__* HDC;
typedef struct HGLRC__* HGLRC;

namespace rendercore {

struct WGLContextInfo {
	HDC hdc;
	HGLRC hglrc;
	int interval;
};

struct FramebufferSettings;
struct RenderingAPICreateInfo;

void initializeWGL();
void terminateWGL();
void createContextWGL(WGLContextInfo& ctx, std::shared_ptr<Swapchain>& swapchain, const RenderingAPICreateInfo& createInfo);
bool analyzeContextWGL(WGLContextInfo& ctx);
void destroyContextWGL(WGLContextInfo& ctx);

} // namespace rendercore

#endif // _rendercore_gl4_wgl_context_hpp_
