//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_render_output_buffer_hpp_
#define _rendercore_gl4_render_output_buffer_hpp_

#include <rendercore/render-output-buffer.hpp>
#include <GL/gl3w.h>

namespace rendercore {

class GL4RenderOutputBuffer : public RenderOutputBuffer
{
public:
	GL4RenderOutputBuffer();
	GL4RenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo);
	~GL4RenderOutputBuffer();

	GL4RenderOutputBuffer(const GL4RenderOutputBuffer&) = delete;
	GL4RenderOutputBuffer& operator=(const GL4RenderOutputBuffer&) = delete;

	GL4RenderOutputBuffer(GL4RenderOutputBuffer&& other);
	GL4RenderOutputBuffer& operator=(GL4RenderOutputBuffer&& other);

	void release();

	inline void bind() const
	{
		glBindRenderbuffer(GL_RENDERBUFFER, m_renderbuffer);
	}

	inline GLuint getRenderbuffer() const
	{
		return m_renderbuffer;
	}

private:
	GLuint m_renderbuffer;

public:
	inline bool empty() const
	{
		return m_renderbuffer == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_render_output_buffer_hpp_
