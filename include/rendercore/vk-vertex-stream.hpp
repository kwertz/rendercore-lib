//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_vk_vertex_stream_hpp_
#define _rendercore_vk_vertex_stream_hpp_

#include <rendercore/vertex-stream.hpp>
#include <array>
#include <rendercore/vk-functions.hpp>

namespace rendercore {

class VulkanVertexStream : public VertexStream
{
public:
	VulkanVertexStream();
	VulkanVertexStream(const VertexStreamCreateInfo& createInfo);
	~VulkanVertexStream();

	VulkanVertexStream(const VulkanVertexStream&) = delete;
	VulkanVertexStream& operator=(const VulkanVertexStream&) = delete;

	VulkanVertexStream(VulkanVertexStream&& other);
	VulkanVertexStream& operator=(VulkanVertexStream&& other);

public:
	inline bool empty() const
	{
		return m_vmemViewInfoCount == 0;
	}

	void release();

private:
	std::array<VertexMemoryViewInfo, kMaxVertexMemoryBindPoints> m_vmemViewInfos;
	uint32_t m_vmemViewInfoCount;
	IndexMemoryViewInfo m_imemViewInfo;
};

} // namespace rendercore

#endif // _rendercore_vk_vertex_stream_hpp_
