//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_data_format_hpp_
#define _rendercore_data_format_hpp_

#include <cstddef>

namespace rendercore {

enum class NumberFormat {
	Undefined,
	UnsignedNorm8,
	UnsignedNorm16,
	UnsignedNorm32,
	SignedNorm8,
	SignedNorm16,
	SignedNorm32,
	UnsignedInt8,
	UnsignedInt16,
	UnsignedInt32,
	SignedInt8,
	SignedInt16,
	SignedInt32,
	Float,
	sRGB,
	DepthStencil
};

//! Returns the size of one element of the underlying type represented by the given NumberFormat in basic machine units. 0 means the size is undefined.
inline size_t getNumberFormatByteSize(NumberFormat numFmt)
{
	switch (numFmt) {
		case NumberFormat::Undefined:
		case NumberFormat::DepthStencil:
			return 0;
		case NumberFormat::UnsignedNorm8:
		case NumberFormat::SignedNorm8:
		case NumberFormat::UnsignedInt8:
		case NumberFormat::SignedInt8:
			return 1;
		case NumberFormat::UnsignedNorm16:
		case NumberFormat::SignedNorm16:
		case NumberFormat::UnsignedInt16:
		case NumberFormat::SignedInt16:
			return 2;
		case NumberFormat::UnsignedNorm32:
		case NumberFormat::SignedNorm32:
		case NumberFormat::UnsignedInt32:
		case NumberFormat::SignedInt32:
			return 4;
		case NumberFormat::Float:
			return sizeof(float);
		case NumberFormat::sRGB:
			return 1;
	}
	return 0;
}

enum class PixelFormat {
	Red,
	RG,
	RGB,
	BGR,
	RGBA,
	BGRA,
	StencilIndex,
	DepthComponent,
	DepthStencil,
	// compressed formats
	RGB_DXT1,  //!< RGB S3TC DXT1 compressed pixel format
	RGBA_DXT1, //!< RGBA S3TC DXT1 compressed pixel format
	RGBA_DXT3, //!< RGBA S3TC DXT3 compressed pixel format
	RGBA_DXT5,  //!< RGBA S3TC DXT5 compressed pixel format
	Red_RGTC1, //!< Red RGTC1 unsigned compressed pixel format
	Red_RGTC1_Signed, //!< Red RGTC1 signed compressed pixel format
	RG_RGTC2, //!< RG RGTC2 unsigned compressed pixel format
	RG_RGTC2_Signed, //!< RG RGTC2 signed compressed pixel format
	Luminance_LATC1, //!< Luminance (R=G=B=L) LATC1 unsigned compressed pixel format
	Luminance_LATC1_Signed, //!< Luminance (R=G=B=L) LATC1 signed compressed pixel format
	LA_LATC2, //!< LA (R=G=B=L, A=A) LATC1 unsigned compressed pixel format
	LA_LATC2_Signed //!< LA (R=G=B=L, A=A) LATC1 signed compressed pixel format
};

//! Returns the number of components in the given PixelFormat.
inline size_t getNumPixelFormatComponents(PixelFormat pixFmt)
{
	switch (pixFmt) {
		case PixelFormat::Red:
			return 1;
		case PixelFormat::RG:
			return 2;
		case PixelFormat::RGB:
		case PixelFormat::BGR:
			return 3;
		case PixelFormat::RGBA:
		case PixelFormat::BGRA:
			return 4;
		case PixelFormat::StencilIndex:
			return 1;
		case PixelFormat::DepthComponent:
			return 1;
		case PixelFormat::DepthStencil:
			return 2;
		case PixelFormat::RGB_DXT1:
			return 3;
		case PixelFormat::RGBA_DXT1:
		case PixelFormat::RGBA_DXT3:
		case PixelFormat::RGBA_DXT5:
			return 4;
		case PixelFormat::Red_RGTC1:
		case PixelFormat::Red_RGTC1_Signed:
			return 1;
		case PixelFormat::RG_RGTC2:
		case PixelFormat::RG_RGTC2_Signed:
			return 2;
		case PixelFormat::Luminance_LATC1:
		case PixelFormat::Luminance_LATC1_Signed:
			return 1;
		case PixelFormat::LA_LATC2:
		case PixelFormat::LA_LATC2_Signed:
			return 2;
	}
	return 0;
}

enum class ImageLayout {
	Undefined,
	General,
	ColorAttachmentOptimal,
	DepthStencilAttachmentOptimal,
	DepthStencilReadOnlyOptimal,
	ShaderReadOnlyOptimal,
	TransferSrcOptimal,
	TransferDstOptimal,
	Preinitialized
};

} // namespace rendercore
#endif // _rendercore_data_format_hpp_
