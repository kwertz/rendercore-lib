//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_texture_hpp_
#define _rendercore_texture_hpp_

#include <cstdint>
#include <cstddef>
#include <type_traits>
#include <rendercore/data-format.hpp>

namespace rendercore {

enum class TextureType {
	Texture1D,
	Texture2D,
	Texture3D,
	TextureCubemap
};

struct TextureFormat {
	NumberFormat numFormat;
	PixelFormat pxFormat;
};

struct TextureSize {
	int width;
	int height;
	int depth;
};

enum class TextureTiling {
	LinearTiling,
	OptimalTiling
};

enum TextureUsageFlags {
	TUSG_ShaderAccessRead = 1 << 0,
	TUSG_ShaderAccessWrite = 1 << 1,
	TUSG_ColorTarget = 1 << 2,
	TUSG_DepthStencilTarget = 1 << 3
};

struct TextureCreateInfo {
	TextureType type;
	TextureFormat format;
	TextureSize size;
	uint32_t mipLevels;
	uint32_t arraySize;
	uint32_t samples;
	TextureTiling tiling;
	int usage;
};

enum class CubemapFace {
	PositiveX = 0,
	NegativeX,
	PositiveY,
	NegativeY,
	PositiveZ,
	NegativeZ,
	Max
};

typedef std::underlying_type<CubemapFace>::type CubemapFaceUType;

class Texture
{
public:
	virtual ~Texture() {}

	virtual void update1D(uint32_t mip, int offset, uint32_t size, size_t dataSize, const void* data, size_t rowStride) = 0;
	virtual void update1DArray(uint32_t mip, uint32_t arrayIndex, int offset, uint32_t size, size_t dataSize, const void* data, size_t rowStride) = 0;
	virtual void update2D(uint32_t mip, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) = 0;
	virtual void update2DArray(uint32_t mip, uint32_t arrayIndex, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) = 0;
	virtual void update3D(uint32_t mip, int offsetX, int offsetY, int offsetZ, uint32_t width, uint32_t height, uint32_t depth, size_t dataSize, const void* data, size_t rowStride) = 0;
	virtual void updateCubemapFace(uint32_t mip, CubemapFace face, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) = 0;
	virtual void updateCubemapArrayFace(uint32_t mip, uint32_t arrayIndex, CubemapFace face, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride) = 0;
};

} // namespace rendercore
#endif // _rendercore_texture_hpp_
