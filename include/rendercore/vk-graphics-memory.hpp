//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_vk_graphics_memory_hpp_
#define _rendercore_vk_graphics_memory_hpp_

#include <rendercore/graphics-memory.hpp>
#include <rendercore/vk-functions.hpp>

namespace rendercore {

class VulkanGraphicsMemory : public GraphicsMemory
{
public:
	VulkanGraphicsMemory();
	VulkanGraphicsMemory(VkDevice device, VulkanFunctions& vk, VkBufferUsageFlags usage, const VkPhysicalDeviceMemoryProperties& memProps);
	~VulkanGraphicsMemory();

	void reallocate(size_t size) override;
	CPUMemoryPointer pin(ptrdiff_t offset, size_t length, int flags) override;
	void unpin() override;
	void updateData(ptrdiff_t offset, size_t length, const void* data) override;
	size_t getCurrentSize() const override;

public:
	void release();

	inline bool empty() const
	{
		return m_device == VK_NULL_HANDLE;
	}

private:
	void releaseMemory();

private:
	VkDevice m_device;
	VulkanFunctions* m_vk;
	VkBufferUsageFlags m_usage;
	const VkPhysicalDeviceMemoryProperties* m_memProps;
	VkBuffer m_buffer;
	VkDeviceMemory m_deviceMem;
};

} // namespace rendercore

#endif // _rendercore_vk_graphics_memory_hpp_
