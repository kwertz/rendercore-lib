//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_rendering_pipeline_hpp_
#define _rendercore_rendering_pipeline_hpp_

#include <cstdint>
#include <rendercore/data-format.hpp>
#include <rendercore/compare-function.hpp>

namespace rendercore {

class DescriptorSetLayout;

struct PipelineLayoutCreateInfo {
	uint32_t setLayoutCount;
	const DescriptorSetLayout* const* setLayouts;
};

class PipelineLayout
{};

class Shader;

struct PipelineShaderInfo {
	const Shader* shader;
};

enum class PrimitiveTopology {
	Points,
	Lines,
	LineStrip,
	Triangles,
	TriangleStrip,
	LinesAdj,
	LineStripAdj,
	TrianglesAdj,
	TriangleStripAdj,
	Patches
};

struct VertexInputBindingDescription {
	uint32_t binding; //!< 0 <= binding < kMaxVertexMemoryBindPoints
	uint32_t stride;
};

struct VertexInputAttributeDescription {
	uint32_t location;
	uint32_t binding; //!< 0 <= binding < kMaxVertexMemoryBindPoints
	uint32_t numComponents;
	NumberFormat format;
	uint32_t offset;
};

struct PipelineVertexInputStateCreateInfo {
	uint32_t vertexBindingDescriptionCount;
	const VertexInputBindingDescription* vertexBindingDescriptions;
	uint32_t vertexAttributeDescriptionCount;
	const VertexInputAttributeDescription* vertexAttributeDescriptions;
};

struct PipelineInputAssemblerStateCreateInfo {
	PrimitiveTopology topology;
};

struct PipelineTessellationStateCreateInfo {
	uint32_t patchControlPoints;
};

static const int kMaxViewports = 8;

struct ViewportInfo {
	float originX;
	float originY;
	float width;
	float height;
	float minDepth;
	float maxDepth;
};

struct Rectangle {
	int32_t offsetX;
	int32_t offsetY;
	uint32_t extentX;
	uint32_t extentY;
};

struct PipelineViewportStateCreateInfo {
	uint8_t viewportCount;
	bool enableScissor;
	ViewportInfo viewports[kMaxViewports];
	Rectangle scissorRegions[kMaxViewports];
};

enum class FillMode {
	Solid,
	Wireframe
	// Dots?
};

enum class CullMode {
	None,
	Front,
	Back,
	FrontAndBack
};

enum class FaceOrientation {
	CounterClockwise,
	Clockwise
};

struct PipelineRasterizerStateCreateInfo {
	bool enableDepthClamping;
	bool enableRasterizerDiscard;
	FillMode fillMode;
	CullMode cullMode;
	FaceOrientation frontFace;
	bool enableDepthBias;
	float depthBiasConstantFactor;
	float depthBiasClamp;
	float depthBiasSlopeFactor;
	float lineWidth;
};

struct PipelineMultisampleStateCreateInfo {
	uint32_t samples;
	uint32_t sampleMask;
	bool enableAlphaToCoverage;
};

enum class StencilOperation {
	Keep,
	Zero,
	Replace,
	IncClamp,
	DecClamp,
	Invert,
	IncWrap,
	DecWrap
};

struct DepthStencilOperation {
	StencilOperation stencilFailOp;
	StencilOperation stencilPassOp;
	StencilOperation stencilDepthFailOp;
	CompareFunction stencilFunc;
	uint8_t stencilRef;
};

struct PipelineDepthStencilStateCreateInfo {
	bool enableDepthTest;
	bool enableDepthWrite;
	CompareFunction depthCompareFunc;
	bool enableDepthBounds;
	float minDepth;
	float maxDepth;
	bool enableStencilTest;
	uint8_t stencilReadMask;
	uint8_t stencilWriteMask;
	DepthStencilOperation frontOp;
	DepthStencilOperation backOp;
};

enum class BlendFactor {
	Zero,
	One,
	SrcColor,
	OneMinusSrcColor,
	DestColor,
	OneMinusDestColor,
	SrcAlpha,
	OneMinusSrcAlpha,
	DestAlpha,
	OneMinusDestAlpha,
	ConstantColor,
	OneMinusConstantColor,
	ConstantAlpha,
	OneMinusConstantAlpha,
	SrcAlphaSaturate,
	Src1Color,
	OneMinusSrc1Color,
	Src1Alpha,
	OneMinusSrc1Alpha,
};

enum class BlendFunction {
	Add,
	Subtract,
	ReverseSubtract,
	Min,
	Max
};

struct PipelineColorBlenderTargetStateInfo {
	bool enableBlending;
	BlendFactor srcBlendColor;
	BlendFactor destBlendColor;
	BlendFunction blendFuncColor;
	BlendFactor srcBlendAlpha;
	BlendFactor destBlendAlpha;
	BlendFunction blendFuncAlpha;
	uint8_t channelWriteMask;
};

static const int kPipelineMaxColorTargets = 8;

enum class LogicalOperation {
	Copy,
	Clear,
	And,
	AndReverse,
	AndInverted,
	Noop,
	Xor,
	Or,
	Nor,
	Equiv,
	Invert,
	OrReverse,
	CopyInverted,
	OrInverted,
	Nand,
	Set
};

struct PipelineColorBlenderStateCreateInfo {
	bool enableDualSourceBlend;
	bool enableLogicOp;
	LogicalOperation logicOp;
	PipelineColorBlenderTargetStateInfo target[kPipelineMaxColorTargets];
	float blendConst[4];
};

struct RenderingPipelineCreateInfo {
	PipelineShaderInfo vertexShader;
	PipelineShaderInfo tessCtrlShader;
	PipelineShaderInfo tessEvalShader;
	PipelineShaderInfo geometryShader;
	PipelineShaderInfo fragmentShader;
	PipelineVertexInputStateCreateInfo viState;
	PipelineInputAssemblerStateCreateInfo iaState;
	PipelineTessellationStateCreateInfo tessState;
	PipelineViewportStateCreateInfo vpState;
	PipelineRasterizerStateCreateInfo rsState;
	PipelineMultisampleStateCreateInfo msState;
	PipelineDepthStencilStateCreateInfo dbState;
	PipelineColorBlenderStateCreateInfo cbState;
};

class RenderingPipeline
{
public:
	virtual ~RenderingPipeline() {}
};

} // namespace rendercore
#endif // _rendercore_rendering_pipeline_hpp_
