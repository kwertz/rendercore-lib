//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_descriptor_set_hpp_
#define _rendercore_descriptor_set_hpp_

#include <cstdint>
#include <rendercore/data-format.hpp>

namespace rendercore {

enum class DescriptorType {
	Sampler = 0,
	CombinedImageSampler = 1,
	SampledImage = 2,
	StorageImage = 3,
	UniformTexelBuffer = 4,
	StorageTexelBuffer = 5,
	UniformBuffer = 6,
	StorageBuffer = 7,
	UniformBufferDynamic = 8,
	StorageBufferDynamic = 9,
	InputAttachment = 10
};

enum ShaderStageFlagBits {
	SHADER_STAGE_VERTEX_BIT = 0x00000001,
	SHADER_STAGE_TESSELLATION_CONTROL_BIT = 0x00000002,
	SHADER_STAGE_TESSELLATION_EVALUATION_BIT = 0x00000004,
	SHADER_STAGE_GEOMETRY_BIT = 0x00000008,
	SHADER_STAGE_FRAGMENT_BIT = 0x00000010,
	SHADER_STAGE_COMPUTE_BIT = 0x00000020
};

struct DescriptorSetLayoutBinding {
	uint32_t binding;
	DescriptorType descriptorType;
	uint32_t descriptorCount;
	int stageFlags;
};

struct DescriptorSetLayoutCreateInfo {
	uint32_t bindingCount;
	const DescriptorSetLayoutBinding* bindings;
};

class DescriptorSetLayout
{};

struct DescriptorPoolSize {
	DescriptorType type;
	uint32_t descriptorCount;
};

struct DescriptorPoolCreateInfo {
	uint32_t maxSets;
	uint32_t poolSizeCount;
	const DescriptorPoolSize* poolSizes;
};

struct DescriptorSetAllocateInfo {
	uint32_t descriptorSetCount;
	const DescriptorSetLayout* setLayouts;
};

class DescriptorSet
{};

class DescriptorPool
{
public:
	virtual ~DescriptorPool();

	virtual void allocateDescriptorSets(uint32_t count, DescriptorSet** sets) = 0;
	virtual void freeDescriptorSets(uint32_t count, const DescriptorSet** sets) = 0;
};

class Sampler;
class ImageView;

struct DescriptorImageInfo {
	Sampler* sampler;
	ImageView* imageView;
	ImageLayout imageLayout;
};

class GraphicsMemory;

struct DescriptorBufferInfo {
	const GraphicsMemory* memory;
	uint32_t offset;
	uint32_t range;
};

class BufferView;

struct WriteDescriptorSet {
	DescriptorSet* dstSet;
	uint32_t dstBinding;
	uint32_t dstArrayElement;
	uint32_t descriptorCount;
	DescriptorType descriptorType;
	const DescriptorImageInfo* imageInfo;
	const DescriptorBufferInfo* bufferInfo;
	const BufferView** texelBufferViews;
};

} // namespace rendercore

#endif // _rendercore_descriptor_set_hpp_
