//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_render_unit_hpp_
#define _rendercore_render_unit_hpp_

#include <cstdint>
#include <rendercore/rendering-pipeline.hpp>
#include <rendercore/render-pass.hpp>

namespace rendercore {

class VertexStream;

class GraphicsMemory;

class TextureSet;

class RenderOutputTarget;

enum class RenderUnitType {
	Draw,
	Draw_BeginRenderPass,
	Draw_EndRenderPass,
	Draw_Once
};

union ClearColorValue {
	float float32[4];
	int32_t int32[4];
	uint32_t uint32[4];
};

struct ClearDepthStencilValue {
	float depth;
	uint32_t stencil;
};

union ClearValue {
	ClearColorValue color;
	ClearDepthStencilValue depthStencil;
};

struct RenderPassInfo {
	const RenderPass* renderPass;
	const RenderOutputTarget* outputTarget;
	Rectangle renderArea;
	uint32_t clearValueCount;
	ClearValue clearValues[4];
};

struct ShaderConstantsMemoryInfo {
	const GraphicsMemory* mem;
	uint32_t offset; //!< must be aligned according to the ShaderConstantsOffsetAlignment property
	uint32_t size;
};

class DescriptorSet;

struct DescriptorSetBindingInfo {
	PipelineBindPoint pipelineBindPoint;
	const PipelineLayout* layout;
	uint32_t firstSet;
	uint32_t descriptorSetCount;
	const DescriptorSet* descriptorSets[8];
	uint32_t dynamicOffsetCount;
	uint32_t dynamicOffsets[8];
};

//! Represents one draw call with all associated state.
struct RenderUnit {
	RenderUnitType type;

	RenderPassInfo renderPass;

	// rendering pipeline and associated resources
	const RenderingPipeline* pipeline;
	DescriptorSetBindingInfo descriptorSetBinding;

	uint32_t instanceCount;

	const VertexStream* vstream;
};

} // namespace rendercore
#endif // _rendercore_render_unit_hpp_
