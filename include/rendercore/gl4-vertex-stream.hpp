//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_vertex_shader_hpp_
#define _rendercore_gl4_vertex_shader_hpp_

#include <rendercore/vertex-stream.hpp>
#include <GL/gl3w.h>
#include <array>

namespace rendercore {

class GL4RenderDispatcher;

class GL4VertexStream : public VertexStream
{
public:
	GL4VertexStream();
	GL4VertexStream(const VertexStreamCreateInfo& createInfo, GL4RenderDispatcher& dispatcher);
	~GL4VertexStream();

	GL4VertexStream(const GL4VertexStream&) = delete;
	GL4VertexStream& operator=(const GL4VertexStream&) = delete;

	GL4VertexStream(GL4VertexStream&& other);
	GL4VertexStream& operator=(GL4VertexStream&& other);

	void release();

	inline void bind(const GLsizei* strides) const
	{
		uint32_t n = m_vsBindInfo.vertexBufferBindInfoCount;
		for (uint32_t i = 0; i < n; ++i) {
			const VertexBufferBindInfo& bindInfo = m_vsBindInfo.vertexBufferBindInfos[i];
			glBindVertexBuffer(bindInfo.binding, bindInfo.vertexBuffer, bindInfo.offset, strides[bindInfo.binding]);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vsBindInfo.indexBuffer);
	}

private:
	bool m_empty;
	GL4RenderDispatcher* m_dispatcher;

	struct VertexBufferBindInfo {
		GLuint vertexBuffer;
		GLuint binding;
		GLuint offset;
	};

	struct GLVertexStreamBindInfo {
		std::array<VertexBufferBindInfo, kMaxVertexMemoryBindPoints> vertexBufferBindInfos;
		uint32_t vertexBufferBindInfoCount;
		GLuint indexBuffer;
	} m_vsBindInfo;

public: // for access from inside rendercore
	struct GLElementDrawInfo {
		GLsizei count;
		GLenum type;
		const GLvoid* ptr;
	} m_drawInfo;

public:
	inline bool empty() const
	{
		return m_empty;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_vertex_shader_hpp_
