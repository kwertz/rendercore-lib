//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_rendering_api_hpp_
#define _rendercore_gl4_rendering_api_hpp_

#include <rendercore/rendering-api.hpp>

#include <vector>
#include <memory>

#include <rendercore/fixed-object-pool.hpp>
#include <rendercore/gl4-render-dispatcher.hpp>
#include <rendercore/gl4-render-queue.hpp>
#include <rendercore/gl4-render-storage.hpp>
#include <rendercore/gl4-graphics-memory.hpp>
#include <rendercore/gl4-shader.hpp>
#include <rendercore/gl4-rendering-pipeline.hpp>
#include <rendercore/gl4-vertex-stream.hpp>
#include <rendercore/gl4-sampler.hpp>
#include <rendercore/gl4-texture.hpp>
#include <rendercore/gl4-texture-set.hpp>
#include <rendercore/gl4-render-output-buffer.hpp>
#include <rendercore/gl4-render-output-target.hpp>
#ifdef RENDERCORE_USE_PLATFORM_WIN32
#include <rendercore/gl4-wgl-context.hpp>
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
#include <rendercore/gl4-glx-context.hpp>
#endif

namespace rendercore {

struct GL4PropertyCache {
	GLint uboOffsetAlignment;
};

class GL4RenderingAPI : public RenderingAPI
{
public:
	GL4RenderingAPI(const RenderingAPICreateInfo& createInfo);
	~GL4RenderingAPI();

	int getPropertyInteger(RenderingAPIProperty which) override;

	RenderQueue* getRenderQueue(RenderQueueType type) override;
	RenderOutputTarget* getDefaultRenderOutputTarget() override;
	Swapchain* getSwapchain() override;

	RenderStorage* createRenderStorage() override;
	GraphicsMemory* createVertexMemory(bool dynamic) override;
	GraphicsMemory* createIndexMemory(bool dynamic) override;
	GraphicsMemory* createShaderConstantsMemory() override;
	Shader* createShader(const ShaderCreateInfo& createInfo) override;
	RenderingPipeline* createRenderingPipeline(const RenderingPipelineCreateInfo& createInfo) override;
	VertexStream* createVertexStream(const VertexStreamCreateInfo& createInfo) override;
	Sampler* createSampler(const SamplerCreateInfo& createInfo) override;
	Texture* createTexture(const TextureCreateInfo& createInfo) override;
	TextureSet* createTextureSet(const TextureSetCreateInfo& createInfo) override;
	RenderOutputBuffer* createRenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo) override;
	RenderOutputTarget* createRenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo) override;
	DescriptorSetLayout* createDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& createInfo) override;
	PipelineLayout* createPipelineLayout(const PipelineLayoutCreateInfo& createInfo) override;
	DescriptorPool* createDescriptorPool(const DescriptorPoolCreateInfo& createInfo) override;
	RenderPass* createRenderPass(const RenderPassCreateInfo& createInfo) override;

	void releaseRenderStorage(RenderStorage* storage) override;
	void releaseVertexMemory(GraphicsMemory* gfxMemory) override;
	void releaseIndexMemory(GraphicsMemory* gfxMemory) override;
	void releaseShaderConstantsMemory(GraphicsMemory* gfxMemory) override;
	void releaseShader(Shader* shader) override;
	void releaseRenderingPipeline(RenderingPipeline* pipeline) override;
	void releaseVertexStream(VertexStream* stream) override;
	void releaseSampler(Sampler* sampler) override;
	void releaseTexture(Texture* texture) override;
	void releaseTextureSet(TextureSet* textureSet) override;
	void releaseRenderOutputBuffer(RenderOutputBuffer* buffer) override;
	void releaseRenderOutputTarget(RenderOutputTarget* buffer) override;
	void releaseDescriptorSetLayout(DescriptorSetLayout* layout) override;
	void releasePipelineLayout(PipelineLayout* layout) override;
	void releaseDescriptorPool(DescriptorPool* pool) override;
	void releaseRenderPass(RenderPass* pass) override;

private:
	void createGLContext(const RenderingAPICreateInfo& createInfo);
	void initializeGLPlatform(Platform platformType, const PlatformSurfaceReference& surface);
	void terminateGLPlatform();
	void destroyGLContext();
	void fillPropertyCache();

private:
	GL4PropertyCache m_propertyCache;
	GL4RenderDispatcher m_renderDispatcher;
	GL4RenderQueue m_renderQueue;
	GL4RenderOutputTarget m_defaultRenderOutputTarget;
	Platform m_currentPlatform;
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	WGLContextInfo m_wglCtx;
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	GLXContextInfo m_glxCtx;
#endif
	struct Cleanup {
		Cleanup(GL4RenderingAPI& api);
		~Cleanup();
		GL4RenderingAPI& api;
	} m_cleanup;
	std::shared_ptr<Swapchain> m_swapchain;
	FixedObjectPool<GL4RenderStorage> m_renderStoragePool;
	FixedObjectPool<GL4GraphicsMemory> m_gfxMemoryPool;
	FixedObjectPool<GL4Shader> m_shaderPool;
	FixedObjectPool<GL4RenderingPipeline> m_pipelinePool;
	FixedObjectPool<GL4VertexStream> m_vertexStreamPool;
	FixedObjectPool<GL4Sampler> m_samplerPool;
	FixedObjectPool<GL4Texture> m_texturePool;
	FixedObjectPool<GL4TextureSet> m_textureSetPool;
	FixedObjectPool<GL4RenderOutputBuffer> m_renderOutputBufferPool;
	FixedObjectPool<GL4RenderOutputTarget> m_renderOutputTargetPool;
};

} // namespace rendercore
#endif // _rendercore_gl4_rendering_api_hpp_
