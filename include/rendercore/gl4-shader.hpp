//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_shader_hpp_
#define _rendercore_gl4_shader_hpp_

#include <rendercore/shader.hpp>
#include <GL/gl3w.h>
#include <stdexcept>
#include <string>

namespace rendercore {

class GL4ShaderBuildException : public std::runtime_error
{
public:
	GL4ShaderBuildException(const std::string& log);
	std::string getBuildLog() const;

private:
	std::string m_log;
};

class GL4Shader : public Shader
{
public:
	GL4Shader();
	GL4Shader(const ShaderCreateInfo& createInfo);
	~GL4Shader();

	GL4Shader(const GL4Shader&) = delete;
	GL4Shader& operator=(const GL4Shader&) = delete;

	GL4Shader(GL4Shader&& other);
	GL4Shader& operator=(GL4Shader&& other);

	void release();

public:
	GLuint m_obj;

	inline bool empty() const
	{
		return m_obj == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_shader_hpp_
