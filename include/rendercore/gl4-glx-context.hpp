//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_glx_context_hpp_
#define _rendercore_gl4_glx_context_hpp_

#include <rendercore/platform.hpp>
#include <rendercore/rendering-api.hpp>

typedef XID GLXWindow;
typedef struct __GLXcontext* GLXContext;

namespace rendercore {

struct GLXContextInfo {
	GLXContext handle;
	GLXWindow window;
};

void initializeGLX(Display* display);
void terminateGLX();
void createContextGLX(GLXContextInfo& ctx, std::shared_ptr<Swapchain>& swapchain, const RenderingAPICreateInfo& createInfo);
void destroyContextGLX(GLXContextInfo& ctx);

} // namespace rendercore

#endif // _rendercore_gl4_glx_context_hpp_
