//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_vk_render_output_target_hpp_
#define _rendercore_vk_render_output_target_hpp_

#include <rendercore/render-output-target.hpp>
#include <array>
#include <rendercore/vk-functions.hpp>
#include <rendercore/rendering-api.hpp>

namespace rendercore {

struct VulkanSwapchain : public Swapchain {
	VulkanSwapchain(VkDevice device, VulkanFunctions& vk, VkSwapchainKHR swapchain, std::array<VkImage, 2> images);
	~VulkanSwapchain();

	void present() override;

	VkDevice device;
	VulkanFunctions* vk;
	VkSwapchainKHR swapchain;
	VkQueue queue;
	std::array<VkImage, 2> images;
	uint32_t currentImage;
};

class VulkanPresentableRenderOutputTarget : public RenderOutputTarget
{
public:
	VulkanPresentableRenderOutputTarget();
	VulkanPresentableRenderOutputTarget(VkInstance instance, VulkanFunctions& functions, VkSurfaceKHR surface);
	~VulkanPresentableRenderOutputTarget();

	VulkanPresentableRenderOutputTarget(const VulkanPresentableRenderOutputTarget&) = delete;
	VulkanPresentableRenderOutputTarget& operator=(const VulkanPresentableRenderOutputTarget&) = delete;

	VulkanPresentableRenderOutputTarget(VulkanPresentableRenderOutputTarget&& other);
	VulkanPresentableRenderOutputTarget& operator=(VulkanPresentableRenderOutputTarget&& other);

private:
	VkInstance m_instance;
	VulkanFunctions* m_vk;
	VkSurfaceKHR m_surface;
};

VulkanPresentableRenderOutputTarget createDefaultOutputTargetVulkan(
    VkInstance instance, VkDevice device,
    VulkanFunctions& vk,
    VkPhysicalDevice gpu, uint32_t queueFamilyIndex,
    const PlatformSurfaceReference& platformSurface,
    const FramebufferSettings& fbsettings,
    std::shared_ptr<VulkanSwapchain>& swapchain);

} // namespace rendercore

#endif // _rendercore_vk_render_output_target_hpp_
