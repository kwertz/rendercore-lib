//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_fixed_object_pool_hpp_
#define _rendercore_fixed_object_pool_hpp_

#include <vector>
#include <cstdint>

namespace rendercore {

template <typename Object>
class FixedObjectPool
{
public:
	FixedObjectPool(size_t numObjects)
		: m_pool(numObjects), m_nextIndex(0)
	{}

	~FixedObjectPool()
	{}

	template <typename... Params>
	Object* create(Params&& ... constructionArgs)
	{
		int slot = findNextFreeSlot();
		if (slot >= 0) {
			m_nextIndex = (slot + 1) % static_cast<int>(m_pool.size());
			return new(&m_pool[slot]) Object(std::forward<Params>(constructionArgs)...);
		} else {
			return new Object(std::forward<Params>(constructionArgs)...);
		}
	}

	void release(Object* obj)
	{
		ptrdiff_t index = obj - m_pool.data();
		if (index >= 0 && index < static_cast<int>(m_pool.size())) {
			obj->release();
			m_nextIndex = static_cast<int>(index);
		} else {
			delete obj;
		}
	}

private:
	int findNextFreeSlot()
	{
		int numObjects = static_cast<int>(m_pool.size());
		for (int i = 0; i < numObjects; ++i) {
			int index = (m_nextIndex + i) % numObjects;
			if (m_pool[index].empty()) {
				return index;
			}
		}
		return -1;
	}

public:
	std::vector<Object> m_pool;
private:
	int m_nextIndex;
};

} // namespace rendercore
#endif // _rendercore_fixed_object_pool_hpp_
