//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_sampler_hpp_
#define _rendercore_gl4_sampler_hpp_

#include <rendercore/sampler.hpp>
#include <GL/gl3w.h>

namespace rendercore {

class GL4Sampler : public Sampler
{
public:
	GL4Sampler();
	GL4Sampler(const SamplerCreateInfo& createInfo);
	~GL4Sampler();

	GL4Sampler(const GL4Sampler&) = delete;
	GL4Sampler& operator=(const GL4Sampler&) = delete;

	GL4Sampler(GL4Sampler&& other);
	GL4Sampler& operator=(GL4Sampler&& other);

	void release();

	inline void bind(GLuint unit) const
	{
		glBindSampler(unit, m_sampler);
	}

private:
	GLuint m_sampler;

public:
	inline bool empty() const
	{
		return m_sampler == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_sampler_hpp_
