//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_texture_set_hpp_
#define _rendercore_gl4_texture_set_hpp_

#include <rendercore/texture-set.hpp>
#include <vector>
#include <GL/gl3w.h>
#include <rendercore/gl4-texture.hpp>
#include <rendercore/gl4-sampler.hpp>

namespace rendercore {

class GL4TextureSet : public TextureSet
{
public:
	GL4TextureSet();
	GL4TextureSet(const TextureSetCreateInfo& createInfo);
	~GL4TextureSet();

	GL4TextureSet(const GL4TextureSet&) = delete;
	GL4TextureSet& operator=(const GL4TextureSet&) = delete;

	GL4TextureSet(GL4TextureSet&& other);
	GL4TextureSet& operator=(GL4TextureSet&& other);

	const TextureSamplerPair& getPair(int which) const override;
	TextureSamplerPair& getPair(int which) override;

	inline void bind() const
	{
		int numPairs = static_cast<int>(m_tsPairs.size());
		for (int i = 0; i < numPairs; ++i) {
			const TextureSamplerPair& tsPair = m_tsPairs[i];
			static_cast<const GL4Texture*>(tsPair.texture)->bind(i);
			static_cast<const GL4Sampler*>(tsPair.sampler)->bind(i);
		}
	}

private:
	std::vector<TextureSamplerPair> m_tsPairs;

public:
	inline void release()
	{
		m_tsPairs.clear();
	}

	inline bool empty() const
	{
		return m_tsPairs.empty();
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_texture_set_hpp_
