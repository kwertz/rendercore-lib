//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_render_dispatcher_hpp_
#define _rendercore_gl4_render_dispatcher_hpp_

#include <rendercore/render-unit.hpp>

namespace rendercore {

class GL4RenderStorage;

class GL4RenderingPipeline;
class GL4VertexStream;
class GL4GraphicsMemory;
class GL4TextureSet;
class GL4RenderOutputTarget;

class GL4RenderDispatcher
{
public:
	GL4RenderDispatcher();
	~GL4RenderDispatcher();

	GL4RenderDispatcher(const GL4RenderDispatcher&) = delete;
	GL4RenderDispatcher& operator=(const GL4RenderDispatcher&) = delete;

	void dispatch(const GL4RenderStorage* storage);

	// remember to invalidate when changing GL state during object construction!
	void invalidatePipeline();
	void invalidateVertexStream();
	void invalidateConstantsMemories();
	void invalidateConstantsMemory(int which);
	void invalidateTextureSet();
	void invalidateRenderOutputTarget();

private:
	void dispatchDraw(const RenderUnit& unit);

	const GL4RenderingPipeline* m_pipeline;
	const GL4VertexStream* m_vertexStream;
	ShaderConstantsMemoryInfo m_constantsMemories[5];
	const GL4TextureSet* m_textureSet;
	const GL4RenderOutputTarget* m_renderOutputTarget;
};

} // namespace rendercore
#endif // _rendercore_gl4_render_dispatcher_hpp_
