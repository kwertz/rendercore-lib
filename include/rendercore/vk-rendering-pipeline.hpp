//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_vk_rendering_pipeline_hpp_
#define _rendercore_vk_rendering_pipeline_hpp_

#include <rendercore/rendering-pipeline.hpp>
#include <rendercore/vk-functions.hpp>

namespace rendercore {

class VulkanPipelineLayout : public PipelineLayout
{
public:
	VulkanPipelineLayout();
	VulkanPipelineLayout(VkDevice device, VulkanFunctions& vk, const PipelineLayoutCreateInfo& createInfo);
	~VulkanPipelineLayout();

	VulkanPipelineLayout(const VulkanPipelineLayout&) = delete;
	VulkanPipelineLayout& operator=(const VulkanPipelineLayout&) = delete;

	VulkanPipelineLayout(VulkanPipelineLayout&& other);
	VulkanPipelineLayout& operator=(VulkanPipelineLayout&& other);

public:
	inline bool empty() const
	{
		return m_layout == VK_NULL_HANDLE;
	}

	void release();

private:
	VkDevice m_device;
	VulkanFunctions* m_vk;
	VkPipelineLayout m_layout;
};

class VulkanRenderingPipeline : public RenderingPipeline
{
public:
	VulkanRenderingPipeline();
	VulkanRenderingPipeline(VkDevice device, VulkanFunctions& vk, const RenderingPipelineCreateInfo& createInfo);
	~VulkanRenderingPipeline();

	VulkanRenderingPipeline(const VulkanRenderingPipeline&) = delete;
	VulkanRenderingPipeline& operator=(const VulkanRenderingPipeline&) = delete;

	VulkanRenderingPipeline(VulkanRenderingPipeline&& other);
	VulkanRenderingPipeline& operator=(VulkanRenderingPipeline&& other);

public:
	inline bool empty() const
	{
		return m_pipeline == VK_NULL_HANDLE;
	}

	void release();

private:
	VkDevice m_device;
	VulkanFunctions* m_vk;
	VkPipeline m_pipeline;
};

} // namespace rendercore

#endif // _rendercore_vk_rendering_pipeline_hpp_
