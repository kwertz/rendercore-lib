//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_vk_rendering_api_hpp_
#define _rendercore_vk_rendering_api_hpp_

#include <rendercore/rendering-api.hpp>
#include <rendercore/vk-functions.hpp>
#include <rendercore/vk-render-output-target.hpp>
#include <rendercore/vk-graphics-memory.hpp>
#include <rendercore/vk-vertex-stream.hpp>
#include <rendercore/vk-shader.hpp>
#include <rendercore/vk-descriptor-set.hpp>
#include <rendercore/vk-rendering-pipeline.hpp>

namespace rendercore {

class VulkanRenderingAPI : public RenderingAPI
{
public:
	VulkanRenderingAPI(const RenderingAPICreateInfo& createInfo);
	~VulkanRenderingAPI();

	int getPropertyInteger(RenderingAPIProperty which) override;

	RenderQueue* getRenderQueue(RenderQueueType type) override;
	RenderOutputTarget* getDefaultRenderOutputTarget() override;
	Swapchain* getSwapchain() override;
	RenderStorage* createRenderStorage() override;
	GraphicsMemory* createVertexMemory(bool dynamic) override;
	GraphicsMemory* createIndexMemory(bool dynamic) override;
	GraphicsMemory* createShaderConstantsMemory() override;
	Shader* createShader(const ShaderCreateInfo& createInfo) override;
	RenderingPipeline* createRenderingPipeline(const RenderingPipelineCreateInfo& createInfo) override;
	VertexStream* createVertexStream(const VertexStreamCreateInfo& createInfo) override;
	Sampler* createSampler(const SamplerCreateInfo& createInfo) override;
	Texture* createTexture(const TextureCreateInfo& createInfo) override;
	TextureSet* createTextureSet(const TextureSetCreateInfo& createInfo) override;
	RenderOutputBuffer* createRenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo) override;
	RenderOutputTarget* createRenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo) override;
	DescriptorSetLayout* createDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& createInfo) override;
	PipelineLayout* createPipelineLayout(const PipelineLayoutCreateInfo& createInfo) override;
	DescriptorPool* createDescriptorPool(const DescriptorPoolCreateInfo& createInfo) override;
	RenderPass* createRenderPass(const RenderPassCreateInfo& createInfo) override;

	void releaseRenderStorage(RenderStorage* storage) override;
	void releaseVertexMemory(GraphicsMemory* gfxMemory) override;
	void releaseIndexMemory(GraphicsMemory* gfxMemory) override;
	void releaseShaderConstantsMemory(GraphicsMemory* gfxMemory) override;
	void releaseShader(Shader* shader) override;
	void releaseRenderingPipeline(RenderingPipeline* pipeline) override;
	void releaseVertexStream(VertexStream* stream) override;
	void releaseSampler(Sampler* sampler) override;
	void releaseTexture(Texture* texture) override;
	void releaseTextureSet(TextureSet* textureSet) override;
	void releaseRenderOutputBuffer(RenderOutputBuffer* buffer) override;
	void releaseRenderOutputTarget(RenderOutputTarget* target) override;
	void releaseDescriptorSetLayout(DescriptorSetLayout* layout) override;
	void releasePipelineLayout(PipelineLayout* layout) override;
	void releaseDescriptorPool(DescriptorPool* pool) override;
	void releaseRenderPass(RenderPass* pass) override;

private:
	void initializeVulkanInstance(const RenderingAPICreateInfo& createInfo);
	void initializeVulkanDevice(const RenderingAPICreateInfo& createInfo);

private:
	struct Cleanup {
		Cleanup(VulkanRenderingAPI& api);
		~Cleanup();
		VulkanRenderingAPI& api;
	} m_cleanup;
	VulkanFunctions m_functions;
	bool m_validationFound;
	VkInstance m_instance;
	VkDebugReportCallbackEXT m_debugReportCallback;
	VkDevice m_device;
	VulkanPresentableRenderOutputTarget m_defaultOutputTarget;
	std::shared_ptr<VulkanSwapchain> m_swapchain;
	VkPhysicalDeviceMemoryProperties m_memProps;
};

} // namespace rendercore

#endif // _rendercore_vk_rendering_api_hpp_
