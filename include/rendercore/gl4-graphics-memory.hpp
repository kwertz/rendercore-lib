//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_graphics_memory_hpp_
#define _rendercore_gl4_graphics_memory_hpp_

#include <rendercore/graphics-memory.hpp>
#include <cstdint>
#include <GL/gl3w.h>

namespace rendercore {

class GL4RenderDispatcher;

class GL4GraphicsMemory : public GraphicsMemory
{
public:
	GL4GraphicsMemory();
	GL4GraphicsMemory(GLenum bindingTarget, GLenum usagePattern, GL4RenderDispatcher& dispatcher);
	~GL4GraphicsMemory();

	GL4GraphicsMemory(const GL4GraphicsMemory&) = delete;
	GL4GraphicsMemory& operator=(const GL4GraphicsMemory&) = delete;

	GL4GraphicsMemory(GL4GraphicsMemory&& other);
	GL4GraphicsMemory& operator=(GL4GraphicsMemory&& other);

	virtual void reallocate(size_t size) override;
	virtual CPUMemoryPointer pin(ptrdiff_t offset, size_t length, int flags) override;
	virtual void unpin() override;
	virtual void updateData(ptrdiff_t offset, size_t length, const void* data) override;
	virtual size_t getCurrentSize() const override;

	void release();

private:
	GLenum m_bindingTarget;
	GLenum m_usagePtn;
	size_t m_currentSize;
	GL4RenderDispatcher* m_dispatcher;

public:
	GLuint m_obj;

	inline bool empty() const
	{
		return m_obj == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_graphics_memory_hpp_
