//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_graphics_memory_hpp_
#define _rendercore_graphics_memory_hpp_

#include <cstddef>

namespace rendercore {

typedef void* CPUMemoryPointer;

class GraphicsMemory
{
public:
	enum MapFlags {
		MF_READ = 1 << 0,
		MF_WRITE = 1 << 1,
		MF_PERSISTENT = 1 << 2,
		MF_COHERENT = 1 << 3,
		MF_INVALIDATERANGE = 1 << 4,
		MF_INVALIDATEALL = 1 << 5,
		MF_UNSYNCHRONIZED = 1 << 6
	};

	virtual ~GraphicsMemory() {}

	virtual void reallocate(size_t size) = 0;
	virtual CPUMemoryPointer pin(ptrdiff_t offset, size_t length, int flags) = 0;
	virtual void unpin() = 0;
	virtual void updateData(ptrdiff_t offset, size_t length, const void* data) = 0;
	virtual size_t getCurrentSize() const = 0;
};

} // namespace rendercore
#endif // _rendercore_graphics_memory_hpp_
