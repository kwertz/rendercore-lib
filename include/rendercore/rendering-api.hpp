//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_rendering_api_hpp_
#define _rendercore_rendering_api_hpp_

#include <memory>
#include <stdexcept>
#include <string>
#include <cstdint>
#include <rendercore/platform.hpp>
#include <rendercore/data-format.hpp>
#include <rendercore/render-queue.hpp>
#include <rendercore/graphics-memory.hpp>
#include <rendercore/shader.hpp>
#include <rendercore/render-dynamic-state.hpp>
#include <rendercore/rendering-pipeline.hpp>
#include <rendercore/vertex-stream.hpp>
#include <rendercore/sampler.hpp>
#include <rendercore/texture.hpp>
#include <rendercore/texture-set.hpp>
#include <rendercore/render-output-buffer.hpp>
#include <rendercore/render-output-target.hpp>
#include <rendercore/descriptor-set.hpp>
#include <rendercore/render-pass.hpp>

namespace rendercore {

enum class SwapchainLayout {
	FrontBufferOnly,
	Doublebuffer
};

struct FramebufferSettings {
	bool srgb;
	SwapchainLayout swapchain;
	NumberFormat numFormat; // always RGBA
	uint32_t width;
	uint32_t height;
};

struct ApplicationInfo {
	std::string applicationName;
	uint32_t applicationVersion;
	std::string engineName;
	uint32_t engineVersion;
};

//! Supported platforms: Xlib, Win32
struct RenderingAPICreateInfo {
	Platform platformType;
	const PlatformSurfaceReference* surface;
	ApplicationInfo appInfo;
	bool enableErrorChecking;
	FramebufferSettings framebufferSettings;
};

class RenderStorage;

class APICreationException : public std::runtime_error
{
public:
	APICreationException(const std::string& msg) : std::runtime_error(msg)
	{}
};

enum class RenderingAPIProperty {
	ShaderConstantsOffsetAlignment
};

class Swapchain
{
public:
	virtual ~Swapchain() {}

	//! Asks the underlying rendering API to present the finished frame.
	virtual void present() = 0;
};

class RenderingAPI
{
public:
	virtual ~RenderingAPI() {}

	virtual int getPropertyInteger(RenderingAPIProperty which) = 0;

	virtual RenderQueue* getRenderQueue(RenderQueueType type) = 0;
	virtual RenderOutputTarget* getDefaultRenderOutputTarget() = 0;
	virtual Swapchain* getSwapchain() = 0;

	virtual RenderStorage* createRenderStorage() = 0;
	virtual GraphicsMemory* createVertexMemory(bool dynamic) = 0;
	virtual GraphicsMemory* createIndexMemory(bool dynamic) = 0;
	virtual GraphicsMemory* createShaderConstantsMemory() = 0; // FIXME - API: add dynamic flag?
	virtual Shader* createShader(const ShaderCreateInfo& createInfo) = 0;
	virtual RenderingPipeline* createRenderingPipeline(const RenderingPipelineCreateInfo& createInfo) = 0;
	virtual VertexStream* createVertexStream(const VertexStreamCreateInfo& createInfo) = 0;
	virtual Sampler* createSampler(const SamplerCreateInfo& createInfo) = 0;
	virtual Texture* createTexture(const TextureCreateInfo& createInfo) = 0;
	virtual TextureSet* createTextureSet(const TextureSetCreateInfo& createInfo) = 0;
	virtual RenderOutputBuffer* createRenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo) = 0;
	virtual RenderOutputTarget* createRenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo) = 0;
	virtual DescriptorSetLayout* createDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& createInfo) = 0;
	virtual PipelineLayout* createPipelineLayout(const PipelineLayoutCreateInfo& createInfo) = 0;
	virtual DescriptorPool* createDescriptorPool(const DescriptorPoolCreateInfo& createInfo) = 0;
	virtual RenderPass* createRenderPass(const RenderPassCreateInfo& createInfo) = 0;

	virtual void releaseRenderStorage(RenderStorage* storage) = 0;
	virtual void releaseVertexMemory(GraphicsMemory* gfxMemory) = 0;
	virtual void releaseIndexMemory(GraphicsMemory* gfxMemory) = 0;
	virtual void releaseShaderConstantsMemory(GraphicsMemory* gfxMemory) = 0;
	virtual void releaseShader(Shader* shader) = 0;
	virtual void releaseRenderingPipeline(RenderingPipeline* pipeline) = 0;
	virtual void releaseVertexStream(VertexStream* stream) = 0;
	virtual void releaseSampler(Sampler* sampler) = 0;
	virtual void releaseTexture(Texture* texture) = 0;
	virtual void releaseTextureSet(TextureSet* textureSet) = 0;
	virtual void releaseRenderOutputBuffer(RenderOutputBuffer* buffer) = 0;
	virtual void releaseRenderOutputTarget(RenderOutputTarget* target) = 0;
	virtual void releaseDescriptorSetLayout(DescriptorSetLayout* layout) = 0;
	virtual void releasePipelineLayout(PipelineLayout* layout) = 0;
	virtual void releaseDescriptorPool(DescriptorPool* pool) = 0;
	virtual void releaseRenderPass(RenderPass* pass) = 0;
};

} // namespace rendercore
#endif // _rendercore_rendering_api_hpp_
