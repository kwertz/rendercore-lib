//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_render_storage_hpp_
#define _rendercore_gl4_render_storage_hpp_

#include <rendercore/render-storage.hpp>
#include <vector>

namespace rendercore {

class GL4RenderStorage : public RenderStorage
{
public:
	GL4RenderStorage();
	~GL4RenderStorage();

	GL4RenderStorage(const GL4RenderStorage&) = delete;
	GL4RenderStorage& operator=(const GL4RenderStorage&) = delete;

	GL4RenderStorage(GL4RenderStorage&& other);
	GL4RenderStorage& operator=(GL4RenderStorage&& other);

	virtual void begin() override;
	virtual void end() override;
	virtual void store(const RenderUnit& renderUnit) override;

public: // for access from inside rendercore
	std::vector<RenderUnit> m_renderUnits;
	bool m_immutable;

public:
	inline void release()
	{
		m_renderUnits.clear();
	}

	inline bool empty() const
	{
		return m_renderUnits.empty();
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_render_storage_hpp_
