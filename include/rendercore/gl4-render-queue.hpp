//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_render_queue_hpp_
#define _rendercore_gl4_render_queue_hpp_

#include <rendercore/render-queue.hpp>
#include <vector>

namespace rendercore {

class GL4RenderDispatcher;

class GL4RenderQueue : public RenderQueue
{
public:
	GL4RenderQueue(GL4RenderDispatcher& dispatcher);
	~GL4RenderQueue();

	GL4RenderQueue(const GL4RenderQueue&) = delete;
	GL4RenderQueue& operator=(const GL4RenderQueue&) = delete;

	virtual void enqueue(const RenderStorage* storage) override;
	virtual void flush() override;

public: // for access inside rendercore
	std::vector<const RenderStorage*> m_storageQueue;
	GL4RenderDispatcher* m_dispatcher;
};

} // namespace rendercore
#endif // _rendercore_gl4_render_queue_hpp_
