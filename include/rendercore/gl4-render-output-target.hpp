//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_gl4_render_output_target_hpp_
#define _rendercore_gl4_render_output_target_hpp_

#include <rendercore/render-output-target.hpp>
#include <stdexcept>
#include <GL/gl3w.h>

namespace rendercore {

class GL4RenderDispatcher;

struct GL4DefaultFramebufferToken {
	inline GL4DefaultFramebufferToken(bool srgb) : srgb(srgb)
	{}

	bool srgb;
};

class GL4FramebufferIncompleteException : public std::runtime_error
{
public:
	GL4FramebufferIncompleteException();
};

class GL4RenderOutputTarget : public RenderOutputTarget
{
public:
	GL4RenderOutputTarget();
	GL4RenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo, GL4RenderDispatcher& dispatcher);
	GL4RenderOutputTarget(GL4DefaultFramebufferToken);
	~GL4RenderOutputTarget();

	GL4RenderOutputTarget(const GL4RenderOutputTarget&) = delete;
	GL4RenderOutputTarget& operator=(const GL4RenderOutputTarget&) = delete;

	GL4RenderOutputTarget(GL4RenderOutputTarget&& other);
	GL4RenderOutputTarget& operator=(GL4RenderOutputTarget&& other);

	void release();

	inline void bind() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
		if (m_enableSRGBGammaCorrection) {
			glEnable(GL_FRAMEBUFFER_SRGB);
		} else {
			glDisable(GL_FRAMEBUFFER_SRGB);
		}
	}

	inline bool isDefaultFramebuffer() const
	{
		return m_isDefaultFramebuffer;
	}

private:
	bool m_isDefaultFramebuffer;
	GLuint m_framebuffer;
	bool m_enableSRGBGammaCorrection;

public:
	inline bool empty() const
	{
		return !m_isDefaultFramebuffer && m_framebuffer == 0;
	}
};

} // namespace rendercore
#endif // _rendercore_gl4_render_output_target_hpp_
