//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifndef _rendercore_render_output_target_hpp_
#define _rendercore_render_output_target_hpp_

#include <cstdint>
#include <rendercore/texture.hpp>

namespace rendercore {

class RenderOutputBuffer;

enum class RenderOutputAttachmentType {
	ColorAttachment,
	DepthAttachment,
	StencilAttachment,
	DepthStencilAttachment
};

struct RenderOutputBufferAttachmentInfo {
	RenderOutputAttachmentType type;
	const RenderOutputBuffer* buffer;
	uint8_t colorAttachmentIndex;
};

struct TextureAttachmentInfo {
	RenderOutputAttachmentType type;
	const Texture* texture;
	uint8_t colorAttachmentIndex;
	int mipLevel;
	CubemapFace face;
	int layer;
};

struct RenderOutputTargetCreateInfo {
	const RenderOutputBufferAttachmentInfo* firstBufferAttachmentInfo;
	uint32_t numBufferAttachmentInfos;
	const TextureAttachmentInfo* firstTextureAttachmentInfo;
	uint32_t numTextureAttachmentInfos;
	bool enableSRGBGammaCorrection;
};

class RenderOutputTarget
{
public:
	virtual ~RenderOutputTarget() {}
};

} // namespace rendercore
#endif // _rendercore_render_output_target_hpp_
