//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-render-output-target.hpp>

#include <vector>
#include <array>
#include <rendercore/vk-platform.hpp>

namespace rendercore {

VulkanSwapchain::VulkanSwapchain(VkDevice device, VulkanFunctions& functions, VkSwapchainKHR swapchain, std::array<VkImage, 2> images)
	: device(device), vk(&functions), swapchain(swapchain),
	  queue(VK_NULL_HANDLE), // will be filled in by VulkanRenderingAPI
	  images(images), currentImage(0)
{}

VulkanSwapchain::~VulkanSwapchain()
{
	if (swapchain != VK_NULL_HANDLE) {
		vk->DestroySwapchainKHR(device, swapchain, nullptr);
	}
}

void VulkanSwapchain::present()
{
	VkSemaphore presentCompleteSemaphore;
	VkSemaphoreCreateInfo pcsCreateInfo = {};
	pcsCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	pcsCreateInfo.pNext = nullptr;
	pcsCreateInfo.flags = 0;

	VKCHECK(vk->CreateSemaphore(device, &pcsCreateInfo, nullptr, &presentCompleteSemaphore));

	VkResult res = vk->AcquireNextImageKHR(device, swapchain, UINT64_MAX, presentCompleteSemaphore, VK_NULL_HANDLE, &currentImage);
	if (res == VK_ERROR_OUT_OF_DATE_KHR) {
		throw std::runtime_error("Swapchain is out of date");
	}
	VKCHECK(res);

	// TODO: submit the queue here

	VkPresentInfoKHR present = {};
	present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present.pNext = nullptr;
	present.swapchainCount = 1;
	present.pSwapchains = &swapchain;
	present.pImageIndices = &currentImage;

	res = vk->QueuePresentKHR(queue, &present);
	if (res == VK_ERROR_OUT_OF_DATE_KHR) {
		throw std::runtime_error("Swapchain is out of date");
	}
	VKCHECK(res);

	VKCHECK(vk->QueueWaitIdle(queue));

	vk->DestroySemaphore(device, presentCompleteSemaphore, nullptr);
}

VulkanPresentableRenderOutputTarget createDefaultOutputTargetVulkan(
    VkInstance instance, VkDevice device,
    VulkanFunctions& vk,
    VkPhysicalDevice gpu, uint32_t queueFamilyIndex,
    const PlatformSurfaceReference& platformSurface,
    const FramebufferSettings& fbsettings,
    std::shared_ptr<VulkanSwapchain>& swapchain)
{
	// create a default render output target based on the given framebuffer settings
	VkSurfaceKHR surface;
	VKCHECK(createSurface(instance, platformSurface, nullptr, surface));
	VkBool32 surfSupported = VK_FALSE;
	VKCHECK(vk.GetPhysicalDeviceSurfaceSupportKHR(gpu, queueFamilyIndex, surface, &surfSupported));

	if (surfSupported == VK_FALSE) {
		vk.DestroySurfaceKHR(instance, surface, nullptr);
		throw std::runtime_error("Vulkan: Physical device does not support surfaces");
	}

	VkSurfaceCapabilitiesKHR surfCaps;
	VKCHECK(vk.GetPhysicalDeviceSurfaceCapabilitiesKHR(gpu, surface, &surfCaps));

	uint32_t presentModeCount;
	VKCHECK(vk.GetPhysicalDeviceSurfacePresentModesKHR(gpu, surface, &presentModeCount, nullptr));
	// choose a present mode, fall back to FIFO if there's no better mode
	// treat mailbox > immediate > FIFO
	VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;
	{
		std::vector<VkPresentModeKHR> presentModes(presentModeCount);
		VKCHECK(vk.GetPhysicalDeviceSurfacePresentModesKHR(gpu, surface, &presentModeCount, presentModes.data()));
		for (uint32_t i = 0; i < presentModeCount; ++i) {
			if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
				swapchainPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}
			if ((swapchainPresentMode != VK_PRESENT_MODE_MAILBOX_KHR) &&
			    (presentModes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR)) {
				swapchainPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
			}
		}
	}

	VkExtent2D swapchainExtent = {};
	if (surfCaps.currentExtent.width == 0xffffffff) {
		swapchainExtent.width = fbsettings.width;
		swapchainExtent.height = fbsettings.height;
	} else {
		swapchainExtent = surfCaps.currentExtent;
		outputDebugMessage("createDefaultOutputTargetVulkan: swapchain extents were forced by the driver");
	}

	VkSurfaceTransformFlagBitsKHR preTransform;
	if (surfCaps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
		preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	} else {
		preTransform = surfCaps.currentTransform;
	}

	uint32_t formatCount;
	VKCHECK(vk.GetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &formatCount, nullptr));
	VkFormat suitableFormat;
	VkColorSpaceKHR suitableColorSpace;
	{
		std::vector<VkSurfaceFormatKHR> surfFormats(formatCount);
		VKCHECK(vk.GetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &formatCount, surfFormats.data()));
		// FIXME: convert RenderCore format -> Vulkan format
		VkFormat desiredFormat = fbsettings.srgb ? VK_FORMAT_B8G8R8A8_SRGB : VK_FORMAT_B8G8R8A8_UNORM;
		if ((formatCount == 1) && (surfFormats[0].format == VK_FORMAT_UNDEFINED)) {
			suitableFormat = desiredFormat;
			suitableColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
		} else {
			assert(formatCount >= 1);
			suitableFormat = surfFormats[0].format;
			suitableColorSpace = surfFormats[0].colorSpace;
			for (uint32_t i = 1; i < formatCount; ++i) {
				if (surfFormats[i].format == desiredFormat) {
					suitableFormat = desiredFormat;
					suitableColorSpace = surfFormats[i].colorSpace;
					break;
				}
			}
		}
	}

	uint32_t swapchainImageCount = fbsettings.swapchain == SwapchainLayout::Doublebuffer ? 2u : 1u;
	VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.pNext = nullptr;
	swapchainCreateInfo.flags = 0;
	swapchainCreateInfo.surface = surface;
	swapchainCreateInfo.minImageCount = swapchainImageCount;
	swapchainCreateInfo.imageFormat = suitableFormat;
	swapchainCreateInfo.imageColorSpace = suitableColorSpace;
	swapchainCreateInfo.imageExtent = swapchainExtent;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCreateInfo.preTransform = preTransform;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchainCreateInfo.queueFamilyIndexCount = 0;
	swapchainCreateInfo.pQueueFamilyIndices = nullptr;
	swapchainCreateInfo.presentMode = swapchainPresentMode;
	swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;
	swapchainCreateInfo.clipped = VK_TRUE;

	VkSwapchainKHR swapchain_;
	VKCHECK(vk.CreateSwapchainKHR(device, &swapchainCreateInfo, nullptr, &swapchain_));

	std::array<VkImage, 2> swapchainImages;
	uint32_t actualSwapchainImageCount;
	VKCHECK(vk.GetSwapchainImagesKHR(device, swapchain_, &actualSwapchainImageCount, nullptr));
	// TODO: handle this case properly
	if (actualSwapchainImageCount > 2) {
		throw std::runtime_error("Vulkan: Got more than two swapchain images");
	}
	VKCHECK(vk.GetSwapchainImagesKHR(device, swapchain_, &actualSwapchainImageCount, swapchainImages.data()));

	swapchain = std::make_shared<VulkanSwapchain>(device, vk, swapchain_, swapchainImages);

	return VulkanPresentableRenderOutputTarget(instance, vk, surface);
}

VulkanPresentableRenderOutputTarget::VulkanPresentableRenderOutputTarget()
	: m_instance(VK_NULL_HANDLE), m_vk(nullptr), m_surface(VK_NULL_HANDLE)
{
}

VulkanPresentableRenderOutputTarget::VulkanPresentableRenderOutputTarget(VkInstance instance, VulkanFunctions& functions, VkSurfaceKHR surface)
	: m_instance(instance), m_vk(&functions), m_surface(surface)
{}

VulkanPresentableRenderOutputTarget::~VulkanPresentableRenderOutputTarget()
{
	if (m_surface != VK_NULL_HANDLE) {
		m_vk->DestroySurfaceKHR(m_instance, m_surface, nullptr);
	}
}

VulkanPresentableRenderOutputTarget::VulkanPresentableRenderOutputTarget(VulkanPresentableRenderOutputTarget&& other)
	: m_instance(other.m_instance), m_vk(other.m_vk), m_surface(other.m_surface)
{
	other.m_surface = nullptr;
}

VulkanPresentableRenderOutputTarget& VulkanPresentableRenderOutputTarget::operator=(VulkanPresentableRenderOutputTarget&& other)
{
	if (this != &other) {
		m_instance = other.m_instance;
		m_vk = other.m_vk;
		m_surface = other.m_surface;
		other.m_surface = nullptr;
	}
	return *this;
}

} // namespace rendercore
