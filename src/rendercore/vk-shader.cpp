//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-shader.hpp>

#include <stdexcept>
#include <vector>
#include <glslang/Public/ShaderLang.h>
#include <StandAlone/ResourceLimits.h>
#include <SPIRV/GlslangToSpv.h>
#include <rendercore/platform.hpp>

namespace rendercore {

VulkanShader::VulkanShader()
	: m_device(VK_NULL_HANDLE), m_vk(nullptr), m_shaderModule(VK_NULL_HANDLE)
{}

static void compileGLSL(std::vector<uint32_t>& spirv, const char* glslCode, size_t glslCodeSize, ShaderType shaderType)
{
	using namespace glslang;

	EShLanguage lang;
	switch (shaderType) {
		case ShaderType::Compute:
			lang = EShLangCompute;
			break;
		case ShaderType::Vertex:
			lang = EShLangVertex;
			break;
		case ShaderType::TessCtl:
			lang = EShLangTessControl;
			break;
		case ShaderType::TessEval:
			lang = EShLangTessEvaluation;
			break;
		case ShaderType::Geometry:
			lang = EShLangGeometry;
			break;
		case ShaderType::Fragment:
			lang = EShLangFragment;
			break;
	}

	// build our shader with default resource limits
	TShader shader(lang);
	const char* const glslStrings[] = { glslCode };
	const int glslLengths[] = { static_cast<int>(glslCodeSize) };
	shader.setPreamble("#define RENDERCORE_VULKAN\n");
	shader.setStringsWithLengths(glslStrings, glslLengths, 1);
	shader.setEntryPoint("main");
	if (!shader.parse(&DefaultTBuiltInResource, 110, false, EShMsgDefault)) {
		throw std::runtime_error("GLSL compilation failed");
	}

	outputDebugMessage("Shader compile info log:\n");
	outputDebugMessage(shader.getInfoLog());
	outputDebugMessage("\nShader compile debug info log:\n");
	outputDebugMessage(shader.getInfoDebugLog());

	// link our program
	TProgram program;
	program.addShader(&shader);
	if (!program.link(EShMsgDefault)) {
		throw std::runtime_error("Failed to link the GLSL shader program");
	}

	outputDebugMessage("Shader link info log:\n");
	outputDebugMessage(program.getInfoLog());
	outputDebugMessage("\nShader link debug info log:\n");
	outputDebugMessage(program.getInfoDebugLog());

	// finally generate our SPIR-V byte code to pass it to the ICD
	GlslangToSpv(*program.getIntermediate(lang), spirv);
}

VulkanShader::VulkanShader(VkDevice device, VulkanFunctions& vk, const ShaderCreateInfo& createInfo)
	: m_device(device), m_vk(&vk), m_shaderModule(VK_NULL_HANDLE)
{
	VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
	shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderModuleCreateInfo.pNext = nullptr;
	shaderModuleCreateInfo.flags = 0;

	std::vector<uint32_t> compiledSpirV;

	switch (createInfo.lang) {
		case ShadingLanguage::GLSL:
			outputDebugMessage("Vulkan: Client passed GLSL, compiling GLSL into SPIR-V on-the-fly.\n");
			compileGLSL(compiledSpirV, reinterpret_cast<const char*>(createInfo.code), createInfo.codeSize, createInfo.type);
			shaderModuleCreateInfo.codeSize = compiledSpirV.size() * sizeof(uint32_t);
			shaderModuleCreateInfo.pCode = compiledSpirV.data();
			break;
		case ShadingLanguage::SPIR_V:
			shaderModuleCreateInfo.codeSize = createInfo.codeSize;
			shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(createInfo.code);
			break;
	}

	VKCHECK(m_vk->CreateShaderModule(m_device, &shaderModuleCreateInfo, nullptr, &m_shaderModule));
}

VulkanShader::~VulkanShader()
{
	release();
}

VulkanShader::VulkanShader(VulkanShader&& other)
	: m_device(other.m_device), m_vk(other.m_vk), m_shaderModule(other.m_shaderModule)
{
	other.m_shaderModule = VK_NULL_HANDLE;
}

VulkanShader& VulkanShader::operator=(VulkanShader&& other)
{
	if (this != &other) {
		m_device = other.m_device;
		m_vk = other.m_vk;
		m_shaderModule = other.m_shaderModule;
		other.m_shaderModule = VK_NULL_HANDLE;
	}

	return *this;
}

void VulkanShader::release()
{
	if (m_shaderModule != VK_NULL_HANDLE) {
		m_vk->DestroyShaderModule(m_device, m_shaderModule, nullptr);
	}
}

} // namespace rendercore_
