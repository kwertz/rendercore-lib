//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-glx-context.hpp>

#include <vector>
#include <cstdint>
#include <cassert>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/glcorearb.h>
#include <m0tools/shared-library.hpp>
#include <rendercore/platform-surface.hpp>
#include <rendercore/gl4-context.hpp>

#define GLX_VENDOR 1
#define GLX_RGBA_BIT 0x00000001
#define GLX_WINDOW_BIT 0x00000001
#define GLX_DRAWABLE_TYPE 0x8010
#define GLX_RENDER_TYPE	0x8011
#define GLX_RGBA_TYPE 0x8014
#define GLX_DOUBLEBUFFER 5
#define GLX_STEREO 6
#define GLX_AUX_BUFFERS	7
#define GLX_RED_SIZE 8
#define GLX_GREEN_SIZE 9
#define GLX_BLUE_SIZE 10
#define GLX_ALPHA_SIZE 11
#define GLX_DEPTH_SIZE 12
#define GLX_STENCIL_SIZE 13
#define GLX_ACCUM_RED_SIZE 14
#define GLX_ACCUM_GREEN_SIZE 15
#define GLX_ACCUM_BLUE_SIZE	16
#define GLX_ACCUM_ALPHA_SIZE 17
#define GLX_SAMPLES 0x186a1
#define GLX_VISUAL_ID 0x800b

#define GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB 0x20b2
#define GLX_CONTEXT_DEBUG_BIT_ARB 0x00000001
#define GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002
#define GLX_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define GLX_CONTEXT_PROFILE_MASK_ARB 0x9126
#define GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x00000002
#define GLX_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define GLX_CONTEXT_MINOR_VERSION_ARB 0x2092
#define GLX_CONTEXT_FLAGS_ARB 0x2094
#define GLX_CONTEXT_ES2_PROFILE_BIT_EXT 0x00000004
#define GLX_CONTEXT_ROBUST_ACCESS_BIT_ARB 0x00000004
#define GLX_LOSE_CONTEXT_ON_RESET_ARB 0x8252
#define GLX_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB 0x8256
#define GLX_NO_RESET_NOTIFICATION_ARB 0x8261
#define GLX_CONTEXT_RELEASE_BEHAVIOR_ARB 0x2097
#define GLX_CONTEXT_RELEASE_BEHAVIOR_NONE_ARB 0
#define GLX_CONTEXT_RELEASE_BEHAVIOR_FLUSH_ARB 0x2098

typedef XID GLXDrawable;
typedef struct __GLXFBConfig* GLXFBConfig;
typedef void (*__GLXextproc)(void);

typedef int (*PFNGLXGETFBCONFIGATTRIBPROC)(Display*, GLXFBConfig, int, int*);
typedef const char* (*PFNGLXGETCLIENTSTRINGPROC)(Display*, int);
typedef Bool(*PFNGLXQUERYEXTENSIONPROC)(Display*, int*, int*);
typedef Bool(*PFNGLXQUERYVERSIONPROC)(Display*, int*, int*);
typedef void (*PFNGLXDESTROYCONTEXTPROC)(Display*, GLXContext);
typedef Bool(*PFNGLXMAKECURRENTPROC)(Display*, GLXDrawable, GLXContext);
typedef void (*PFNGLXSWAPBUFFERSPROC)(Display*, GLXDrawable);
typedef const char* (*PFNGLXQUERYEXTENSIONSSTRINGPROC)(Display*, int);
typedef GLXFBConfig* (*PFNGLXGETFBCONFIGSPROC)(Display*, int, int*);
typedef GLXContext(*PFNGLXCREATENEWCONTEXTPROC)(Display*, GLXFBConfig, int, GLXContext, Bool);
typedef __GLXextproc(* PFNGLXGETPROCADDRESSPROC)(const GLubyte* procName);
typedef int (*PFNGLXSWAPINTERVALMESAPROC)(int);
typedef int (*PFNGLXSWAPINTERVALSGIPROC)(int);
typedef void (*PFNGLXSWAPINTERVALEXTPROC)(Display*, GLXDrawable, int);
typedef GLXContext(*PFNGLXCREATECONTEXTATTRIBSARBPROC)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
typedef XVisualInfo* (*PFNGLXGETVISUALFROMFBCONFIGPROC)(Display*, GLXFBConfig);
typedef GLXWindow(*PFNGLXCREATEWINDOWPROC)(Display*, GLXFBConfig, Window, const int*);
typedef void (*PFNGLXDESTROYWINDOWPROC)(Display*, GLXWindow);
typedef int (*PFNXFREEPROC)(void*);

namespace rendercore {

struct GLXInfo {
	m0tools::SharedLibrary library;
	m0tools::SharedLibrary xlibLibrary;

	Display* display;

	int major;
	int minor;
	int eventBase;
	int errorBase;

	// GLX 1.3 functions
	PFNGLXGETFBCONFIGSPROC GetFBConfigs;
	PFNGLXGETFBCONFIGATTRIBPROC GetFBConfigAttrib;
	PFNGLXGETCLIENTSTRINGPROC GetClientString;
	PFNGLXQUERYEXTENSIONPROC QueryExtension;
	PFNGLXQUERYVERSIONPROC QueryVersion;
	PFNGLXDESTROYCONTEXTPROC DestroyContext;
	PFNGLXMAKECURRENTPROC MakeCurrent;
	PFNGLXSWAPBUFFERSPROC SwapBuffers;
	PFNGLXQUERYEXTENSIONSSTRINGPROC QueryExtensionsString;
	PFNGLXCREATENEWCONTEXTPROC CreateNewContext;
	PFNGLXGETVISUALFROMFBCONFIGPROC GetVisualFromFBConfig;
	PFNGLXCREATEWINDOWPROC CreateWindow;
	PFNGLXDESTROYWINDOWPROC DestroyWindow;

	// GLX 1.4 and extension functions
	PFNGLXGETPROCADDRESSPROC GetProcAddress;
	PFNGLXGETPROCADDRESSPROC GetProcAddressARB;
	PFNGLXSWAPINTERVALSGIPROC SwapIntervalSGI;
	PFNGLXSWAPINTERVALEXTPROC SwapIntervalEXT;
	PFNGLXSWAPINTERVALMESAPROC SwapIntervalMESA;
	PFNGLXCREATECONTEXTATTRIBSARBPROC CreateContextAttribsARB;
	bool SGI_swap_control;
	bool EXT_swap_control;
	bool MESA_swap_control;
	bool ARB_multisample;
	bool ARB_framebuffer_sRGB;
	bool EXT_framebuffer_sRGB;
	bool ARB_create_context;
	bool ARB_create_context_profile;
	bool ARB_create_context_robustness;
	bool EXT_create_context_es2_profile;
	bool ARB_context_flush_control;

	// Xlib functions
	PFNXFREEPROC XFree;
};

static GLXInfo s_glxInfo = {};

#ifndef GLXBadProfileARB
#define GLXBadProfileARB 13
#endif

static int getFBConfigAttrib(GLXFBConfig fbconfig, int attrib)
{
	int value;
	s_glxInfo.GetFBConfigAttrib(s_glxInfo.display, fbconfig, attrib, &value);
	return value;
}

static void chooseFBConfig(GLXFBConfig& result, GLXContextInfo& ctx, const FramebufferSettings& fbsettings)
{
	GLFramebufferConfig desired;
	makeGLFramebufferConfig(desired, fbsettings);

	int nativeCount;
	GLXFBConfig* nativeConfigs = s_glxInfo.GetFBConfigs(s_glxInfo.display, DefaultScreen(s_glxInfo.display), &nativeCount);
	if (nativeCount == 0) {
		throw std::runtime_error("GLX: glXGetFBConfigs returned 0 native configurations");
	}

	std::vector<GLFramebufferConfig> usableConfigs(nativeCount);
	int usableCount = 0;

	for (int i = 0; i < nativeCount; ++i) {
		const GLXFBConfig n = nativeConfigs[i];
		GLFramebufferConfig& u = usableConfigs[usableCount];

		if (!(getFBConfigAttrib(n, GLX_RENDER_TYPE) & GLX_RGBA_BIT)) {
			continue;
		}

		if (!(getFBConfigAttrib(n, GLX_DRAWABLE_TYPE) & GLX_WINDOW_BIT)) {
			continue;
		}

		u.redBits = getFBConfigAttrib(n, GLX_RED_SIZE);
		u.greenBits = getFBConfigAttrib(n, GLX_GREEN_SIZE);
		u.blueBits = getFBConfigAttrib(n, GLX_BLUE_SIZE);
		u.alphaBits = getFBConfigAttrib(n, GLX_ALPHA_SIZE);

		u.depthBits = getFBConfigAttrib(n, GLX_DEPTH_SIZE);
		u.stencilBits = getFBConfigAttrib(n, GLX_STENCIL_SIZE);

		u.accumRedBits = getFBConfigAttrib(n, GLX_ACCUM_RED_SIZE);
		u.accumGreenBits = getFBConfigAttrib(n, GLX_ACCUM_GREEN_SIZE);
		u.accumBlueBits = getFBConfigAttrib(n, GLX_ACCUM_BLUE_SIZE);
		u.accumAlphaBits = getFBConfigAttrib(n, GLX_ACCUM_ALPHA_SIZE);

		u.auxBuffers = getFBConfigAttrib(n, GLX_AUX_BUFFERS);

		u.stereo = getFBConfigAttrib(n, GLX_STEREO) > 0;
		u.doublebuffer = getFBConfigAttrib(n, GLX_DOUBLEBUFFER) > 0;

		if (s_glxInfo.ARB_multisample) {
			u.samples = getFBConfigAttrib(n, GLX_SAMPLES);
		}

		if (s_glxInfo.ARB_framebuffer_sRGB || s_glxInfo.EXT_framebuffer_sRGB) {
			u.sRGB = getFBConfigAttrib(n, GLX_FRAMEBUFFER_SRGB_CAPABLE_ARB) > 0;
		}

		u.handle = reinterpret_cast<uintptr_t>(n);
		++usableCount;
	}

	usableConfigs.resize(usableCount);
	const GLFramebufferConfig& closest = chooseFramebufferConfig(desired, usableConfigs);
	result = reinterpret_cast<GLXFBConfig>(closest.handle);

	s_glxInfo.XFree(nativeConfigs);
}

static void* getProcAddress(const char* procname)
{
	if (s_glxInfo.GetProcAddress != nullptr) {
		return reinterpret_cast<void*>(s_glxInfo.GetProcAddress((const GLubyte*)procname));
	} else if (s_glxInfo.GetProcAddressARB != nullptr) {
		return reinterpret_cast<void*>(s_glxInfo.GetProcAddressARB((const GLubyte*)procname));
	}
	return m0tools::getSharedLibrarySymbol(s_glxInfo.library, procname);
}

extern "C" void* rendercoreGLGetProcAddress(const char* procname)
{
	return getProcAddress(procname);
}

static bool isExtensionSupported(const char* extension)
{
	const char* extensions = s_glxInfo.QueryExtensionsString(s_glxInfo.display, DefaultScreen(s_glxInfo.display));
	if (extensions != nullptr) {
		if (stringInExtensionString(extension, extensions)) {
			return true;
		}
	}
	return false;
}

void initializeGLX(Display* display)
{
	using namespace m0tools;

	// save for later
	s_glxInfo.display = display;

	s_glxInfo.library = openSharedLibrary("libGL.so", SHLL_LAZY_BIT);
	if (s_glxInfo.library == nullptr) {
		throw std::runtime_error("GLX: Failed to load libGL.so");
	}

	s_glxInfo.GetFBConfigs = function_cast<PFNGLXGETFBCONFIGSPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXGetFBConfigs"));
	s_glxInfo.GetFBConfigAttrib = function_cast<PFNGLXGETFBCONFIGATTRIBPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXGetFBConfigAttrib"));
	s_glxInfo.GetClientString = function_cast<PFNGLXGETCLIENTSTRINGPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXGetClientString"));
	s_glxInfo.QueryExtension = function_cast<PFNGLXQUERYEXTENSIONPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXQueryExtension"));
	s_glxInfo.QueryVersion = function_cast<PFNGLXQUERYVERSIONPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXQueryVersion"));
	s_glxInfo.DestroyContext = function_cast<PFNGLXDESTROYCONTEXTPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXDestroyContext"));
	s_glxInfo.MakeCurrent = function_cast<PFNGLXMAKECURRENTPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXMakeCurrent"));
	s_glxInfo.SwapBuffers = function_cast<PFNGLXSWAPBUFFERSPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXSwapBuffers"));
	s_glxInfo.QueryExtensionsString = function_cast<PFNGLXQUERYEXTENSIONSSTRINGPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXQueryExtensionsString"));
	s_glxInfo.CreateNewContext = function_cast<PFNGLXCREATENEWCONTEXTPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXCreateNewContext"));
	s_glxInfo.CreateWindow = function_cast<PFNGLXCREATEWINDOWPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXCreateWindow"));
	s_glxInfo.DestroyWindow = function_cast<PFNGLXDESTROYWINDOWPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXDestroyWindow"));
	s_glxInfo.GetProcAddress = function_cast<PFNGLXGETPROCADDRESSPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXGetProcAddress"));
	s_glxInfo.GetProcAddressARB = function_cast<PFNGLXGETPROCADDRESSPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXGetProcAddressARB"));
	s_glxInfo.GetVisualFromFBConfig = function_cast<PFNGLXGETVISUALFROMFBCONFIGPROC>(getSharedLibrarySymbol(s_glxInfo.library, "glXGetVisualFromFBConfig"));
	assert(s_glxInfo.GetFBConfigs != nullptr);
	assert(s_glxInfo.GetFBConfigAttrib != nullptr);
	assert(s_glxInfo.GetClientString != nullptr);
	assert(s_glxInfo.QueryExtension != nullptr);
	assert(s_glxInfo.QueryVersion != nullptr);
	assert(s_glxInfo.DestroyContext != nullptr);
	assert(s_glxInfo.MakeCurrent != nullptr);
	assert(s_glxInfo.SwapBuffers != nullptr);
	assert(s_glxInfo.QueryExtensionsString != nullptr);
	assert(s_glxInfo.CreateNewContext != nullptr);
	assert(s_glxInfo.CreateWindow != nullptr);
	assert(s_glxInfo.DestroyWindow != nullptr);
	assert(s_glxInfo.GetProcAddress != nullptr);
	assert(s_glxInfo.GetProcAddressARB != nullptr);
	assert(s_glxInfo.GetVisualFromFBConfig != nullptr);

	if (!s_glxInfo.QueryExtension(display, &s_glxInfo.errorBase, &s_glxInfo.eventBase)) {
		throw std::runtime_error("GLX: GLX extension not found");
	}

	if (!s_glxInfo.QueryVersion(display, &s_glxInfo.major, &s_glxInfo.minor)) {
		throw std::runtime_error("GLX: Failed to query GLX version");
	}

	if (s_glxInfo.major == 1 && s_glxInfo.minor < 3) {
		throw std::runtime_error("GLX: GLX version 1.3 required");
	}

	if (isExtensionSupported("GLX_EXT_swap_control")) {
		s_glxInfo.SwapIntervalEXT = function_cast<PFNGLXSWAPINTERVALEXTPROC>(getProcAddress("glXSwapIntervalEXT"));
		s_glxInfo.EXT_swap_control = s_glxInfo.SwapIntervalEXT != nullptr;
	}
	if (isExtensionSupported("GLX_SGI_swap_control")) {
		s_glxInfo.SwapIntervalSGI = function_cast<PFNGLXSWAPINTERVALSGIPROC>(getProcAddress("glXSwapIntervalSGI"));
		s_glxInfo.SGI_swap_control = s_glxInfo.SwapIntervalSGI != nullptr;
	}
	if (isExtensionSupported("GLX_MESA_swap_control")) {
		s_glxInfo.SwapIntervalMESA = function_cast<PFNGLXSWAPINTERVALMESAPROC>(getProcAddress("glXSwapIntervalMESA"));
		s_glxInfo.MESA_swap_control = s_glxInfo.SwapIntervalMESA != nullptr;
	}
	s_glxInfo.ARB_multisample = isExtensionSupported("GLX_ARB_multisample");
	s_glxInfo.ARB_framebuffer_sRGB = isExtensionSupported("GLX_ARB_framebuffer_sRGB");
	s_glxInfo.EXT_framebuffer_sRGB = isExtensionSupported("GLX_EXT_framebuffer_sRGB");
	if (isExtensionSupported("GLX_ARB_create_context")) {
		s_glxInfo.CreateContextAttribsARB = function_cast<PFNGLXCREATECONTEXTATTRIBSARBPROC>(getProcAddress("glXCreateContextAttribsARB"));
		s_glxInfo.ARB_create_context = s_glxInfo.CreateContextAttribsARB != nullptr;
	}
	s_glxInfo.ARB_create_context_robustness = isExtensionSupported("GLX_ARB_create_context_robustness");
	s_glxInfo.ARB_create_context_profile = isExtensionSupported("GLX_ARB_create_context_profile");
	s_glxInfo.EXT_create_context_es2_profile = isExtensionSupported("GLX_EXT_create_context_es2_profile");
	s_glxInfo.ARB_context_flush_control = isExtensionSupported("GLX_ARB_context_flush_control");

	// also dynamically load Xlib
	s_glxInfo.xlibLibrary = openSharedLibrary("libX11.so", SHLL_LAZY_BIT);

	if (s_glxInfo.xlibLibrary == nullptr) {
		throw std::runtime_error("GLX: Failed to load libX11.so");
	}

	s_glxInfo.XFree = function_cast<PFNXFREEPROC>(getSharedLibrarySymbol(s_glxInfo.xlibLibrary, "XFree"));
	assert(s_glxInfo.XFree != nullptr);
}

void terminateGLX()
{
	if (s_glxInfo.xlibLibrary != nullptr) {
		m0tools::closeSharedLibrary(s_glxInfo.xlibLibrary);
		s_glxInfo.xlibLibrary = nullptr;
	}
	if (s_glxInfo.library != nullptr) {
		m0tools::closeSharedLibrary(s_glxInfo.library);
		s_glxInfo.library = nullptr;
	}
}

#define setGLXattrib(attribName, attribValue) \
{ \
	attribs[index++] = attribName; \
	attribs[index++] = attribValue; \
	assert((size_t) index < sizeof(attribs) / sizeof(attribs[0])); \
}

class GLXSwapchain : public Swapchain
{
public:
	GLXSwapchain(GLXContextInfo& ctx) : m_ctx(&ctx)
	{}

	void present() override
	{
		s_glxInfo.SwapBuffers(s_glxInfo.display, m_ctx->window);
	}

private:
	GLXContextInfo* m_ctx;
};

static void makeContextCurrent(GLXContextInfo& ctx)
{
	if (!s_glxInfo.MakeCurrent(s_glxInfo.display, ctx.window, ctx.handle)) {
		throw std::runtime_error("GLX: Failed to make context current");
	}
}

void createContextGLX(GLXContextInfo& ctx, std::shared_ptr<Swapchain>& swapchain, const RenderingAPICreateInfo& createInfo)
{
	int attribs[40];
	GLXFBConfig native = NULL;

	chooseFBConfig(native, ctx, createInfo.framebufferSettings);

	if (!s_glxInfo.ARB_create_context || !s_glxInfo.ARB_create_context_profile) {
		throw std::runtime_error("GLX: GLX_ARB_create_context and/or GLX_ARB_create_context_profile not supported");
	}

	int index = 0, mask = 0, flags = 0;

	mask |= GLX_CONTEXT_CORE_PROFILE_BIT_ARB;
	// TODO: debug

	setGLXattrib(GLX_CONTEXT_MAJOR_VERSION_ARB, 4);
	setGLXattrib(GLX_CONTEXT_MINOR_VERSION_ARB, 3);

	if (mask > 0) {
		setGLXattrib(GLX_CONTEXT_PROFILE_MASK_ARB, mask);
	}

	if (flags > 0) {
		setGLXattrib(GLX_CONTEXT_FLAGS_ARB, flags);
	}

	setGLXattrib(None, None);

	ctx.handle = s_glxInfo.CreateContextAttribsARB(createInfo.surface->xlib.display, native, NULL, True, attribs);
	if (ctx.handle == nullptr) {
		throw std::runtime_error("GLX: Failed to create a core profile context");
	}

	// FIXME: don't use this API because we have no control over the visual
	// that is used during creation of the window that we get from the client
	/*ctx.window = s_glxInfo.CreateWindow(createInfo.surface->xlib.display, native, createInfo.surface->xlib.window, NULL);
	if (ctx.window == None) {
		throw std::runtime_error("GLX: Failed to create a GLX window");
	}*/
	ctx.window = createInfo.surface->xlib.window;
	makeContextCurrent(ctx);

	swapchain = std::make_shared<GLXSwapchain>(ctx);
}

void destroyContextGLX(GLXContextInfo& ctx)
{
	/*if (ctx.window != None) {
		s_glxInfo.DestroyWindow(s_glxInfo.display, ctx.window);
		ctx.window = None;
	}*/
	if (ctx.handle != nullptr) {
		s_glxInfo.DestroyContext(s_glxInfo.display, ctx.handle);
		ctx.handle = nullptr;
	}
}

} // namespace rendercore
