//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-render-queue.hpp>

#include <rendercore/gl4-render-dispatcher.hpp>
#include <rendercore/gl4-render-storage.hpp>

namespace rendercore {

GL4RenderQueue::GL4RenderQueue(GL4RenderDispatcher& dispatcher)
	: m_dispatcher(&dispatcher)
{}

GL4RenderQueue::~GL4RenderQueue()
{}

void GL4RenderQueue::enqueue(const RenderStorage* storage)
{
	m_storageQueue.push_back(storage);
}

void GL4RenderQueue::flush()
{
	for (const RenderStorage* storage : m_storageQueue) {
		m_dispatcher->dispatch(static_cast<const GL4RenderStorage*>(storage));
	}

	// done with all storages
	m_storageQueue.clear();
}

} // namespace rendercore