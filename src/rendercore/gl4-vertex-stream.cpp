//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-vertex-stream.hpp>
#include <cassert>
#include <rendercore/gl4-graphics-memory.hpp>
#include <rendercore/gl4-render-dispatcher.hpp>
#include <rendercore/gl4-data-format.hpp>

namespace rendercore {

GL4VertexStream::GL4VertexStream()
	: m_empty(true), m_dispatcher(nullptr), m_vsBindInfo(), m_drawInfo()
{
}

GL4VertexStream::GL4VertexStream(const VertexStreamCreateInfo& createInfo, GL4RenderDispatcher& dispatcher)
	: m_empty(false), m_dispatcher(&dispatcher), m_vsBindInfo(), m_drawInfo()
{
	// invalidate GL state
	m_dispatcher->invalidateVertexStream();

	m_vsBindInfo.indexBuffer = (static_cast<const GL4GraphicsMemory*>(createInfo.indexMemoryViewInfo.mem))->m_obj;

	for (uint32_t i = 0; i < createInfo.numVertexMemoryViewInfos; ++i) {
		const VertexMemoryViewInfo& viewInfo = createInfo.firstVertexMemoryViewInfo[i];
		m_vsBindInfo.vertexBufferBindInfos[i].vertexBuffer = (static_cast<const GL4GraphicsMemory*>(viewInfo.mem))->m_obj;
		m_vsBindInfo.vertexBufferBindInfos[i].binding = static_cast<GLuint>(viewInfo.binding);
		m_vsBindInfo.vertexBufferBindInfos[i].offset = static_cast<GLuint>(viewInfo.offset);
	}
	m_vsBindInfo.vertexBufferBindInfoCount = createInfo.numVertexMemoryViewInfos;

	m_drawInfo.count = static_cast<GLsizei>(createInfo.indexMemoryViewInfo.numIndices);
	m_drawInfo.type = translateNumFormat(createInfo.indexMemoryViewInfo.format);
	assert(m_drawInfo.type == GL_UNSIGNED_BYTE || m_drawInfo.type == GL_UNSIGNED_SHORT || m_drawInfo.type == GL_UNSIGNED_INT);
	m_drawInfo.ptr = (const GLvoid*)(uintptr_t)createInfo.indexMemoryViewInfo.offset;
}

GL4VertexStream::~GL4VertexStream()
{
	release();
}

GL4VertexStream::GL4VertexStream(GL4VertexStream&& other)
	: m_empty(other.m_empty), m_dispatcher(other.m_dispatcher), m_vsBindInfo(other.m_vsBindInfo), m_drawInfo(other.m_drawInfo)
{
	other.release();
}

GL4VertexStream& GL4VertexStream::operator=(GL4VertexStream&& other)
{
	if (&other != this) {
		m_empty = other.m_empty;
		m_dispatcher = other.m_dispatcher;
		m_vsBindInfo = other.m_vsBindInfo;
		m_drawInfo = other.m_drawInfo;
		other.release();
	}
	return *this;
}

void GL4VertexStream::release()
{
	m_empty = true;
}

} // namespace rendercore