//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-texture.hpp>
#include <cassert>
#include <rendercore/gl4-render-dispatcher.hpp>
#include <rendercore/gl4-data-format.hpp>

namespace rendercore {

GL4Texture::GL4Texture()
	: m_texture(0), m_dispatcher(nullptr), m_updateInfo(), m_bindInfo()
{
}

static GLenum translateTextureType(TextureType type, int arraySize)
{
	switch (type) {
		case TextureType::Texture1D:
			return arraySize > 1 ? GL_TEXTURE_1D_ARRAY : GL_TEXTURE_1D;
		case TextureType::Texture2D:
			return arraySize > 1 ? GL_TEXTURE_2D_ARRAY : GL_TEXTURE_2D;
		case TextureType::Texture3D:
			return GL_TEXTURE_3D;
		case TextureType::TextureCubemap:
			return arraySize > 1 ? GL_TEXTURE_CUBE_MAP_ARRAY : GL_TEXTURE_CUBE_MAP;
		default:
			assert(0);
			break;
	}
	return GL_TEXTURE_2D;
}

static size_t getPixelByteSize(NumberFormat numFmt, PixelFormat pixFmt)
{
	return getNumberFormatByteSize(numFmt) * getNumPixelFormatComponents(pixFmt);
}

GL4Texture::GL4Texture(const TextureCreateInfo& createInfo, GL4RenderDispatcher& dispatcher)
	: m_texture(0), m_dispatcher(&dispatcher), m_updateInfo(), m_bindInfo()
{
	assert(createInfo.arraySize > 0);
	assert(createInfo.type != TextureType::Texture3D || createInfo.arraySize == 1);

	glGenTextures(1, &m_texture);
	m_bindInfo.target = translateTextureType(createInfo.type, createInfo.arraySize);
	m_dispatcher->invalidateTextureSet();
	glBindTexture(m_bindInfo.target, m_texture);
	glTexParameteri(m_bindInfo.target, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(m_bindInfo.target, GL_TEXTURE_MAX_LEVEL, createInfo.mipLevels - 1);

	// specify the texture
	GLenum internalFormat;
	translateNumberAndPixelFormat(createInfo.format.numFormat, createInfo.format.pxFormat, internalFormat, m_updateInfo.format, m_updateInfo.dataType, m_updateInfo.isCompressed);
	m_updateInfo.mipLevels = createInfo.mipLevels;
	m_updateInfo.size = createInfo.size;
	// if the texture is compressed, specify the images later
	if (m_updateInfo.isCompressed) {
		m_updateInfo.format = internalFormat;
	} else {
		m_updateInfo.pixelSizeBytes = getPixelByteSize(createInfo.format.numFormat, createInfo.format.pxFormat);
		switch (createInfo.type) {
			case TextureType::Texture1D:
				if (createInfo.arraySize > 1) {
					// GL 4.3+
					glTexStorage2D(m_bindInfo.target, createInfo.mipLevels, internalFormat, createInfo.size.width, createInfo.arraySize);
					m_updateInfo.size.height = createInfo.arraySize;
				} else {
					glTexStorage1D(m_bindInfo.target, createInfo.mipLevels, internalFormat, createInfo.size.width);
				}
				break;
			case TextureType::Texture2D:
			case TextureType::TextureCubemap:
				if (createInfo.arraySize > 1) {
					glTexStorage3D(m_bindInfo.target, createInfo.mipLevels, internalFormat, createInfo.size.width, createInfo.size.height, createInfo.arraySize);
					m_updateInfo.size.depth = createInfo.arraySize;
				} else {
					glTexStorage2D(m_bindInfo.target, createInfo.mipLevels, internalFormat, createInfo.size.width, createInfo.size.height);
				}
				break;
			case TextureType::Texture3D:
				glTexStorage3D(m_bindInfo.target, createInfo.mipLevels, internalFormat, createInfo.size.width, createInfo.size.height, createInfo.size.depth);
				break;
		}
	}
}

GL4Texture::~GL4Texture()
{
	release();
}

GL4Texture::GL4Texture(GL4Texture&& other)
	: m_texture(other.m_texture), m_dispatcher(other.m_dispatcher), m_updateInfo(other.m_updateInfo), m_bindInfo(other.m_bindInfo)
{
	other.m_texture = 0;
}

GL4Texture& GL4Texture::operator=(GL4Texture&& other)
{
	if (&other != this) {
		m_texture = other.m_texture;
		m_dispatcher = other.m_dispatcher;
		m_updateInfo = other.m_updateInfo;
		m_bindInfo = other.m_bindInfo;
		other.m_texture = 0;
	}
	return *this;
}

static GLint getRowLengthAndAlignment(size_t rowStride, size_t pixelSizeBytes, uint32_t width, GLint& alignment)
{
	auto unpaddedRowStride = width * pixelSizeBytes;
	auto padding = rowStride - unpaddedRowStride;
	if ((rowStride % 8 == padding) || (rowStride % 8 == 0)) {
		alignment = 8;
	} else if ((rowStride % 4 == padding) || (rowStride % 4 == 0)) {
		alignment = 4;
	} else if ((rowStride % 2 == padding) || (rowStride % 2 == 0)) {
		alignment = 2;
	} else {
		alignment = 1;
	}
	return static_cast<GLint>(rowStride / pixelSizeBytes);
}

void GL4Texture::update1D(uint32_t mip, int offset, uint32_t size, size_t dataSize, const void* data, size_t rowStride)
{
	assert(m_bindInfo.target == GL_TEXTURE_1D);
	assert(mip < m_updateInfo.mipLevels);
	m_dispatcher->invalidateTextureSet();
	glBindTexture(m_bindInfo.target, m_texture);
	if (m_updateInfo.isCompressed) {
		assert(offset == 0); // offsets are not supported since we're not specifying a sub-image
		glCompressedTexImage1D(m_bindInfo.target, static_cast<GLint>(mip), m_updateInfo.format, size, 0, static_cast<GLsizei>(dataSize), data);
	} else {
		GLint alignment;
		glPixelStorei(GL_UNPACK_ROW_LENGTH, getRowLengthAndAlignment(rowStride, m_updateInfo.pixelSizeBytes, size, alignment));
		glPixelStorei(GL_UNPACK_ALIGNMENT, alignment);
		glTexSubImage1D(m_bindInfo.target, static_cast<GLint>(mip), offset, size, m_updateInfo.format, m_updateInfo.dataType, data);
	}
}

void GL4Texture::update1DArray(uint32_t mip, uint32_t arrayIndex, int offset, uint32_t size, size_t dataSize, const void* data, size_t rowStride)
{
	update2D(mip, offset, arrayIndex, size, 1, dataSize, data, rowStride);
}

void GL4Texture::update2D(uint32_t mip, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride)
{
	assert(m_bindInfo.target == GL_TEXTURE_2D || m_bindInfo.target == GL_TEXTURE_1D_ARRAY);
	assert(mip < m_updateInfo.mipLevels);
	m_dispatcher->invalidateTextureSet();
	glBindTexture(m_bindInfo.target, m_texture);
	if (m_updateInfo.isCompressed) {
		assert((offsetX == 0) && (offsetY == 0)); // offsets are not supported since we're not specifying a sub-image
		glCompressedTexImage2D(m_bindInfo.target, static_cast<GLint>(mip), m_updateInfo.format, width, height, 0, static_cast<GLsizei>(dataSize), data);
	} else {
		GLint alignment;
		glPixelStorei(GL_UNPACK_ROW_LENGTH, getRowLengthAndAlignment(rowStride, m_updateInfo.pixelSizeBytes, width, alignment));
		glPixelStorei(GL_UNPACK_ALIGNMENT, alignment);
		glTexSubImage2D(m_bindInfo.target, static_cast<GLint>(mip), offsetX, offsetY, width, height, m_updateInfo.format, m_updateInfo.dataType, data);
	}
}

void GL4Texture::update2DArray(uint32_t mip, uint32_t arrayIndex, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride)
{
	update3D(mip, offsetX, offsetY, arrayIndex, width, height, 1, dataSize, data, rowStride);
}

void GL4Texture::update3D(uint32_t mip, int offsetX, int offsetY, int offsetZ, uint32_t width, uint32_t height, uint32_t depth, size_t dataSize, const void* data, size_t rowStride)
{
	assert(m_bindInfo.target == GL_TEXTURE_3D || m_bindInfo.target == GL_TEXTURE_2D_ARRAY || m_bindInfo.target == GL_TEXTURE_CUBE_MAP_ARRAY);
	assert(mip < m_updateInfo.mipLevels);
	m_dispatcher->invalidateTextureSet();
	glBindTexture(m_bindInfo.target, m_texture);
	if (m_updateInfo.isCompressed) {
		assert((offsetX == 0) && (offsetY == 0) && (offsetZ == 0)); // offsets are not supported since we're not specifying a sub-image
		glCompressedTexImage3D(m_bindInfo.target, static_cast<GLint>(mip), m_updateInfo.format, width, height, depth, 0, static_cast<GLsizei>(dataSize), data);
	} else {
		GLint alignment;
		glPixelStorei(GL_UNPACK_ROW_LENGTH, getRowLengthAndAlignment(rowStride, m_updateInfo.pixelSizeBytes, width, alignment));
		glPixelStorei(GL_UNPACK_ALIGNMENT, alignment);
		glTexSubImage3D(m_bindInfo.target, static_cast<GLint>(mip), offsetX, offsetY, offsetZ, width, height, depth, m_updateInfo.format, m_updateInfo.dataType, data);
	}
}

GLenum translateCubemapFace(CubemapFace face)
{
	switch (face) {
		case CubemapFace::PositiveX:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
		case CubemapFace::NegativeX:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
		case CubemapFace::PositiveY:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
		case CubemapFace::NegativeY:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
		case CubemapFace::PositiveZ:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
		case CubemapFace::NegativeZ:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
		case CubemapFace::Max:
		default:
			assert(0);
			break;
	}
	return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
}

void GL4Texture::updateCubemapFace(uint32_t mip, CubemapFace face, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride)
{
	assert(m_bindInfo.target == GL_TEXTURE_CUBE_MAP);
	assert(mip < m_updateInfo.mipLevels);
	m_dispatcher->invalidateTextureSet();
	glBindTexture(m_bindInfo.target, m_texture);
	if (m_updateInfo.isCompressed) {
		assert((offsetX == 0) && (offsetY == 0)); // offsets are not supported since we're not specifying a sub-image
		glCompressedTexImage2D(translateCubemapFace(face), static_cast<GLint>(mip), m_updateInfo.format, width, height, 0, static_cast<GLsizei>(dataSize), data);
	} else {
		GLint alignment;
		glPixelStorei(GL_UNPACK_ROW_LENGTH, getRowLengthAndAlignment(rowStride, m_updateInfo.pixelSizeBytes, width, alignment));
		glPixelStorei(GL_UNPACK_ALIGNMENT, alignment);
		glTexSubImage2D(translateCubemapFace(face), static_cast<GLint>(mip), offsetX, offsetY, width, height, m_updateInfo.format, m_updateInfo.dataType, data);
	}
}

void GL4Texture::updateCubemapArrayFace(uint32_t mip, uint32_t arrayIndex, CubemapFace face, int offsetX, int offsetY, uint32_t width, uint32_t height, size_t dataSize, const void* data, size_t rowStride)
{
	update3D(mip, offsetX, offsetY, arrayIndex * static_cast<CubemapFaceUType>(CubemapFace::Max) + static_cast<CubemapFaceUType>(face), width, height, 1, dataSize, data, rowStride);
}

void GL4Texture::release()
{
	if (!empty()) {
		glDeleteTextures(1, &m_texture);
	}
	m_texture = 0;
}

} // namespace rendercore