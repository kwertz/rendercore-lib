//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/rendering-api-factory.hpp>

#include <rendercore/gl4-rendering-api.hpp>
#include <rendercore/vk-rendering-api.hpp>

namespace rendercore {

std::unique_ptr<RenderingAPI> RenderingAPIFactory::createRenderingAPI(RenderingAPIImplementation implementation, const RenderingAPICreateInfo& createInfo)
{
	switch (implementation) {
		case RenderingAPIImplementation::OpenGL4:
			return std::unique_ptr<GL4RenderingAPI>(new GL4RenderingAPI(createInfo));
		case RenderingAPIImplementation::Vulkan:
			return std::unique_ptr<VulkanRenderingAPI>(new VulkanRenderingAPI(createInfo));
		default:
			return std::unique_ptr<RenderingAPI>();
	}
}

} // namespace rendercore