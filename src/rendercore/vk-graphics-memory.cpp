//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-graphics-memory.hpp>

#include <cassert>
#include <cstring>
#include <rendercore/vk-platform.hpp>

namespace rendercore {

static bool getMemoryTypeFromProperties(VkPhysicalDeviceMemoryProperties memProps, uint32_t typeBits, VkFlags requirementsMask, uint32_t* typeIndex)
{
	for (uint32_t i = 0; i < VK_MAX_MEMORY_TYPES; ++i) {
		if ((typeBits & 1) == 1) {
			if ((memProps.memoryTypes[i].propertyFlags & requirementsMask) == requirementsMask) {
				*typeIndex = i;
				return true;
			}
		}
		typeBits >>= 1;
	}
	return false;
}

VulkanGraphicsMemory::VulkanGraphicsMemory()
	: m_device(VK_NULL_HANDLE), m_vk(nullptr), m_usage(0), m_memProps(nullptr),
	  m_buffer(VK_NULL_HANDLE), m_deviceMem(VK_NULL_HANDLE)
{}

VulkanGraphicsMemory::VulkanGraphicsMemory(VkDevice device, VulkanFunctions& vk, VkBufferUsageFlags usage, const VkPhysicalDeviceMemoryProperties& memProps)
	: m_device(device), m_vk(&vk), m_usage(usage), m_memProps(&memProps),
	  m_buffer(VK_NULL_HANDLE), m_deviceMem(VK_NULL_HANDLE)
{}

VulkanGraphicsMemory::~VulkanGraphicsMemory()
{
	release();
}

void VulkanGraphicsMemory::reallocate(size_t size)
{
	// release our existing buffer object and the associated
	// device memory first before recreating them
	releaseMemory();

	VkBufferCreateInfo vbCreateInfo = {};
	vbCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	vbCreateInfo.pNext = nullptr;
	vbCreateInfo.flags = 0;
	vbCreateInfo.usage = m_usage;
	vbCreateInfo.size = static_cast<VkDeviceSize>(size);
	vbCreateInfo.queueFamilyIndexCount = 0;
	vbCreateInfo.pQueueFamilyIndices = nullptr;
	vbCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	VKCHECK(m_vk->CreateBuffer(m_device, &vbCreateInfo, nullptr, &m_buffer));

	VkMemoryRequirements memReqs;
	m_vk->GetBufferMemoryRequirements(m_device, m_buffer, &memReqs);

	VkMemoryAllocateInfo memAllocInfo = {};
	memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memAllocInfo.pNext = nullptr;
	memAllocInfo.memoryTypeIndex = 0;
	memAllocInfo.allocationSize = memReqs.size;
	bool ok = getMemoryTypeFromProperties(*m_memProps, memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &memAllocInfo.memoryTypeIndex);
	assert(ok);

	VKCHECK(m_vk->AllocateMemory(m_device, &memAllocInfo, nullptr, &m_deviceMem));
	VKCHECK(m_vk->BindBufferMemory(m_device, m_buffer, m_deviceMem, 0));
}

CPUMemoryPointer VulkanGraphicsMemory::pin(ptrdiff_t offset, size_t length, int flags)
{
	assert(!empty());

	if (m_deviceMem == VK_NULL_HANDLE) {
		return nullptr;
	}

	void* dataPtr;
	VKCHECK(m_vk->MapMemory(m_device, m_deviceMem, 0, static_cast<VkDeviceSize>(length), 0, &dataPtr));

	return dataPtr;
}

void VulkanGraphicsMemory::unpin()
{
	m_vk->UnmapMemory(m_device, m_deviceMem);
}

void VulkanGraphicsMemory::updateData(ptrdiff_t offset, size_t length, const void* data)
{
	CPUMemoryPointer dataPtr = pin(offset, length, 0);
	std::memcpy(dataPtr, data, length);
	unpin();
}

size_t VulkanGraphicsMemory::getCurrentSize() const
{
	VkMemoryRequirements memReqs;
	m_vk->GetBufferMemoryRequirements(m_device, m_buffer, &memReqs);
	return static_cast<size_t>(memReqs.size);
}

void VulkanGraphicsMemory::release()
{
	if (!empty()) {
		releaseMemory();
		m_device = VK_NULL_HANDLE;
		m_vk = nullptr;
		m_usage = 0;
		m_memProps = nullptr;
		m_buffer = VK_NULL_HANDLE;
		m_deviceMem = VK_NULL_HANDLE;
	}
}

void VulkanGraphicsMemory::releaseMemory()
{
	if (m_deviceMem != VK_NULL_HANDLE) {
		m_vk->FreeMemory(m_device, m_deviceMem, nullptr);
	}
	if (m_buffer != VK_NULL_HANDLE) {
		m_vk->DestroyBuffer(m_device, m_buffer, nullptr);
	}
}

} // namespace rendercore
