//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#define UNICODE
#include <rendercore/gl4-wgl-context.hpp>

#include <cassert>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <m0tools/shared-library.hpp>
#include <rendercore/rendering-api.hpp>
#include <rendercore/platform-surface.hpp>

namespace rendercore {

#define WGL_NUMBER_PIXEL_FORMATS_ARB 0x2000
#define WGL_SUPPORT_OPENGL_ARB 0x2010
#define WGL_DRAW_TO_WINDOW_ARB 0x2001
#define WGL_PIXEL_TYPE_ARB 0x2013
#define WGL_TYPE_RGBA_ARB 0x202b
#define WGL_ACCELERATION_ARB 0x2003
#define WGL_NO_ACCELERATION_ARB 0x2025
#define WGL_RED_BITS_ARB 0x2015
#define WGL_RED_SHIFT_ARB 0x2016
#define WGL_GREEN_BITS_ARB 0x2017
#define WGL_GREEN_SHIFT_ARB 0x2018
#define WGL_BLUE_BITS_ARB 0x2019
#define WGL_BLUE_SHIFT_ARB 0x201a
#define WGL_ALPHA_BITS_ARB 0x201b
#define WGL_ALPHA_SHIFT_ARB 0x201c
#define WGL_ACCUM_BITS_ARB 0x201d
#define WGL_ACCUM_RED_BITS_ARB 0x201e
#define WGL_ACCUM_GREEN_BITS_ARB 0x201f
#define WGL_ACCUM_BLUE_BITS_ARB 0x2020
#define WGL_ACCUM_ALPHA_BITS_ARB 0x2021
#define WGL_DEPTH_BITS_ARB 0x2022
#define WGL_STENCIL_BITS_ARB 0x2023
#define WGL_AUX_BUFFERS_ARB 0x2024
#define WGL_STEREO_ARB 0x2012
#define WGL_DOUBLE_BUFFER_ARB 0x2011
#define WGL_SAMPLES_ARB 0x2042
#define WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB 0x20a9
#define WGL_CONTEXT_DEBUG_BIT_ARB 0x00000001
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x00000002
#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126
#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002
#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define WGL_CONTEXT_ES2_PROFILE_BIT_EXT 0x00000004
#define WGL_CONTEXT_ROBUST_ACCESS_BIT_ARB 0x00000004
#define WGL_LOSE_CONTEXT_ON_RESET_ARB 0x8252
#define WGL_CONTEXT_RESET_NOTIFICATION_STRATEGY_ARB 0x8256
#define WGL_NO_RESET_NOTIFICATION_ARB 0x8261
#define WGL_CONTEXT_RELEASE_BEHAVIOR_ARB 0x2097
#define WGL_CONTEXT_RELEASE_BEHAVIOR_NONE_ARB 0
#define WGL_CONTEXT_RELEASE_BEHAVIOR_FLUSH_ARB 0x2098

typedef BOOL (WINAPI* PFNWGLSWAPINTERVALEXTPROC)(int);
typedef BOOL (WINAPI* PFNWGLGETPIXELFORMATATTRIBIVARBPROC)(HDC, int, int, UINT, const int*, int*);
typedef const char* (WINAPI* PFNWGLGETEXTENSIONSSTRINGEXTPROC)(void);
typedef const char* (WINAPI* PFNWGLGETEXTENSIONSSTRINGARBPROC)(HDC);
typedef HGLRC(WINAPI* PFNWGLCREATECONTEXTATTRIBSARBPROC)(HDC, HGLRC, const int*);

typedef HGLRC(WINAPI* WGLCREATECONTEXT_T)(HDC);
typedef BOOL (WINAPI* WGLDELETECONTEXT_T)(HGLRC);
typedef PROC(WINAPI* WGLGETPROCADDRESS_T)(LPCSTR);
typedef BOOL (WINAPI* WGLMAKECURRENT_T)(HDC, HGLRC);
typedef BOOL(WINAPI* SWAPBUFFERS_T)(HDC);

typedef HRESULT(WINAPI* DWMISCOMPOSITIONENABLED_T)(BOOL*);
typedef HRESULT(WINAPI* DWMFLUSH_T)(VOID);

struct WGLInfo {
	m0tools::SharedLibrary library;
	m0tools::SharedLibrary dwmLibrary;

	WGLCREATECONTEXT_T CreateContext;
	WGLDELETECONTEXT_T DeleteContext;
	WGLGETPROCADDRESS_T GetProcAddress;
	WGLMAKECURRENT_T MakeCurrent;

	bool extensionsLoaded;

	PFNWGLSWAPINTERVALEXTPROC SwapIntervalEXT;
	PFNWGLGETPIXELFORMATATTRIBIVARBPROC GetPixelFormatAttribivARB;
	PFNWGLGETEXTENSIONSSTRINGEXTPROC GetExtensionsStringEXT;
	PFNWGLGETEXTENSIONSSTRINGARBPROC GetExtensionsStringARB;
	PFNWGLCREATECONTEXTATTRIBSARBPROC CreateContextAttribsARB;
	bool EXT_swap_control;
	bool ARB_multisample;
	bool ARB_framebuffer_sRGB;
	bool EXT_framebuffer_sRGB;
	bool ARB_pixel_format;
	bool ARB_create_context;
	bool ARB_create_context_profile;
	bool EXT_create_context_es2_profile;
	bool ARB_create_context_robustness;
	bool ARB_context_flush_control;

	// the WGL context is the only user of this function, so put the pointer in here
	DWMISCOMPOSITIONENABLED_T DwmIsCompositionEnabled;
	DWMFLUSH_T DwmFlush;
};

static WGLInfo s_wglInfo = {};

static int getPixelFormatAttrib(WGLInfo& wgl, WGLContextInfo& ctx, int pixelFormat, int attrib)
{
	int value = 0;

	assert(wgl.ARB_pixel_format);

	if (!wgl.GetPixelFormatAttribivARB(ctx.hdc, pixelFormat, 0, 1, &attrib, &value)) {
		throw std::runtime_error("WGL: Failed to retrieve pixel format attribute");
	}

	return value;
}

static int choosePixelFormat(WGLInfo& wgl, WGLContextInfo& ctx, const FramebufferSettings& fbsettings)
{
	GLFramebufferConfig desired;
	makeGLFramebufferConfig(desired, fbsettings);

	int nativeCount;

	if (wgl.ARB_pixel_format) {
		nativeCount = getPixelFormatAttrib(wgl, ctx, 1, WGL_NUMBER_PIXEL_FORMATS_ARB);
	} else {
		nativeCount = DescribePixelFormat(ctx.hdc, 1, sizeof(PIXELFORMATDESCRIPTOR), NULL);
	}

	std::vector<GLFramebufferConfig> usableConfigs(nativeCount);
	int usableCount = 0;

	for (int i = 0; i < nativeCount; ++i) {
		const int n = i + 1;
		GLFramebufferConfig& usableCfg = usableConfigs[usableCount];
		if (wgl.ARB_pixel_format) {
			if (!getPixelFormatAttrib(wgl, ctx, n, WGL_SUPPORT_OPENGL_ARB) ||
			    !getPixelFormatAttrib(wgl, ctx, n, WGL_DRAW_TO_WINDOW_ARB)) {
				continue;
			}

			if (getPixelFormatAttrib(wgl, ctx, n, WGL_PIXEL_TYPE_ARB) != WGL_TYPE_RGBA_ARB) {
				continue;
			}

			if (getPixelFormatAttrib(wgl, ctx, n, WGL_ACCELERATION_ARB) == WGL_NO_ACCELERATION_ARB) {
				continue;
			}

			usableCfg.redBits = getPixelFormatAttrib(wgl, ctx, n, WGL_RED_BITS_ARB);
			usableCfg.greenBits = getPixelFormatAttrib(wgl, ctx, n, WGL_GREEN_BITS_ARB);
			usableCfg.blueBits = getPixelFormatAttrib(wgl, ctx, n, WGL_BLUE_BITS_ARB);
			usableCfg.alphaBits = getPixelFormatAttrib(wgl, ctx, n, WGL_ALPHA_BITS_ARB);

			usableCfg.depthBits = getPixelFormatAttrib(wgl, ctx, n, WGL_DEPTH_BITS_ARB);
			usableCfg.stencilBits = getPixelFormatAttrib(wgl, ctx, n, WGL_STENCIL_BITS_ARB);

			usableCfg.accumRedBits = getPixelFormatAttrib(wgl, ctx, n, WGL_ACCUM_RED_BITS_ARB);
			usableCfg.accumGreenBits = getPixelFormatAttrib(wgl, ctx, n, WGL_ACCUM_GREEN_BITS_ARB);
			usableCfg.accumBlueBits = getPixelFormatAttrib(wgl, ctx, n, WGL_ACCUM_BLUE_BITS_ARB);
			usableCfg.accumAlphaBits = getPixelFormatAttrib(wgl, ctx, n, WGL_ACCUM_ALPHA_BITS_ARB);

			usableCfg.auxBuffers = getPixelFormatAttrib(wgl, ctx, n, WGL_AUX_BUFFERS_ARB);

			usableCfg.stereo = getPixelFormatAttrib(wgl, ctx, n, WGL_STEREO_ARB) == 1;
			usableCfg.doublebuffer = getPixelFormatAttrib(wgl, ctx, n, WGL_DOUBLE_BUFFER_ARB) == 1;

			if (wgl.ARB_multisample) {
				usableCfg.samples = getPixelFormatAttrib(wgl, ctx, n, WGL_SAMPLES_ARB);
			}

			if (wgl.ARB_framebuffer_sRGB || wgl.EXT_framebuffer_sRGB) {
				usableCfg.sRGB = getPixelFormatAttrib(wgl, ctx, n, WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB) == 1;
			}
		} else {
			PIXELFORMATDESCRIPTOR pfd;

			if (!DescribePixelFormat(ctx.hdc, n, sizeof(PIXELFORMATDESCRIPTOR), &pfd)) {
				continue;
			}

			if (!(pfd.dwFlags & PFD_DRAW_TO_WINDOW) ||
			    !(pfd.dwFlags & PFD_SUPPORT_OPENGL)) {
				continue;
			}

			if (pfd.iPixelType != PFD_TYPE_RGBA) {
				continue;
			}

			usableCfg.redBits = pfd.cRedBits;
			usableCfg.greenBits = pfd.cGreenBits;
			usableCfg.blueBits = pfd.cBlueBits;
			usableCfg.alphaBits = pfd.cAlphaBits;

			usableCfg.depthBits = pfd.cDepthBits;
			usableCfg.stencilBits = pfd.cStencilBits;

			usableCfg.accumRedBits = pfd.cAccumRedBits;
			usableCfg.accumGreenBits = pfd.cAccumGreenBits;
			usableCfg.accumBlueBits = pfd.cAccumBlueBits;
			usableCfg.accumAlphaBits = pfd.cAccumAlphaBits;

			usableCfg.auxBuffers = pfd.cAuxBuffers;

			usableCfg.stereo = (pfd.dwFlags & PFD_STEREO) > 0;
			usableCfg.doublebuffer = (pfd.dwFlags & PFD_DOUBLEBUFFER) > 0;
		}

		usableCfg.handle = n;
		++usableCount;
	}

	if (usableCount == 0) {
		throw std::runtime_error("WGL: The driver does not appear to support OpenGL");
	}

	usableConfigs.resize(usableCount);
	const GLFramebufferConfig& closest = chooseFramebufferConfig(desired, usableConfigs);

	return static_cast<int>(closest.handle);
}

static bool isCompositionEnabled(WGLInfo& wgl)
{
	if (wgl.DwmIsCompositionEnabled == nullptr) {
		return false;
	}

	BOOL enabled;
	if (wgl.DwmIsCompositionEnabled(&enabled) != S_OK) {
		return false;
	}

	return enabled == TRUE;
}

static void makeContextCurrent(WGLInfo& wgl, WGLContextInfo& ctx)
{
	if (!wgl.MakeCurrent(ctx.hdc, ctx.hglrc)) {
		throw std::runtime_error("WGL: Failed to make context current");
	}
}

static void swapInterval(WGLInfo& wgl, WGLContextInfo& ctx, int interval)
{
	ctx.interval = interval;

	if (isCompositionEnabled(wgl)) {
		interval = 0;
	}

	if (wgl.EXT_swap_control) {
		wgl.SwapIntervalEXT(interval);
	}
}

static bool isExtensionSupported(WGLInfo& wgl, WGLContextInfo& ctx, const char* extension)
{
	const char* extensions = nullptr;

	if (wgl.GetExtensionsStringEXT) {
		extensions = wgl.GetExtensionsStringEXT();
	} else if (wgl.GetExtensionsStringARB) {
		extensions = wgl.GetExtensionsStringARB(ctx.hdc);
	}

	if (extensions != nullptr) {
		return stringInExtensionString(extension, extensions);
	}

	return false;
}

static void* getProcAddress(WGLInfo& wgl, const char* procname)
{
	void* proc = reinterpret_cast<void*>(wgl.GetProcAddress(procname));
	if (proc != nullptr) {
		return proc;
	}

	return m0tools::getSharedLibrarySymbol(wgl.library, procname);
}

extern "C" void* rendercoreGLGetProcAddress(const char* procname)
{
	return getProcAddress(s_wglInfo, procname);
}

void destroyContextWGL(WGLContextInfo& ctx)
{
	if (ctx.hglrc) {
		BOOL ok = s_wglInfo.MakeCurrent(NULL, NULL);
		assert(ok);
		ok = s_wglInfo.DeleteContext(ctx.hglrc);
		assert(ok);
		ctx.hglrc = NULL;
	}
}

static void loadExtensions(WGLInfo& wgl, WGLContextInfo& ctx)
{
	using namespace m0tools;

	wgl.GetExtensionsStringEXT = function_cast<PFNWGLGETEXTENSIONSSTRINGEXTPROC>(getProcAddress(wgl, "wglGetExtensionsStringEXT"));
	wgl.GetExtensionsStringARB = function_cast<PFNWGLGETEXTENSIONSSTRINGARBPROC>(getProcAddress(wgl, "wglGetExtensionsStringARB"));

	// WGL_ARB_create_context
	wgl.CreateContextAttribsARB = function_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(getProcAddress(wgl, "wglCreateContextAttribsARB"));

	// WGL_EXT_swap_control
	wgl.SwapIntervalEXT = function_cast<PFNWGLSWAPINTERVALEXTPROC>(getProcAddress(wgl, "wglSwapIntervalEXT"));

	// WGL_ARB_pixel_format
	wgl.GetPixelFormatAttribivARB = function_cast<PFNWGLGETPIXELFORMATATTRIBIVARBPROC>(getProcAddress(wgl, "wglGetPixelFormatAttribivARB"));

	wgl.ARB_multisample = isExtensionSupported(wgl, ctx, "WGL_ARB_multisample");
	wgl.ARB_framebuffer_sRGB = isExtensionSupported(wgl, ctx, "WGL_ARB_framebuffer_sRGB");
	wgl.EXT_framebuffer_sRGB = isExtensionSupported(wgl, ctx, "WGL_EXT_framebuffer_sRGB");
	wgl.ARB_create_context = isExtensionSupported(wgl, ctx, "WGL_ARB_create_context");
	wgl.ARB_create_context_profile = isExtensionSupported(wgl, ctx, "WGL_ARB_create_context_profile");
	wgl.EXT_create_context_es2_profile = isExtensionSupported(wgl, ctx, "WGL_EXT_create_context_es2_profile");
	wgl.ARB_create_context_robustness = isExtensionSupported(wgl, ctx, "WGL_ARB_create_context_robustness");
	wgl.EXT_swap_control = isExtensionSupported(wgl, ctx, "WGL_EXT_swap_control");
	wgl.ARB_pixel_format = isExtensionSupported(wgl, ctx, "WGL_ARB_pixel_format");
	wgl.ARB_context_flush_control = isExtensionSupported(wgl, ctx, "WGL_ARB_context_flush_control");

	wgl.extensionsLoaded = true;
}

static LPCWSTR kHelperWndClass = L"RenderCoreWGL";

static void registerHelperWindowClass()
{
	WNDCLASSEXW wc = {};
	wc.cbSize = sizeof(WNDCLASSEXW);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = DefWindowProcW;
	wc.hInstance = GetModuleHandleW(NULL);
	wc.hCursor = LoadCursorW(NULL, IDC_ARROW);
	wc.lpszClassName = kHelperWndClass;
	wc.hIcon = (HICON)LoadImageW(NULL, IDI_APPLICATION, IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED);

	if (!RegisterClassExW(&wc)) {
		throw std::runtime_error("WGL: Failed to register helper window class");
	}
}

static void unregisterHelperWindowClass()
{
	UnregisterClassW(kHelperWndClass, GetModuleHandleW(NULL));
}

static HWND createHelperWindow(int x, int y)
{
	HWND window = CreateWindowExW(WS_EX_OVERLAPPEDWINDOW, kHelperWndClass, L"RenderCore Win32 GL helper window", WS_CLIPSIBLINGS | WS_CLIPCHILDREN, x, y, 1, 1, HWND_MESSAGE, NULL, GetModuleHandleW(NULL), NULL);
	if (window == NULL) {
		throw std::runtime_error("WGL: Cannot create a helper window for context creation");
	}
	return window;
}

void initializeWGL()
{
	using namespace m0tools;

	WGLInfo& wgl = s_wglInfo;

	registerHelperWindowClass();

	wgl.library = openSharedLibrary("opengl32.dll", 0);
	if (wgl.library == nullptr) {
		throw std::runtime_error("WGL: Failed to load opengl32.dll");
	}

	wgl.CreateContext = function_cast<WGLCREATECONTEXT_T>(getSharedLibrarySymbol(wgl.library, "wglCreateContext"));
	wgl.DeleteContext = function_cast<WGLDELETECONTEXT_T>(getSharedLibrarySymbol(wgl.library, "wglDeleteContext"));
	wgl.GetProcAddress = function_cast<WGLGETPROCADDRESS_T>(getSharedLibrarySymbol(wgl.library, "wglGetProcAddress"));
	wgl.MakeCurrent = function_cast<WGLMAKECURRENT_T>(getSharedLibrarySymbol(wgl.library, "wglMakeCurrent"));

	assert(wgl.CreateContext != nullptr);
	assert(wgl.DeleteContext != nullptr);
	assert(wgl.GetProcAddress != nullptr);
	assert(wgl.MakeCurrent != nullptr);

	wgl.dwmLibrary = openSharedLibrary("dwmapi.dll", 0);

	// these are optional
	if (wgl.dwmLibrary != nullptr) {
		wgl.DwmIsCompositionEnabled = function_cast<DWMISCOMPOSITIONENABLED_T>(getSharedLibrarySymbol(wgl.dwmLibrary, "DwmIsCompositionEnabled"));
		wgl.DwmFlush = function_cast<DWMFLUSH_T>(getSharedLibrarySymbol(wgl.dwmLibrary, "DwmFlush"));

		assert(wgl.DwmIsCompositionEnabled != nullptr);
		assert(wgl.DwmFlush != nullptr);
	}
}

void terminateWGL()
{
	WGLInfo& wgl = s_wglInfo;

	unregisterHelperWindowClass();

	if (wgl.dwmLibrary != nullptr) {
		m0tools::closeSharedLibrary(wgl.dwmLibrary);
	}
	if (wgl.library != nullptr) {
		m0tools::closeSharedLibrary(wgl.library);
	}
}

class WGLSwapchain : public Swapchain
{
public:
	WGLSwapchain(WGLContextInfo& ctx) : m_ctx(&ctx)
	{}

	void present() override
	{
		if (isCompositionEnabled(s_wglInfo)) {
			int count = std::abs(m_ctx->interval);
			while (count--) {
				HRESULT ok = s_wglInfo.DwmFlush();
				assert(ok == S_OK);
			}
		}

		SwapBuffers(m_ctx->hdc);
	}

private:
	WGLContextInfo* m_ctx;
};

#define setWGLattrib(attribName, attribValue) \
{ \
    attribs[index++] = attribName; \
    attribs[index++] = attribValue; \
    assert((size_t) index < sizeof(attribs) / sizeof(attribs[0])); \
}

static void createContextWGLImpl(WGLInfo& wgl, WGLContextInfo& ctx, const RenderingAPICreateInfo& createInfo)
{
	int attribs[40];
	PIXELFORMATDESCRIPTOR pfd;

	ctx.hdc = GetDC(createInfo.surface->win32.hwnd);
	if (ctx.hdc == NULL) {
		throw std::runtime_error("WGL: Failed to retrieve DC for window");
	}

	int pixelFormat = choosePixelFormat(wgl, ctx, createInfo.framebufferSettings);

	if (!DescribePixelFormat(ctx.hdc, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), &pfd)) {
		throw std::runtime_error("WGL: Failed to retrieve PFD for selected pixel format");
	}

	if (!SetPixelFormat(ctx.hdc, pixelFormat, &pfd)) {
		throw std::runtime_error("WGL: Failed to set selected pixel format");
	}

	if (wgl.ARB_create_context) {
		int index = 0, mask = 0;

		mask |= WGL_CONTEXT_CORE_PROFILE_BIT_ARB;

		// TODO: debugging context

		setWGLattrib(WGL_CONTEXT_MAJOR_VERSION_ARB, 4);
		setWGLattrib(WGL_CONTEXT_MINOR_VERSION_ARB, 3);

		if (mask) {
			setWGLattrib(WGL_CONTEXT_PROFILE_MASK_ARB, mask);
		}

		setWGLattrib(0, 0);

		ctx.hglrc = wgl.CreateContextAttribsARB(ctx.hdc, NULL, attribs);
		if (ctx.hglrc == NULL) {
			throw std::runtime_error("WGL: Failed to create OpenGL context");
		}
	} else {
		ctx.hglrc = wgl.CreateContext(ctx.hdc);
		if (ctx.hglrc == NULL) {
			throw std::runtime_error("WGL: Failed to create OpenGL context");
		}
	}

	/*window->context.makeContextCurrent = makeContextCurrent;
	window->context.swapBuffers = swapBuffers;
	window->context.swapInterval = swapInterval;
	window->context.extensionSupported = extensionSupported;
	window->context.getProcAddress = getProcAddress;
	window->context.destroyContext = destroyContext;*/
}

void createContextWGL(WGLContextInfo& ctx, std::shared_ptr<Swapchain>& swapchain, const RenderingAPICreateInfo& createInfo)
{
	WGLInfo& wgl = s_wglInfo;

	// Procedure here:
	// 1. Create a helper window on the monitor where the window passed by the client is displayed
	// 2. Create a dummy context on that window and load extension pointers
	// 3. Destroy that window along with its context
	// 4. Now create a "proper" OpenGL context on the window passed by the client

	HMONITOR monitorWindow = MonitorFromWindow(createInfo.surface->win32.hwnd, 0);
	MONITORINFO monitorInfo;
	monitorInfo.cbSize = sizeof(MONITORINFO);
	GetMonitorInfo(monitorWindow, &monitorInfo);

	HWND helperWindow = createHelperWindow(monitorInfo.rcWork.left, monitorInfo.rcWork.top);

	PlatformSurfaceReference helperSurfaceRef = {};
	helperSurfaceRef.win32.hwnd = helperWindow;
	helperSurfaceRef.win32.hinstance = GetModuleHandleW(NULL);

	RenderingAPICreateInfo helperCreateInfo = createInfo;
	helperCreateInfo.surface = &helperSurfaceRef;
	createContextWGLImpl(wgl, ctx, helperCreateInfo);

	analyzeContextWGL(ctx);

	destroyContextWGL(ctx);
	BOOL ok = DestroyWindow(helperWindow);
	assert(ok);

	createContextWGLImpl(wgl, ctx, createInfo);
	makeContextCurrent(wgl, ctx);

	swapchain = std::make_shared<WGLSwapchain>(ctx);
}

bool analyzeContextWGL(WGLContextInfo& ctx)
{
	WGLInfo& wgl = s_wglInfo;

	if (wgl.extensionsLoaded) {
		return false;
	}

	makeContextCurrent(wgl, ctx);
	loadExtensions(wgl, ctx);

	if (!wgl.ARB_create_context) {
		throw std::runtime_error("WGL: ARB_create_context is missing");
	}

	if (!wgl.ARB_create_context_profile) {
		throw std::runtime_error("WGL: ARB_create_context_profile is missing");
	}

	if (!wgl.ARB_context_flush_control) {
		throw std::runtime_error("WGL: ARB_context_flush_control is missing");
	}

	return true;
}

} // namespace rendercore
