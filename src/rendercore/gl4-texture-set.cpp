//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-texture-set.hpp>
#include <algorithm>

namespace rendercore {

GL4TextureSet::GL4TextureSet()
	: m_tsPairs()
{}

GL4TextureSet::GL4TextureSet(const TextureSetCreateInfo& createInfo)
	: m_tsPairs()
{
	m_tsPairs.resize(static_cast<size_t>(createInfo.numTsPairs));
	std::copy(createInfo.firstTsPair, createInfo.firstTsPair + createInfo.numTsPairs, m_tsPairs.begin());
}

GL4TextureSet::~GL4TextureSet()
{}

GL4TextureSet::GL4TextureSet(GL4TextureSet&& other)
	: m_tsPairs(std::move(other.m_tsPairs))
{}

GL4TextureSet& GL4TextureSet::operator=(GL4TextureSet&& other)
{
	if (&other != this) {
		m_tsPairs = std::move(other.m_tsPairs);
	}
	return *this;
}

const TextureSamplerPair& GL4TextureSet::getPair(int which) const
{
	return m_tsPairs.at(which);
}

TextureSamplerPair& GL4TextureSet::getPair(int which)
{
	return m_tsPairs.at(which);
}

} // namespace rendercore