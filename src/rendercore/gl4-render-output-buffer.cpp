//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-render-output-buffer.hpp>
#include <cassert>
#include <rendercore/gl4-data-format.hpp>

namespace rendercore {

GL4RenderOutputBuffer::GL4RenderOutputBuffer()
	: m_renderbuffer(0)
{}

GL4RenderOutputBuffer::GL4RenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo)
	: m_renderbuffer(0)
{
	glGenRenderbuffers(1, &m_renderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_renderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, translateNumberAndPixelFormat(createInfo.format.numFormat, createInfo.format.pxFormat), createInfo.size.width, createInfo.size.height);
}

GL4RenderOutputBuffer::~GL4RenderOutputBuffer()
{
	release();
}

GL4RenderOutputBuffer::GL4RenderOutputBuffer(GL4RenderOutputBuffer&& other)
	: m_renderbuffer(other.m_renderbuffer)
{
	other.m_renderbuffer = 0;
}

GL4RenderOutputBuffer& GL4RenderOutputBuffer::operator=(GL4RenderOutputBuffer&& other)
{
	if (&other != this) {
		m_renderbuffer = other.m_renderbuffer;
		other.m_renderbuffer = 0;
	}
	return *this;
}

void GL4RenderOutputBuffer::release()
{
	if (!empty()) {
		glDeleteRenderbuffers(1, &m_renderbuffer);
	}
	m_renderbuffer = 0;
}

} // namespace rendercore