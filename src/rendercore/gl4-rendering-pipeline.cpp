//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-rendering-pipeline.hpp>
#include <vector>
#include <cassert>
#include <GL/gl3w.h>
#include <rendercore/gl4-shader.hpp>
#include <rendercore/gl4-render-dispatcher.hpp>
#include <rendercore/gl4-data-format.hpp>
#include <rendercore/gl4-compare-function.hpp>

namespace rendercore {

GL4ProgramLinkException::GL4ProgramLinkException(const std::string& log)
	: std::runtime_error("Program link error: " + log), m_log(log)
{}

std::string GL4ProgramLinkException::getBuildLog() const
{
	return m_log;
}

GL4RenderingPipeline::GL4RenderingPipeline()
	: m_program(0), m_dispatcher(nullptr), m_pipelineInfo()
{}

static GLenum translateLogicOp(LogicalOperation op)
{
	switch (op) {
		case LogicalOperation::Copy:
			return GL_COPY;
		case LogicalOperation::Clear:
			return GL_CLEAR;
		case LogicalOperation::And:
			return GL_AND;
		case LogicalOperation::AndReverse:
			return GL_AND_REVERSE;
		case LogicalOperation::AndInverted:
			return GL_AND_INVERTED;
		case LogicalOperation::Noop:
			return GL_NOOP;
		case LogicalOperation::Xor:
			return GL_XOR;
		case LogicalOperation::Or:
			return GL_OR;
		case LogicalOperation::Nor:
			return GL_NOR;
		case LogicalOperation::Equiv:
			return GL_EQUIV;
		case LogicalOperation::Invert:
			return GL_INVERT;
		case LogicalOperation::OrReverse:
			return GL_OR_REVERSE;
		case LogicalOperation::CopyInverted:
			return GL_COPY_INVERTED;
		case LogicalOperation::OrInverted:
			return GL_OR_INVERTED;
		case LogicalOperation::Nand:
			return GL_NAND;
		case LogicalOperation::Set:
			return GL_SET;
		default:
			assert(0);
			break;
	}
	return GL_COPY;
}

static GLenum translatePrimitiveTopology(PrimitiveTopology topology)
{
	switch (topology) {
		case PrimitiveTopology::Points:
			return GL_POINTS;
		case PrimitiveTopology::Lines:
			return GL_LINES;
		case PrimitiveTopology::LineStrip:
			return GL_LINE_STRIP;
		case PrimitiveTopology::Triangles:
			return GL_TRIANGLES;
		case PrimitiveTopology::TriangleStrip:
			return GL_TRIANGLE_STRIP;
		case PrimitiveTopology::LinesAdj:
			return GL_LINES_ADJACENCY;
		case PrimitiveTopology::LineStripAdj:
			return GL_LINE_STRIP_ADJACENCY;
		case PrimitiveTopology::TrianglesAdj:
			return GL_TRIANGLES_ADJACENCY;
		case PrimitiveTopology::TriangleStripAdj:
			return GL_TRIANGLE_STRIP_ADJACENCY;
		case PrimitiveTopology::Patches:
			return GL_PATCHES;
		default:
			assert(0);
			break;
	}
	return GL_TRIANGLES;
}

static GLenum translateStencilOp(StencilOperation sclop)
{
	switch (sclop) {
		case StencilOperation::Keep:
			return GL_KEEP;
		case StencilOperation::Zero:
			return GL_ZERO;
		case StencilOperation::Replace:
			return GL_REPLACE;
		case StencilOperation::IncClamp:
			return GL_INCR;
		case StencilOperation::DecClamp:
			return GL_DECR;
		case StencilOperation::Invert:
			return GL_INVERT;
		case StencilOperation::IncWrap:
			return GL_INCR_WRAP;
		case StencilOperation::DecWrap:
			return GL_DECR_WRAP;
		default:
			assert(0);
			break;
	}
	return GL_KEEP; // will never be reached
}

static GLenum translateBlendFactor(BlendFactor bf)
{
	switch (bf) {
		case BlendFactor::Zero:
			return GL_ZERO;
		case BlendFactor::One:
			return GL_ONE;
		case BlendFactor::SrcColor:
			return GL_SRC_COLOR;
		case BlendFactor::OneMinusSrcColor:
			return GL_ONE_MINUS_SRC_COLOR;
		case BlendFactor::DestColor:
			return GL_DST_COLOR;
		case BlendFactor::OneMinusDestColor:
			return GL_ONE_MINUS_DST_COLOR;
		case BlendFactor::SrcAlpha:
			return GL_SRC_ALPHA;
		case BlendFactor::OneMinusSrcAlpha:
			return GL_ONE_MINUS_SRC_ALPHA;
		case BlendFactor::DestAlpha:
			return GL_DST_ALPHA;
		case BlendFactor::OneMinusDestAlpha:
			return GL_ONE_MINUS_DST_ALPHA;
		case BlendFactor::ConstantColor:
			return GL_CONSTANT_COLOR;
		case BlendFactor::OneMinusConstantColor:
			return GL_ONE_MINUS_CONSTANT_COLOR;
		case BlendFactor::ConstantAlpha:
			return GL_CONSTANT_ALPHA;
		case BlendFactor::OneMinusConstantAlpha:
			return GL_ONE_MINUS_CONSTANT_ALPHA;
		case BlendFactor::SrcAlphaSaturate:
			return GL_SRC_ALPHA_SATURATE;
		case BlendFactor::Src1Color:
			return GL_SRC1_COLOR;
		case BlendFactor::OneMinusSrc1Color:
			return GL_ONE_MINUS_SRC1_COLOR;
		case BlendFactor::Src1Alpha:
			return GL_SRC1_ALPHA;
		case BlendFactor::OneMinusSrc1Alpha:
			return GL_ONE_MINUS_SRC1_ALPHA;
		default:
			assert(0);
			break;
	}
	return GL_ZERO; // can't reach this
}

static GLenum translateBlendFunction(BlendFunction bfn)
{
	switch (bfn) {
		case BlendFunction::Add:
			return GL_FUNC_ADD;
		case BlendFunction::Subtract:
			return GL_FUNC_SUBTRACT;
		case BlendFunction::ReverseSubtract:
			return GL_FUNC_REVERSE_SUBTRACT;
		case BlendFunction::Min:
			return GL_MIN;
		case BlendFunction::Max:
			return GL_MAX;
		default:
			assert(0);
			break;
	}
	return GL_FUNC_ADD; // can't reach this
}

// TODO: descriptor sets
/*static void setupShaderConstantsBinding(GLuint program, const ShaderConstantsMappingInfo& mappingInfo, GLuint slot)
{
	if (mappingInfo.objectType == ResourceSlotType::Generic) {
		GLuint blockIndex = glGetUniformBlockIndex(program, mappingInfo.shaderEntityName);
		glUniformBlockBinding(program, blockIndex, slot);
	}
}*/

GL4RenderingPipeline::GL4RenderingPipeline(const RenderingPipelineCreateInfo& createInfo, GL4RenderDispatcher& dispatcher)
	: m_program(0), m_dispatcher(&dispatcher), m_pipelineInfo()
{
	m_program = glCreateProgram();

	GLuint attachedShaders[5] = { 0, 0, 0, 0, 0 };
	int nAttachedShaders = 0;

	if (createInfo.vertexShader.shader != nullptr) {
		attachedShaders[nAttachedShaders++] = static_cast<const GL4Shader*>(createInfo.vertexShader.shader)->m_obj;
	}
	if (createInfo.tessCtrlShader.shader != nullptr) {
		attachedShaders[nAttachedShaders++] = static_cast<const GL4Shader*>(createInfo.tessCtrlShader.shader)->m_obj;
	}
	if (createInfo.tessEvalShader.shader != nullptr) {
		attachedShaders[nAttachedShaders++] = static_cast<const GL4Shader*>(createInfo.tessEvalShader.shader)->m_obj;
	}
	if (createInfo.geometryShader.shader != nullptr) {
		attachedShaders[nAttachedShaders++] = static_cast<const GL4Shader*>(createInfo.geometryShader.shader)->m_obj;
	}
	if (createInfo.fragmentShader.shader != nullptr) {
		attachedShaders[nAttachedShaders++] = static_cast<const GL4Shader*>(createInfo.fragmentShader.shader)->m_obj;
	}

	for (int i = 0; i < nAttachedShaders; ++i) {
		glAttachShader(m_program, attachedShaders[i]);
	}

	// TODO: descriptor sets
	/*
	int nextAttribIndex = 0;
	int nextFragDataIndex = 0;
	for (int i = 0; i < kMaxResourceSets; ++i) {
		const ResourceSetMappingInfo& vmapping = createInfo.vertexShader.resourceSetMapping[i];
		for (uint32_t j = 0; j < vmapping.slotCount; ++j) {
			if (vmapping.firstResourceSlotInfo[j].objectType == ResourceSlotType::Generic) {
				glBindAttribLocation(m_program, nextAttribIndex++, vmapping.firstResourceSlotInfo[j].shaderEntityName);
			}
		}
		const ResourceSetMappingInfo& fmapping = createInfo.fragmentShader.resourceSetMapping[i];
		for (uint32_t j = 0; j < fmapping.slotCount; ++j) {
			if (fmapping.firstResourceSlotInfo[j].objectType == ResourceSlotType::Generic) {
				glBindFragDataLocation(m_program, nextFragDataIndex++, fmapping.firstResourceSlotInfo[j].shaderEntityName);
			}
		}
	}
	*/

	glLinkProgram(m_program);

	GLint status;
	glGetProgramiv(m_program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLength;
		glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &infoLogLength);

		std::vector<char> infoLog(infoLogLength);
		glGetProgramInfoLog(m_program, infoLogLength, nullptr, infoLog.data());

		std::string infoLogStr(infoLog.begin(), infoLog.end());
		release();
		throw GL4ProgramLinkException(infoLogStr);
	}

	for (int i = nAttachedShaders - 1; i >= 0; --i) {
		glDetachShader(m_program, attachedShaders[i]);
	}

	// TODO: descriptor sets
	/*
	// sampler uniforms
	m_dispatcher->invalidatePipeline(); // need to bind
	int nextTexUnit = 0;
	for (int i = 0; i < kMaxResourceSets; ++i) {
		// TODO: other shader types?
		const ResourceSetMappingInfo& fmapping = createInfo.fragmentShader.resourceSetMapping[i];
		for (uint32_t j = 0; j < fmapping.slotCount; ++j) {
			if (fmapping.firstResourceSlotInfo[j].objectType == ResourceSlotType::Sampler) {
				glUseProgram(m_program);
				GLint loc = glGetUniformLocation(m_program, fmapping.firstResourceSlotInfo[j].shaderEntityName);
				if (loc != -1) {
					glUniform1i(loc, nextTexUnit);
				}
				++nextTexUnit;
			}
		}
	}

	// UBO mappings
	setupShaderConstantsBinding(m_program, createInfo.vertexShader.shaderConstantsMapping, 0);
	setupShaderConstantsBinding(m_program, createInfo.tessCtrlShader.shaderConstantsMapping, 1);
	setupShaderConstantsBinding(m_program, createInfo.tessEvalShader.shaderConstantsMapping, 2);
	setupShaderConstantsBinding(m_program, createInfo.geometryShader.shaderConstantsMapping, 3);
	setupShaderConstantsBinding(m_program, createInfo.fragmentShader.shaderConstantsMapping, 4);
	*/

	// pipeline state

	m_pipelineInfo.logicOp = translateLogicOp(createInfo.cbState.logicOp);

	for (int i = 0; i < kPipelineMaxColorTargets; ++i) {
		m_pipelineInfo.enableTarget[i] = createInfo.cbState.target[i].enableBlending;
		m_pipelineInfo.colorMask[i] = createInfo.cbState.target[i].channelWriteMask;
	}

	m_pipelineInfo.mode = translatePrimitiveTopology(createInfo.iaState.topology);

	// pipeline vertex input layout
	glGenVertexArrays(1, &m_pipelineInfo.vao);
	glBindVertexArray(m_pipelineInfo.vao);

	for (uint32_t i = 0; i < createInfo.viState.vertexAttributeDescriptionCount; ++i) {
		const VertexInputAttributeDescription& desc = createInfo.viState.vertexAttributeDescriptions[i];
		glVertexAttribFormat(static_cast<GLuint>(desc.location), static_cast<GLint>(desc.numComponents), translateNumFormat(desc.format), isNumFormatNormalized(desc.format) ? GL_TRUE : GL_FALSE, static_cast<GLuint>(desc.offset));
		glVertexAttribBinding(static_cast<GLuint>(desc.location), static_cast<GLuint>(desc.binding));
		glEnableVertexAttribArray(desc.location);
	}
	for (uint32_t i = 0; i < createInfo.viState.vertexBindingDescriptionCount; ++i) {
		const VertexInputBindingDescription& desc = createInfo.viState.vertexBindingDescriptions[i];
		m_pipelineInfo.bindPointStrides[desc.binding] = desc.stride;
	}

	glBindVertexArray(0);

	// tessellation state
	m_pipelineInfo.patchControlPoints = static_cast<GLint>(createInfo.tessState.patchControlPoints);

	// viewport state
	m_pipelineInfo.viewportState = createInfo.vpState;

	// rasterizer state
	switch (createInfo.rsState.fillMode) {
		case FillMode::Solid:
			m_pipelineInfo.fillMode = GL_FILL;
			break;
		case FillMode::Wireframe:
			m_pipelineInfo.fillMode = GL_LINE;
			break;
		default:
			assert(0);
			break;
	}

	switch (createInfo.rsState.cullMode) {
		case CullMode::None:
			m_pipelineInfo.enableCulling = false;
			break;
		case CullMode::Front:
			m_pipelineInfo.enableCulling = true;
			m_pipelineInfo.cullMode = GL_FRONT;
			break;
		case CullMode::Back:
			m_pipelineInfo.enableCulling = true;
			m_pipelineInfo.cullMode = GL_BACK;
			break;
		case CullMode::FrontAndBack:
			m_pipelineInfo.enableCulling = true;
			m_pipelineInfo.cullMode = GL_FRONT_AND_BACK;
		default:
			assert(0);
			break;
	}

	switch (createInfo.rsState.frontFace) {
		case FaceOrientation::CounterClockwise:
			m_pipelineInfo.frontFace = GL_CCW;
			break;
		case FaceOrientation::Clockwise:
			m_pipelineInfo.frontFace = GL_CW;
			break;
		default:
			assert(0);
			break;
	}

	// TODO: depth biases (glPolygonOffset?)

	// multisample state
	// TODO: glSampleCoverage/glSampleMask?
	m_pipelineInfo.enableMultisample = createInfo.msState.samples > 1;

	// depth-stencil state
	m_pipelineInfo.enableDepthTest = createInfo.dbState.enableDepthTest;
	m_pipelineInfo.enableDepthWriting = createInfo.dbState.enableDepthWrite ? GL_TRUE : GL_FALSE;
	m_pipelineInfo.depthFunc = gl4TranslateCompareFunc(createInfo.dbState.depthCompareFunc);
	m_pipelineInfo.enableStencilTest = createInfo.dbState.enableStencilTest;
	m_pipelineInfo.frontSFunc = gl4TranslateCompareFunc(createInfo.dbState.frontOp.stencilFunc);
	m_pipelineInfo.frontSRef = createInfo.dbState.frontOp.stencilRef;
	m_pipelineInfo.backSFunc = gl4TranslateCompareFunc(createInfo.dbState.backOp.stencilFunc);
	m_pipelineInfo.backSRef = createInfo.dbState.backOp.stencilRef;
	m_pipelineInfo.stencilReadMask = createInfo.dbState.stencilReadMask;
	m_pipelineInfo.stencilWriteMask = createInfo.dbState.stencilWriteMask;
	m_pipelineInfo.frontSFail = translateStencilOp(createInfo.dbState.frontOp.stencilFailOp);
	m_pipelineInfo.frontDPFail = translateStencilOp(createInfo.dbState.frontOp.stencilDepthFailOp);
	m_pipelineInfo.frontDPPass = translateStencilOp(createInfo.dbState.frontOp.stencilPassOp);
	m_pipelineInfo.backSFail = translateStencilOp(createInfo.dbState.backOp.stencilFailOp);
	m_pipelineInfo.backDPFail = translateStencilOp(createInfo.dbState.backOp.stencilDepthFailOp);
	m_pipelineInfo.backDPPass = translateStencilOp(createInfo.dbState.backOp.stencilPassOp);

	// color blend state
	for (int i = 0; i < 4; ++i) {
		m_pipelineInfo.blendColor[i] = createInfo.cbState.blendConst[i];
	}
	for (int i = 0; i < kPipelineMaxColorTargets; ++i) {
		GLBlendInfo& blendInfo = m_pipelineInfo.blendInfo[i];
		const PipelineColorBlenderTargetStateInfo& stateInfo = createInfo.cbState.target[i];
		blendInfo.enable = stateInfo.enableBlending;
		blendInfo.srcBlendColor = translateBlendFactor(stateInfo.srcBlendColor);
		blendInfo.destBlendColor = translateBlendFactor(stateInfo.destBlendColor);
		blendInfo.blendEqnColor = translateBlendFunction(stateInfo.blendFuncColor);
		blendInfo.srcBlendAlpha = translateBlendFactor(stateInfo.srcBlendAlpha);
		blendInfo.destBlendAlpha = translateBlendFactor(stateInfo.destBlendAlpha);
		blendInfo.blendEqnAlpha = translateBlendFunction(stateInfo.blendFuncAlpha);
	}
}

GL4RenderingPipeline::~GL4RenderingPipeline()
{
	release();
}

GL4RenderingPipeline::GL4RenderingPipeline(GL4RenderingPipeline&& other)
	: m_program(other.m_program), m_dispatcher(other.m_dispatcher), m_pipelineInfo(other.m_pipelineInfo)
{
	other.m_program = 0;
}

GL4RenderingPipeline& GL4RenderingPipeline::operator=(GL4RenderingPipeline&& other)
{
	if (&other != this) {
		m_program = other.m_program;
		m_dispatcher = other.m_dispatcher;
		m_pipelineInfo = other.m_pipelineInfo;
		other.m_program = 0;
	}
	return *this;
}

void GL4RenderingPipeline::release()
{
	if (!empty()) {
		glDeleteProgram(m_program);
	}
	m_program = 0;
}

static inline void bindForColorTarget(const GL4RenderingPipeline::GLBlendInfo& blendInfo, int colorTarget)
{
	if (blendInfo.enable) {
		glEnablei(GL_BLEND, static_cast<GLuint>(colorTarget));
		glBlendFuncSeparatei(static_cast<GLuint>(colorTarget), blendInfo.srcBlendColor, blendInfo.destBlendColor, blendInfo.srcBlendAlpha, blendInfo.destBlendAlpha);
		glBlendEquationSeparatei(static_cast<GLuint>(colorTarget), blendInfo.blendEqnColor, blendInfo.blendEqnAlpha);
	} else {
		glDisablei(GL_BLEND, static_cast<GLuint>(colorTarget));
	}
}

void GL4RenderingPipeline::bind() const
{
	glLogicOp(m_pipelineInfo.logicOp);
	for (int i = 0; i < kPipelineMaxColorTargets; ++i) {
		if (m_pipelineInfo.enableTarget[i]) {
			glColorMask(
			    (m_pipelineInfo.colorMask[i] & 1) > 0 ? GL_TRUE : GL_FALSE,
			    (m_pipelineInfo.colorMask[i] & 2) > 0 ? GL_TRUE : GL_FALSE,
			    (m_pipelineInfo.colorMask[i] & 4) > 0 ? GL_TRUE : GL_FALSE,
			    (m_pipelineInfo.colorMask[i] & 8) > 0 ? GL_TRUE : GL_FALSE);
		}
	}
	glUseProgram(m_program);
	glBindVertexArray(m_pipelineInfo.vao);

	// tessellation state
	glPatchParameteri(GL_PATCH_VERTICES, m_pipelineInfo.patchControlPoints);

	// viewport state
	for (int i = 0; i < static_cast<int>(m_pipelineInfo.viewportState.viewportCount); ++i) {
		const ViewportInfo& vpInfo = m_pipelineInfo.viewportState.viewports[i];
		glViewportIndexedf(i, vpInfo.originX, vpInfo.originY, vpInfo.width, vpInfo.height);
		glDepthRangeIndexed(i, static_cast<GLdouble>(vpInfo.minDepth), static_cast<GLdouble>(vpInfo.maxDepth));
	}
	if (m_pipelineInfo.viewportState.enableScissor) {
		glEnable(GL_SCISSOR_TEST);
		for (int i = 0; i < static_cast<int>(m_pipelineInfo.viewportState.viewportCount); ++i) {
			const Rectangle scissorRect = m_pipelineInfo.viewportState.scissorRegions[i];
			glScissorIndexed(i, scissorRect.offsetX, scissorRect.offsetY, scissorRect.extentX, scissorRect.extentY);
		}
	} else {
		glDisable(GL_SCISSOR_TEST);
	}

	// rasterizer state
	glPolygonMode(GL_FRONT_AND_BACK, m_pipelineInfo.fillMode);
	if (m_pipelineInfo.enableCulling) {
		glEnable(GL_CULL_FACE);
	} else {
		glDisable(GL_CULL_FACE);
	}
	glCullFace(m_pipelineInfo.cullMode);
	glFrontFace(m_pipelineInfo.frontFace);

	// multisample state
	if (m_pipelineInfo.enableMultisample) {
		glEnable(GL_MULTISAMPLE);
	} else {
		glDisable(GL_MULTISAMPLE);
	}

	// depth-stencil state
	if (m_pipelineInfo.enableDepthTest) {
		glEnable(GL_DEPTH_TEST);
	} else {
		glDisable(GL_DEPTH_TEST);
	}
	glDepthMask(m_pipelineInfo.enableDepthWriting);
	glDepthFunc(m_pipelineInfo.depthFunc);
	// FIXME: no depth bounds test available in core :(
	// but there is glDepthBoundsEXT
	// RESOLUTION: ignore it for now, since its use case is quite limited
	if (m_pipelineInfo.enableStencilTest) {
		glEnable(GL_STENCIL_TEST);
	} else {
		glDisable(GL_STENCIL_TEST);
	}
	glStencilFuncSeparate(GL_FRONT, m_pipelineInfo.frontSFunc, m_pipelineInfo.frontSRef, m_pipelineInfo.stencilReadMask);
	glStencilFuncSeparate(GL_BACK, m_pipelineInfo.backSFunc, m_pipelineInfo.backSRef, m_pipelineInfo.stencilReadMask);
	glStencilMask(m_pipelineInfo.stencilWriteMask);
	glStencilOpSeparate(GL_FRONT, m_pipelineInfo.frontSFail, m_pipelineInfo.frontDPFail, m_pipelineInfo.frontDPPass);
	glStencilOpSeparate(GL_BACK, m_pipelineInfo.backSFail, m_pipelineInfo.backDPFail, m_pipelineInfo.backDPPass);

	// color blend state
	glBlendColor(
	    m_pipelineInfo.blendColor[0], m_pipelineInfo.blendColor[1],
	    m_pipelineInfo.blendColor[2], m_pipelineInfo.blendColor[3]);
	for (int i = 0; i < kPipelineMaxColorTargets; ++i) {
		bindForColorTarget(m_pipelineInfo.blendInfo[i], i);
	}
}

void GL4RenderingPipeline::setDrawBuffers(bool targetIsDefault) const
{
	if (!targetIsDefault) {
		int nActiveTargets = 0;
		GLenum activeTargets[kPipelineMaxColorTargets];
		for (int i = 0; i < kPipelineMaxColorTargets; ++i) {
			if (m_pipelineInfo.blendInfo[i].enable) {
				activeTargets[nActiveTargets++] = GL_COLOR_ATTACHMENT0 + i;
			}
		}
		if (nActiveTargets > 0) {
			glDrawBuffers(nActiveTargets, activeTargets);
		} else {
			glDrawBuffer(GL_NONE);
		}
	}
}

} // namespace rendercore
