//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-data-format.hpp>
#include <cassert>

namespace rendercore {

// enum values for the compressed S3TC pixel format
// https://www.opengl.org/registry/specs/EXT/texture_compression_s3tc.txt
static const GLenum GL_COMPRESSED_RGB_S3TC_DXT1_EXT = 0x83F0;
static const GLenum GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = 0x83F1;
static const GLenum GL_COMPRESSED_RGBA_S3TC_DXT3_EXT = 0x83F2;
static const GLenum GL_COMPRESSED_RGBA_S3TC_DXT5_EXT = 0x83F3;

// https://www.opengl.org/registry/specs/EXT/texture_sRGB.txt
static const GLenum GL_COMPRESSED_SRGB_S3TC_DXT1_EXT = 0x8C4C;
static const GLenum GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT = 0x8C4D;
static const GLenum GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT = 0x8C4E;
static const GLenum GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT = 0x8C4F;

// https://www.opengl.org/registry/specs/EXT/texture_compression_latc.txt
static const GLenum GL_COMPRESSED_LUMINANCE_LATC1_EXT = 0x8C70;
static const GLenum GL_COMPRESSED_SIGNED_LUMINANCE_LATC1_EXT = 0x8C71;
static const GLenum GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT = 0x8C72;
static const GLenum GL_COMPRESSED_SIGNED_LUMINANCE_ALPHA_LATC2_EXT = 0x8C73;

void translateNumberAndPixelFormat(NumberFormat nfmt, PixelFormat pxfmt, GLenum& internalFormat, GLenum& format, GLenum& type, bool& isCompressed)
{
	bool isSigned = false;
	bool isNormalized = false;
	bool isFloat = false;
	bool isGammaCorrected = false;
	int channelSize = 8;
	isCompressed = false;
	switch (nfmt) {
		case NumberFormat::Undefined:
			type = GL_UNSIGNED_BYTE; // default
			break;
		case NumberFormat::UnsignedNorm8:
			type = GL_UNSIGNED_BYTE;
			isNormalized = true;
			break;
		case NumberFormat::UnsignedNorm16:
			type = GL_UNSIGNED_SHORT;
			isNormalized = true;
			channelSize = 16;
			break;
		case NumberFormat::UnsignedNorm32:
			type = GL_UNSIGNED_INT;
			isNormalized = true;
			channelSize = 32;
			break;
		case NumberFormat::SignedNorm8:
			type = GL_BYTE;
			isSigned = true;
			isNormalized = true;
			break;
		case NumberFormat::SignedNorm16:
			type = GL_SHORT;
			isSigned = true;
			isNormalized = true;
			channelSize = 16;
			break;
		case NumberFormat::SignedNorm32:
			type = GL_INT;
			isSigned = true;
			isNormalized = true;
			channelSize = 32;
			break;
		case NumberFormat::UnsignedInt8:
			type = GL_UNSIGNED_BYTE;
			break;
		case NumberFormat::UnsignedInt16:
			type = GL_UNSIGNED_SHORT;
			channelSize = 16;
			break;
		case NumberFormat::UnsignedInt32:
			type = GL_UNSIGNED_INT;
			channelSize = 32;
			break;
		case NumberFormat::SignedInt8:
			type = GL_BYTE;
			isSigned = true;
			break;
		case NumberFormat::SignedInt16:
			type = GL_SHORT;
			isSigned = true;
			channelSize = 16;
			break;
		case NumberFormat::SignedInt32:
			type = GL_INT;
			isSigned = true;
			channelSize = 32;
			break;
		case NumberFormat::Float:
			type = GL_FLOAT;
			isSigned = true;
			isNormalized = false;
			isFloat = true;
			channelSize = 32;
			break;
		case NumberFormat::sRGB:
			type = GL_UNSIGNED_BYTE;
			isNormalized = true;
			isGammaCorrected = true;
			break;
		case NumberFormat::DepthStencil:
			type = GL_UNSIGNED_BYTE; // default
			break;
		default:
			assert(0);
			break;
	}

	// why does GL need 61 internal image formats?
	// hardcore decision making, holy crap
	switch (pxfmt) {
		case PixelFormat::Red:
			format = (isNormalized || isFloat) ? GL_RED : GL_RED_INTEGER;
			if (isSigned) {
				if (isNormalized) {
					switch (channelSize) {
						case 8:
							internalFormat = GL_R8_SNORM;
							break;
						case 16:
							internalFormat = GL_R16_SNORM;
							break;
						default:
							assert(0);
							break;
					}
				} else if (isFloat) {
					switch (channelSize) {
						case 16:
							internalFormat = GL_R16F;
							break;
						case 32:
							internalFormat = GL_R32F;
							break;
						default:
							assert(0);
							break;
					}
				} else {
					switch (channelSize) {
						case 8:
							internalFormat = GL_R8I;
							break;
						case 16:
							internalFormat = GL_R16I;
							break;
						case 32:
							internalFormat = GL_R32I;
							break;
					}
				}
			} else {
				if (isNormalized) {
					switch (channelSize) {
						case 8:
							internalFormat = GL_R8;
							break;
						case 16:
							internalFormat = GL_R16;
							break;
						default:
							assert(0);
							break;
					}
				} else if (isFloat) {
					assert(0); // floats cannot be unsigned
				} else {
					switch (channelSize) {
						case 8:
							internalFormat = GL_R8UI;
							break;
						case 16:
							internalFormat = GL_R16UI;
							break;
						case 32:
							internalFormat = GL_R32UI;
							break;
					}
				}
			}
			break;
		case PixelFormat::RG:
			format = (isNormalized || isFloat) ? GL_RG : GL_RG_INTEGER;
			if (isSigned) {
				if (isNormalized) {
					switch (channelSize) {
						case 8:
							internalFormat = GL_RG8_SNORM;
							break;
						case 16:
							internalFormat = GL_RG16_SNORM;
							break;
						default:
							assert(0);
							break;
					}
				} else if (isFloat) {
					switch (channelSize) {
						case 16:
							internalFormat = GL_RG16F;
							break;
						case 32:
							internalFormat = GL_RG32F;
							break;
						default:
							assert(0);
							break;
					}
				} else {
					switch (channelSize) {
						case 8:
							internalFormat = GL_RG8I;
							break;
						case 16:
							internalFormat = GL_RG16I;
							break;
						case 32:
							internalFormat = GL_RG32I;
							break;
					}
				}
			} else {
				if (isNormalized) {
					switch (channelSize) {
						case 8:
							internalFormat = GL_RG8;
							break;
						case 16:
							internalFormat = GL_RG16;
							break;
						default:
							assert(0);
							break;
					}
				} else if (isFloat) {
					assert(0); // floats cannot be unsigned
				} else {
					switch (channelSize) {
						case 8:
							internalFormat = GL_RG8UI;
							break;
						case 16:
							internalFormat = GL_RG16UI;
							break;
						case 32:
							internalFormat = GL_RG32UI;
							break;
					}
				}
			}
			break;
		case PixelFormat::RGB:
			format = (isNormalized || isFloat) ? GL_RGB : GL_RGB_INTEGER;
			if (isGammaCorrected) {
				internalFormat = GL_SRGB8;
			} else {
				if (isSigned) {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8_SNORM;
								break;
							case 16:
								internalFormat = GL_RGB16_SNORM;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						switch (channelSize) {
							case 16:
								internalFormat = GL_RGB16F;
								break;
							case 32:
								internalFormat = GL_RGB32F;
								break;
							default:
								assert(0);
								break;
						}
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8I;
								break;
							case 16:
								internalFormat = GL_RGB16I;
								break;
							case 32:
								internalFormat = GL_RGB32I;
								break;
						}
					}
				} else {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8;
								break;
							case 16:
								internalFormat = GL_RGB16;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						assert(0); // floats cannot be unsigned
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8UI;
								break;
							case 16:
								internalFormat = GL_RGB16UI;
								break;
							case 32:
								internalFormat = GL_RGB32UI;
								break;
						}
					}
				}
			}
			break;
		case PixelFormat::BGR:
			format = (isNormalized || isFloat) ? GL_BGR : GL_BGR_INTEGER;
			if (isGammaCorrected) {
				internalFormat = GL_SRGB8;
			} else {
				if (isSigned) {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8_SNORM;
								break;
							case 16:
								internalFormat = GL_RGB16_SNORM;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						switch (channelSize) {
							case 16:
								internalFormat = GL_RGB16F;
								break;
							case 32:
								internalFormat = GL_RGB32F;
								break;
							default:
								assert(0);
								break;
						}
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8I;
								break;
							case 16:
								internalFormat = GL_RGB16I;
								break;
							case 32:
								internalFormat = GL_RGB32I;
								break;
						}
					}
				} else {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8;
								break;
							case 16:
								internalFormat = GL_RGB16;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						assert(0); // floats cannot be unsigned
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGB8UI;
								break;
							case 16:
								internalFormat = GL_RGB16UI;
								break;
							case 32:
								internalFormat = GL_RGB32UI;
								break;
						}
					}
				}
			}
			break;
		case PixelFormat::RGBA:
			format = (isNormalized || isFloat) ? GL_RGBA : GL_RGBA_INTEGER;
			if (isGammaCorrected) {
				internalFormat = GL_SRGB8_ALPHA8;
			} else {
				if (isSigned) {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8_SNORM;
								break;
							case 16:
								internalFormat = GL_RGBA16_SNORM;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						switch (channelSize) {
							case 16:
								internalFormat = GL_RGBA16F;
								break;
							case 32:
								internalFormat = GL_RGBA32F;
								break;
							default:
								assert(0);
								break;
						}
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8I;
								break;
							case 16:
								internalFormat = GL_RGBA16I;
								break;
							case 32:
								internalFormat = GL_RGBA32I;
								break;
						}
					}
				} else {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8;
								break;
							case 16:
								internalFormat = GL_RGBA16;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						assert(0); // floats cannot be unsigned
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8UI;
								break;
							case 16:
								internalFormat = GL_RGBA16UI;
								break;
							case 32:
								internalFormat = GL_RGBA32UI;
								break;
						}
					}
				}
			}
			break;
		case PixelFormat::BGRA:
			format = (isNormalized || isFloat) ? GL_BGRA : GL_BGRA_INTEGER;
			if (isGammaCorrected) {
				internalFormat = GL_SRGB8_ALPHA8;
			} else {
				if (isSigned) {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8_SNORM;
								break;
							case 16:
								internalFormat = GL_RGBA16_SNORM;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						switch (channelSize) {
							case 16:
								internalFormat = GL_RGBA16F;
								break;
							case 32:
								internalFormat = GL_RGBA32F;
								break;
							default:
								assert(0);
								break;
						}
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8I;
								break;
							case 16:
								internalFormat = GL_RGBA16I;
								break;
							case 32:
								internalFormat = GL_RGBA32I;
								break;
						}
					}
				} else {
					if (isNormalized) {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8;
								break;
							case 16:
								internalFormat = GL_RGBA16;
								break;
							default:
								assert(0);
								break;
						}
					} else if (isFloat) {
						assert(0); // floats cannot be unsigned
					} else {
						switch (channelSize) {
							case 8:
								internalFormat = GL_RGBA8UI;
								break;
							case 16:
								internalFormat = GL_RGBA16UI;
								break;
							case 32:
								internalFormat = GL_RGBA32UI;
								break;
						}
					}
				}
			}
			break;
		case PixelFormat::StencilIndex:
			format = GL_STENCIL_INDEX;
			internalFormat = GL_STENCIL_INDEX; // GL 4.4
			break;
		case PixelFormat::DepthComponent:
			format = GL_DEPTH_COMPONENT;
			internalFormat = GL_DEPTH_COMPONENT24;
			break;
		case PixelFormat::DepthStencil:
			format = GL_DEPTH_STENCIL;
			internalFormat = GL_DEPTH24_STENCIL8;
			break;
		case PixelFormat::RGB_DXT1:
			format = GL_RGB;
			internalFormat = isGammaCorrected ? GL_COMPRESSED_SRGB_S3TC_DXT1_EXT : GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
			isCompressed = true;
			break;
		case PixelFormat::RGBA_DXT1:
			format = GL_RGBA;
			internalFormat = isGammaCorrected ? GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT : GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
			isCompressed = true;
			break;
		case PixelFormat::RGBA_DXT3:
			format = GL_RGBA;
			internalFormat = isGammaCorrected ? GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT : GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			isCompressed = true;
			break;
		case PixelFormat::RGBA_DXT5:
			format = GL_RGBA;
			internalFormat = isGammaCorrected ? GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT : GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			isCompressed = true;
			break;
		case PixelFormat::Red_RGTC1:
			format = GL_RED;
			internalFormat = GL_COMPRESSED_RED_RGTC1; // RGTC and LATC are never explicitly gamma-corrected
			isCompressed = true;
			break;
		case PixelFormat::Red_RGTC1_Signed:
			format = GL_RED;
			internalFormat = GL_COMPRESSED_SIGNED_RED_RGTC1;
			isCompressed = true;
			break;
		case PixelFormat::RG_RGTC2:
			format = GL_RG;
			internalFormat = GL_COMPRESSED_RG_RGTC2;
			isCompressed = true;
			break;
		case PixelFormat::RG_RGTC2_Signed:
			format = GL_RG;
			internalFormat = GL_COMPRESSED_SIGNED_RG_RGTC2;
			isCompressed = true;
			break;
		// all graphics hardware that supports RGTC (which is core since GL 3.0), should
		// also support LATC, because it's essentially the same thing except that the color
		// data is interpreted in a different way (search for OpenGL Hardware Database)
		case PixelFormat::Luminance_LATC1:
			format = GL_RGB; // correct?
			internalFormat = GL_COMPRESSED_LUMINANCE_LATC1_EXT;
			isCompressed = true;
			break;
		case PixelFormat::Luminance_LATC1_Signed:
			format = GL_RGB;
			internalFormat = GL_COMPRESSED_SIGNED_LUMINANCE_LATC1_EXT;
			isCompressed = true;
			break;
		case PixelFormat::LA_LATC2:
			format = GL_RGBA;
			internalFormat = GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT;
			isCompressed = true;
			break;
		case PixelFormat::LA_LATC2_Signed:
			format = GL_RGBA;
			internalFormat = GL_COMPRESSED_SIGNED_LUMINANCE_ALPHA_LATC2_EXT;
			isCompressed = true;
			break;
	}
}

GLenum translateNumberAndPixelFormat(NumberFormat nfmt, PixelFormat pxfmt)
{
	GLenum unused;
	GLenum internalFormat;
	bool bUnused;
	translateNumberAndPixelFormat(nfmt, pxfmt, internalFormat, unused, unused, bUnused);
	return internalFormat;
}

GLenum translateNumFormat(NumberFormat fmt)
{
	switch (fmt) {
		case NumberFormat::Undefined:
			assert(0 && "Number format must not be undefined for vertex memory views");
			break;
		case NumberFormat::UnsignedNorm8:
			return GL_UNSIGNED_BYTE;
		case NumberFormat::UnsignedNorm16:
			return GL_UNSIGNED_SHORT;
		case NumberFormat::UnsignedNorm32:
			return GL_UNSIGNED_INT;
		case NumberFormat::SignedNorm8:
			return GL_BYTE;
		case NumberFormat::SignedNorm16:
			return GL_SHORT;
		case NumberFormat::SignedNorm32:
			return GL_INT;
		case NumberFormat::UnsignedInt8:
			return GL_UNSIGNED_BYTE;
		case NumberFormat::UnsignedInt16:
			return GL_UNSIGNED_SHORT;
		case NumberFormat::UnsignedInt32:
			return GL_UNSIGNED_INT;
		case NumberFormat::SignedInt8:
			return GL_BYTE;
		case NumberFormat::SignedInt16:
			return GL_SHORT;
		case NumberFormat::SignedInt32:
			return GL_INT;
		case NumberFormat::Float:
			return GL_FLOAT;
		case NumberFormat::sRGB:
			assert(0 && "Number format must not be sRGB for vertex memory views");
			break;
		case NumberFormat::DepthStencil:
			assert(0 && "Number format must not be DepthStencil for vertex memory views");
			break;
		default:
			assert(0);
			break;
	}
	return GL_FLOAT;
}

bool isNumFormatNormalized(NumberFormat fmt)
{
	switch (fmt) {
		case NumberFormat::Undefined:
			assert(0 && "Number format must not be undefined for vertex memory views");
			break;
		case NumberFormat::UnsignedNorm8:
			return true;
		case NumberFormat::UnsignedNorm16:
			return true;
		case NumberFormat::UnsignedNorm32:
			return true;
		case NumberFormat::SignedNorm8:
			return true;
		case NumberFormat::SignedNorm16:
			return true;
		case NumberFormat::SignedNorm32:
			return true;
		case NumberFormat::UnsignedInt8:
			return false;
		case NumberFormat::UnsignedInt16:
			return false;
		case NumberFormat::UnsignedInt32:
			return false;
		case NumberFormat::SignedInt8:
			return false;
		case NumberFormat::SignedInt16:
			return false;
		case NumberFormat::SignedInt32:
			return false;
		case NumberFormat::Float:
			return false;
		case NumberFormat::sRGB:
			assert(0 && "Number format must not be sRGB for vertex memory views");
			break;
		case NumberFormat::DepthStencil:
			assert(0 && "Number format must not be DepthStencil for vertex memory views");
			break;
		default:
			assert(0);
			break;
	}
	return false;
}

} // namespace rendercore