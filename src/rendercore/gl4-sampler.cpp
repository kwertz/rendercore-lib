//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-sampler.hpp>
#include <cassert>
#include <rendercore/gl4-compare-function.hpp>

namespace rendercore {

GL4Sampler::GL4Sampler()
	: m_sampler(0)
{}

static void translateTextureFilter(TextureFilter filter, GLenum& magFilter, GLenum& minFilter)
{
	switch (filter) {
		case TextureFilter::MagNearestMinNearestMipNearest:
			magFilter = GL_NEAREST;
			minFilter = GL_NEAREST_MIPMAP_NEAREST;
			break;
		case TextureFilter::MagLinearMinNearestMipNearest:
			magFilter = GL_LINEAR;
			minFilter = GL_NEAREST_MIPMAP_NEAREST;
			break;
		case TextureFilter::MagNearestMinLinearMipNearest:
			magFilter = GL_NEAREST;
			minFilter = GL_LINEAR_MIPMAP_NEAREST;
			break;
		case TextureFilter::MagLinearMinLinearMipNearest:
			magFilter = GL_LINEAR;
			minFilter = GL_LINEAR_MIPMAP_NEAREST;
			break;
		case TextureFilter::MagNearestMinNearestMipLinear:
			magFilter = GL_NEAREST;
			minFilter = GL_NEAREST_MIPMAP_LINEAR;
			break;
		case TextureFilter::MagLinearMinNearestMipLinear:
			magFilter = GL_LINEAR;
			minFilter = GL_NEAREST_MIPMAP_LINEAR;
			break;
		case TextureFilter::MagNearestMinLinearMipLinear:
			magFilter = GL_NEAREST;
			minFilter = GL_LINEAR_MIPMAP_LINEAR;
			break;
		case TextureFilter::Anisotropic: // not an explicit enum
		case TextureFilter::MagLinearMinLinearMipLinear:
			magFilter = GL_LINEAR;
			minFilter = GL_LINEAR_MIPMAP_LINEAR;
			break;
		default:
			assert(0);
			break;
	}
}

static GLenum translateTextureAddressingMode(TextureAddressingMode mode)
{
	switch (mode) {
		case TextureAddressingMode::Wrap:
			return GL_REPEAT;
		case TextureAddressingMode::Mirror:
			return GL_MIRRORED_REPEAT;
		case TextureAddressingMode::Clamp:
			return GL_CLAMP_TO_EDGE;
		case TextureAddressingMode::MirrorOnce:
			return GL_MIRROR_CLAMP_TO_EDGE;
		case TextureAddressingMode::ClampToBorder:
			return GL_CLAMP_TO_BORDER;
		default:
			assert(0);
			break;
	}
	return GL_REPEAT;
}

static void translateBorderColor(BorderColor color, GLfloat outColor[])
{
	switch (color) {
		case BorderColor::White:
			outColor[0] = 1.0f;
			outColor[1] = 1.0f;
			outColor[2] = 1.0f;
			outColor[3] = 1.0f;
			break;
		case BorderColor::TransparentBlack:
			outColor[0] = 0.0f;
			outColor[1] = 0.0f;
			outColor[2] = 0.0f;
			outColor[3] = 0.0f;
			break;
		case BorderColor::OpaqueBlack:
			outColor[0] = 0.0f;
			outColor[1] = 0.0f;
			outColor[2] = 0.0f;
			outColor[3] = 1.0f;
			break;
		default:
			assert(0);
			break;
	}
}

#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE

GL4Sampler::GL4Sampler(const SamplerCreateInfo& createInfo)
	: m_sampler(0)
{
	glGenSamplers(1, &m_sampler);
	GLenum magFilter, minFilter;
	translateTextureFilter(createInfo.filter, magFilter, minFilter);
	glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, minFilter);
	glSamplerParameteri(m_sampler, GL_TEXTURE_MAG_FILTER, magFilter);
	glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_S, translateTextureAddressingMode(createInfo.addressingS));
	glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_T, translateTextureAddressingMode(createInfo.addressingT));
	glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_R, translateTextureAddressingMode(createInfo.addressingR));
	glSamplerParameterf(m_sampler, GL_TEXTURE_LOD_BIAS, createInfo.mipLODBias);
	if (createInfo.filter == TextureFilter::Anisotropic) {
		glSamplerParameterf(m_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<GLfloat>(createInfo.maxAnisotropy));
	}
	glSamplerParameteri(m_sampler, GL_TEXTURE_COMPARE_MODE, createInfo.enableCompare ? GL_COMPARE_REF_TO_TEXTURE : GL_NONE);
	glSamplerParameteri(m_sampler, GL_TEXTURE_COMPARE_FUNC, gl4TranslateCompareFunc(createInfo.compareFunc));
	glSamplerParameterf(m_sampler, GL_TEXTURE_MIN_LOD, createInfo.minLOD);
	glSamplerParameterf(m_sampler, GL_TEXTURE_MAX_LOD, createInfo.maxLOD);
	GLfloat color[4];
	translateBorderColor(createInfo.borderColor, color);
	glSamplerParameterfv(m_sampler, GL_TEXTURE_BORDER_COLOR, color);
}

GL4Sampler::~GL4Sampler()
{
	release();
}

GL4Sampler::GL4Sampler(GL4Sampler&& other)
	: m_sampler(other.m_sampler)
{
	other.m_sampler = 0;
}

GL4Sampler& GL4Sampler::operator=(GL4Sampler&& other)
{
	if (&other != this) {
		m_sampler = other.m_sampler;
		other.m_sampler = 0;
	}
	return *this;
}

void GL4Sampler::release()
{
	if (!empty()) {
		glDeleteSamplers(1, &m_sampler);
	}
	m_sampler = 0;
}

} // namespace rendercore