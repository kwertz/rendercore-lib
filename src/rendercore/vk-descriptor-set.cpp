//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-descriptor-set.hpp>

#include <vector>

namespace rendercore {

static VkDescriptorType translateDescriptorType(DescriptorType type)
{
	switch (type)
	{
		case DescriptorType::Sampler:
			return VK_DESCRIPTOR_TYPE_SAMPLER;
		case DescriptorType::CombinedImageSampler:
			return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		case DescriptorType::SampledImage:
			return VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		case DescriptorType::StorageImage:
			return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		case DescriptorType::UniformTexelBuffer:
			return VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
		case DescriptorType::StorageTexelBuffer:
			return VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
		case DescriptorType::UniformBuffer:
			return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		case DescriptorType::StorageBuffer:
			return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		case DescriptorType::UniformBufferDynamic:
			return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		case DescriptorType::StorageBufferDynamic:
			return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
		case DescriptorType::InputAttachment:
			return VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		default:
			break;
	}

	return VkDescriptorType();
}

static VkShaderStageFlags translateShaderStageFlags(int stageFlags)
{
	VkShaderStageFlags flags = 0;
	if (stageFlags & SHADER_STAGE_VERTEX_BIT) {
		flags |= VK_SHADER_STAGE_VERTEX_BIT;
	}
	if (stageFlags & SHADER_STAGE_TESSELLATION_CONTROL_BIT) {
		flags |= VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
	}
	if (stageFlags & SHADER_STAGE_TESSELLATION_EVALUATION_BIT) {
		flags |= VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
	}
	if (stageFlags & SHADER_STAGE_GEOMETRY_BIT) {
		flags |= VK_SHADER_STAGE_GEOMETRY_BIT;
	}
	if (stageFlags & SHADER_STAGE_FRAGMENT_BIT) {
		flags |= VK_SHADER_STAGE_FRAGMENT_BIT;
	}
	if (stageFlags & SHADER_STAGE_COMPUTE_BIT) {
		flags |= VK_SHADER_STAGE_COMPUTE_BIT;
	}
	return flags;
}

VulkanDescriptorSetLayout::VulkanDescriptorSetLayout(VkDevice device, VulkanFunctions& vk, const DescriptorSetLayoutCreateInfo& createInfo)
{
	std::vector<VkDescriptorSetLayoutBinding> bindings(createInfo.bindingCount);

	for (uint32_t i = 0; i < createInfo.bindingCount; ++i) {
		// map RenderCore -> Vulkan DescriptorSetLayoutBindings
		const DescriptorSetLayoutBinding& rcBinding = createInfo.bindings[i];
		VkDescriptorSetLayoutBinding& vkBinding = bindings[i];
		vkBinding.binding = rcBinding.binding;
		vkBinding.descriptorType = translateDescriptorType(rcBinding.descriptorType);
		vkBinding.descriptorCount = rcBinding.descriptorCount;
		vkBinding.pImmutableSamplers = nullptr;
		vkBinding.stageFlags = translateShaderStageFlags(rcBinding.stageFlags);
	}

	VkDescriptorSetLayoutCreateInfo vkCreateInfo;
	vkCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	vkCreateInfo.pNext = nullptr;
	vkCreateInfo.flags = 0;
	vkCreateInfo.bindingCount = createInfo.bindingCount;
	vkCreateInfo.pBindings = bindings.data();

	vk.CreateDescriptorSetLayout(device, &vkCreateInfo, nullptr, &m_layout);
}

VulkanDescriptorSetLayout::~VulkanDescriptorSetLayout()
{
	if (m_layout != VK_NULL_HANDLE) {
		m_vk->DestroyDescriptorSetLayout(m_device, m_layout, nullptr);
	}
}

VulkanDescriptorSetLayout::VulkanDescriptorSetLayout(VulkanDescriptorSetLayout&& other)
{

}

VulkanDescriptorSetLayout& VulkanDescriptorSetLayout::operator=(VulkanDescriptorSetLayout&& other)
{
	return *this;
}

} // namespace rendercore
