//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-context.hpp>

#include <limits>
#include <cstdint>
#include <stdexcept>
#include <cstring>
#include <rendercore/rendering-api.hpp>

namespace rendercore {

void makeGLFramebufferConfig(GLFramebufferConfig& fbconfig, const FramebufferSettings& fbsettings)
{
	int bits;
	switch (fbsettings.numFormat) {
		case NumberFormat::UnsignedNorm8:
		case NumberFormat::SignedNorm8:
		case NumberFormat::UnsignedInt8:
		case NumberFormat::SignedInt8: {
			bits = 8;
			break;
		}
		case NumberFormat::UnsignedNorm16:
		case NumberFormat::SignedNorm16:
		case NumberFormat::UnsignedInt16:
		case NumberFormat::SignedInt16: {
			bits = 16;
			break;
		}
		case NumberFormat::UnsignedNorm32:
		case NumberFormat::SignedNorm32:
		case NumberFormat::UnsignedInt32:
		case NumberFormat::SignedInt32: {
			bits = 32;
			break;
		}
		default: {
			bits = 8;
			break;
		}
	}
	fbconfig.redBits = bits;
	fbconfig.greenBits = bits;
	fbconfig.blueBits = bits;
	fbconfig.alphaBits = bits;
	fbconfig.depthBits = 32; // FIXME: hardcoded
	fbconfig.stencilBits = 8; // FIXME: hardcoded
	// using the accumulation buffer is not supported by GL4
	fbconfig.accumRedBits = 0;
	fbconfig.accumGreenBits = 0;
	fbconfig.accumBlueBits = 0;
	fbconfig.accumAlphaBits = 0;
	fbconfig.auxBuffers = 0; // no aux buffers supported
	fbconfig.stereo = false;
	fbconfig.samples = 1; // FIXME: support multisampling
	fbconfig.sRGB = fbsettings.srgb;
	fbconfig.doublebuffer = fbsettings.swapchain == SwapchainLayout::Doublebuffer;
	fbconfig.handle = 0;
}

const GLFramebufferConfig& chooseFramebufferConfig(const GLFramebufferConfig& desired, const std::vector<GLFramebufferConfig>& usableConfigs)
{
	uint32_t missing, leastMissing = std::numeric_limits<uint32_t>::max();
	uint32_t colorDiff, leastColorDiff = std::numeric_limits<uint32_t>::max();
	uint32_t extraDiff, leastExtraDiff = std::numeric_limits<uint32_t>::max();
	const GLFramebufferConfig* closest = nullptr;

	int n = static_cast<int>(usableConfigs.size());
	for (int i = 0; i < n; ++i) {
		const GLFramebufferConfig& current = usableConfigs[i];

		if (desired.stereo && !current.stereo) {
			continue;
		}

		if (desired.doublebuffer && !current.doublebuffer) {
			continue;
		}

		// count number of missing buffers
		{
			missing = 0;

			if (desired.alphaBits > 0 && current.alphaBits == 0) {
				++missing;
			}
			if (desired.depthBits > 0 && current.depthBits == 0) {
				++missing;
			}
			if (desired.auxBuffers > 0 && (current.auxBuffers < desired.auxBuffers)) {
				++missing;
			}
			if (desired.samples > 0 && current.samples == 0) {
				++missing;
			}
		}

		// calculate color channel size difference
		{
			colorDiff = 0;

			if (desired.redBits > 0) {
				colorDiff += (desired.redBits - current.redBits) *
				             (desired.redBits - current.redBits);
			}
			if (desired.greenBits > 0) {
				colorDiff += (desired.greenBits - current.greenBits) *
				             (desired.greenBits - current.greenBits);
			}
			if (desired.blueBits > 0) {
				colorDiff += (desired.blueBits - current.blueBits) *
				             (desired.blueBits - current.blueBits);
			}
		}

		// calculate non-color channel size difference
		{
			extraDiff = 0;

			if (desired.alphaBits > 0) {
				extraDiff += (desired.alphaBits - current.alphaBits) *
				             (desired.alphaBits - current.alphaBits);
			}

			if (desired.depthBits > 0) {
				extraDiff += (desired.depthBits - current.depthBits) *
				             (desired.depthBits - current.depthBits);
			}

			if (desired.stencilBits > 0) {
				extraDiff += (desired.stencilBits - current.stencilBits) *
				             (desired.stencilBits - current.stencilBits);
			}

			if (desired.accumRedBits > 0) {
				extraDiff += (desired.accumRedBits - current.accumRedBits) *
				             (desired.accumRedBits - current.accumRedBits);
			}

			if (desired.accumGreenBits > 0) {
				extraDiff += (desired.accumGreenBits - current.accumGreenBits) *
				             (desired.accumGreenBits - current.accumGreenBits);
			}

			if (desired.accumBlueBits > 0) {
				extraDiff += (desired.accumBlueBits - current.accumBlueBits) *
				             (desired.accumBlueBits - current.accumBlueBits);
			}

			if (desired.accumAlphaBits > 0) {
				extraDiff += (desired.accumAlphaBits - current.accumAlphaBits) *
				             (desired.accumAlphaBits - current.accumAlphaBits);
			}

			if (desired.samples > 0) {
				extraDiff += (desired.samples - current.samples) *
				             (desired.samples - current.samples);
			}

			if (desired.sRGB && !current.sRGB) {
				extraDiff++;
			}
		}

		if (missing < leastMissing) {
			closest = &current;
		} else if (missing == leastMissing) {
			if ((colorDiff < leastColorDiff) ||
			    (colorDiff == leastColorDiff && extraDiff < leastExtraDiff)) {
				closest = &current;
			}
		}

		if (&current == closest) {
			leastMissing = missing;
			leastColorDiff = colorDiff;
			leastExtraDiff = extraDiff;
		}
	}

	return *closest;
}

bool stringInExtensionString(const char* string, const char* extensions)
{
	const char* start = extensions;

	for (;;) {
		const char* where;
		const char* terminator;

		where = strstr(start, string);
		if (!where) {
			return false;
		}

		terminator = where + strlen(string);
		if (where == start || *(where - 1) == ' ') {
			if (*terminator == ' ' || *terminator == '\0') {
				break;
			}
		}

		start = terminator;
	}

	return true;
}

} // namespace rendercore
