//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-render-dispatcher.hpp>

// TODO: clears, descriptor sets

#include <cassert>
#include <cstring>
#include <rendercore/gl4-render-storage.hpp>
#include <GL/gl3w.h>
#include <rendercore/gl4-rendering-pipeline.hpp>
#include <rendercore/gl4-vertex-stream.hpp>
#include <rendercore/gl4-graphics-memory.hpp>
#include <rendercore/gl4-texture-set.hpp>
#include <rendercore/gl4-render-output-target.hpp>

namespace rendercore {

GL4RenderDispatcher::GL4RenderDispatcher()
	: m_pipeline(nullptr),
	  m_vertexStream(nullptr), m_constantsMemories(), m_textureSet(nullptr),
	  m_renderOutputTarget(nullptr)
{}

GL4RenderDispatcher::~GL4RenderDispatcher()
{}

void GL4RenderDispatcher::dispatch(const GL4RenderStorage* storage)
{
	assert(storage->m_immutable);
	const std::vector<RenderUnit>& renderUnits = storage->m_renderUnits;
	for (const RenderUnit& renderUnit : renderUnits) {
		assert(renderUnit.renderPass.outputTarget != nullptr);
		switch (renderUnit.type) {
			case RenderUnitType::Draw:
			case RenderUnitType::Draw_BeginRenderPass:
			case RenderUnitType::Draw_EndRenderPass:
				dispatchDraw(renderUnit);
				break;
			default:
				assert(0);
				break;
		}
	}
}

void GL4RenderDispatcher::invalidatePipeline()
{
	m_pipeline = nullptr;
}

void GL4RenderDispatcher::invalidateVertexStream()
{
	m_vertexStream = nullptr;
}

void GL4RenderDispatcher::invalidateConstantsMemories()
{
	std::memset(m_constantsMemories, 0, sizeof(m_constantsMemories));
}

void GL4RenderDispatcher::invalidateConstantsMemory(int which)
{
	m_constantsMemories[which] = {};
}

void GL4RenderDispatcher::invalidateTextureSet()
{
	m_textureSet = nullptr;
}

void GL4RenderDispatcher::invalidateRenderOutputTarget()
{
	m_renderOutputTarget = nullptr;
}

void GL4RenderDispatcher::dispatchDraw(const RenderUnit& unit)
{
	// TODO: bind descriptor resources

	// TODO: bind textures

	const GL4RenderOutputTarget* outputTarget = static_cast<const GL4RenderOutputTarget*>(unit.renderPass.outputTarget);

	bool outputTargetChanged = outputTarget != m_renderOutputTarget;
	if (outputTargetChanged) {
		outputTarget->bind();
	}

	m_renderOutputTarget = outputTarget;

	// bind the rendering pipeline
	const GL4RenderingPipeline* pipeline = static_cast<const GL4RenderingPipeline*>(unit.pipeline);

	if (pipeline != m_pipeline) {
		pipeline->bind();
		pipeline->setDrawBuffers(outputTarget->isDefaultFramebuffer());
	}

	m_pipeline = pipeline;

	const GL4VertexStream* vertexStream = static_cast<const GL4VertexStream*>(unit.vstream);
	assert(vertexStream != nullptr);

	if (vertexStream != m_vertexStream) {
		vertexStream->bind(pipeline->m_pipelineInfo.bindPointStrides);
	}

	const auto& streamDrawInfo = vertexStream->m_drawInfo;

	glDrawElementsInstanced(pipeline->m_pipelineInfo.mode, streamDrawInfo.count, streamDrawInfo.type, streamDrawInfo.ptr, static_cast<GLsizei>(unit.instanceCount));

	m_vertexStream = vertexStream;
}

} // namespace rendercore
