//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-rendering-api.hpp>

#include <stdexcept>
#include <GL/gl3w.h>

namespace rendercore {

static const int kDefaultSmallPoolSize = 1024;
static const int kDefaultMediumPoolSize = 4096;

#ifdef RENDERCORE_USE_PLATFORM_WIN32
static int s_wglUseCount = 0;
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
static int s_glxUseCount = 0;
#endif

GL4RenderingAPI::GL4RenderingAPI(const RenderingAPICreateInfo& createInfo)
	: m_propertyCache(), m_renderDispatcher(), m_renderQueue(m_renderDispatcher),
	  m_defaultRenderOutputTarget(GL4DefaultFramebufferToken(createInfo.framebufferSettings.srgb)),
	  m_currentPlatform(createInfo.platformType),
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	  m_wglCtx(),
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	  m_glxCtx(),
#endif
	  m_cleanup(*this), // fine since we're not accessing members
	  m_swapchain(),
	  m_renderStoragePool(kDefaultSmallPoolSize),
	  m_gfxMemoryPool(kDefaultMediumPoolSize), m_shaderPool(kDefaultMediumPoolSize),
	  m_pipelinePool(kDefaultMediumPoolSize),
	  m_vertexStreamPool(kDefaultMediumPoolSize), m_samplerPool(kDefaultMediumPoolSize),
	  m_texturePool(kDefaultMediumPoolSize), m_textureSetPool(kDefaultSmallPoolSize),
	  m_renderOutputBufferPool(kDefaultSmallPoolSize), m_renderOutputTargetPool(kDefaultSmallPoolSize)
{
	initializeGLPlatform(createInfo.platformType, *createInfo.surface);
	createGLContext(createInfo);
	if (gl3wInit() != 0) {
		throw APICreationException("OpenGL API loader failed");
	}
	if (gl3wIsSupported(4, 3) <= 0) {
		throw APICreationException("Requested OpenGL version is unsupported");
	}
	fillPropertyCache();
}

GL4RenderingAPI::~GL4RenderingAPI()
{}

int GL4RenderingAPI::getPropertyInteger(RenderingAPIProperty which)
{
	switch (which) {
		case RenderingAPIProperty::ShaderConstantsOffsetAlignment:
			return m_propertyCache.uboOffsetAlignment;
		default:
			break;
	}
	return -1;
}

RenderQueue* GL4RenderingAPI::getRenderQueue(RenderQueueType type)
{
	return type == RenderQueueType::Universal ? &m_renderQueue : nullptr;
}

RenderOutputTarget* GL4RenderingAPI::getDefaultRenderOutputTarget()
{
	return &m_defaultRenderOutputTarget;
}

Swapchain* GL4RenderingAPI::getSwapchain()
{
	return m_swapchain.get();
}

RenderStorage* GL4RenderingAPI::createRenderStorage()
{
	return m_renderStoragePool.create();
}

GraphicsMemory* GL4RenderingAPI::createVertexMemory(bool dynamic)
{
	return m_gfxMemoryPool.create(GL_ARRAY_BUFFER, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW, m_renderDispatcher);
}

GraphicsMemory* GL4RenderingAPI::createIndexMemory(bool dynamic)
{
	return m_gfxMemoryPool.create(GL_ELEMENT_ARRAY_BUFFER, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW, m_renderDispatcher);
}

GraphicsMemory* GL4RenderingAPI::createShaderConstantsMemory()
{
	return m_gfxMemoryPool.create(GL_UNIFORM_BUFFER, GL_DYNAMIC_DRAW, m_renderDispatcher);
}

Shader* GL4RenderingAPI::createShader(const ShaderCreateInfo& createInfo)
{
	return m_shaderPool.create(createInfo);
}

RenderingPipeline* GL4RenderingAPI::createRenderingPipeline(const RenderingPipelineCreateInfo& createInfo)
{
	return m_pipelinePool.create(createInfo, m_renderDispatcher);
}

VertexStream* GL4RenderingAPI::createVertexStream(const VertexStreamCreateInfo& createInfo)
{
	return m_vertexStreamPool.create(createInfo, m_renderDispatcher);
}

Sampler* GL4RenderingAPI::createSampler(const SamplerCreateInfo& createInfo)
{
	return m_samplerPool.create(createInfo);
}

Texture* GL4RenderingAPI::createTexture(const TextureCreateInfo& createInfo)
{
	return m_texturePool.create(createInfo, m_renderDispatcher);
}

TextureSet* GL4RenderingAPI::createTextureSet(const TextureSetCreateInfo& createInfo)
{
	return m_textureSetPool.create(createInfo);
}

RenderOutputBuffer* GL4RenderingAPI::createRenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo)
{
	return m_renderOutputBufferPool.create(createInfo);
}

RenderOutputTarget* GL4RenderingAPI::createRenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo)
{
	return m_renderOutputTargetPool.create(createInfo, m_renderDispatcher);
}

DescriptorSetLayout* GL4RenderingAPI::createDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

PipelineLayout*GL4RenderingAPI::createPipelineLayout(const PipelineLayoutCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

DescriptorPool* GL4RenderingAPI::createDescriptorPool(const DescriptorPoolCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

RenderPass* GL4RenderingAPI::createRenderPass(const RenderPassCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

void GL4RenderingAPI::releaseRenderStorage(RenderStorage* storage)
{
	m_renderStoragePool.release(static_cast<GL4RenderStorage*>(storage));
}

void GL4RenderingAPI::releaseVertexMemory(GraphicsMemory* gfxMemory)
{
	m_gfxMemoryPool.release(static_cast<GL4GraphicsMemory*>(gfxMemory));
}

void GL4RenderingAPI::releaseIndexMemory(GraphicsMemory* gfxMemory)
{
	m_gfxMemoryPool.release(static_cast<GL4GraphicsMemory*>(gfxMemory));
}

void GL4RenderingAPI::releaseShaderConstantsMemory(GraphicsMemory* gfxMemory)
{
	m_gfxMemoryPool.release(static_cast<GL4GraphicsMemory*>(gfxMemory));
}

void GL4RenderingAPI::releaseShader(Shader* shader)
{
	m_shaderPool.release(static_cast<GL4Shader*>(shader));
}

void GL4RenderingAPI::releaseRenderingPipeline(RenderingPipeline* pipeline)
{
	m_pipelinePool.release(static_cast<GL4RenderingPipeline*>(pipeline));
}

void GL4RenderingAPI::releaseVertexStream(VertexStream* stream)
{
	m_vertexStreamPool.release(static_cast<GL4VertexStream*>(stream));
}

void GL4RenderingAPI::releaseSampler(Sampler* sampler)
{
	m_samplerPool.release(static_cast<GL4Sampler*>(sampler));
}

void GL4RenderingAPI::releaseTexture(Texture* texture)
{
	m_texturePool.release(static_cast<GL4Texture*>(texture));
}

void GL4RenderingAPI::releaseTextureSet(TextureSet* textureSet)
{
	m_textureSetPool.release(static_cast<GL4TextureSet*>(textureSet));
}

void GL4RenderingAPI::releaseRenderOutputBuffer(RenderOutputBuffer* buffer)
{
	m_renderOutputBufferPool.release(static_cast<GL4RenderOutputBuffer*>(buffer));
}

void GL4RenderingAPI::releaseRenderOutputTarget(RenderOutputTarget* buffer)
{
	m_renderOutputTargetPool.release(static_cast<GL4RenderOutputTarget*>(buffer));
}

void GL4RenderingAPI::releaseDescriptorSetLayout(DescriptorSetLayout* layout)
{
	throw std::logic_error("Not implemented");
}

void GL4RenderingAPI::releasePipelineLayout(PipelineLayout* layout)
{
	throw std::logic_error("Not implemented");
}

void GL4RenderingAPI::releaseDescriptorPool(DescriptorPool* pool)
{
	throw std::logic_error("Not implemented");
}

void GL4RenderingAPI::releaseRenderPass(RenderPass* pass)
{
	throw std::logic_error("Not implemented");
}

void GL4RenderingAPI::initializeGLPlatform(Platform platformType, const PlatformSurfaceReference& surface)
{
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	if (platformType != Platform::Win32) {
		throw std::invalid_argument("Platform type must be Win32");
	}
	if (s_wglUseCount++ == 0) {
		initializeWGL();
	}
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	if (platformType == Platform::Xlib) {
		if (s_glxUseCount++ == 0) {
			initializeGLX(surface.xlib.display);
		}
	}
#endif
}

void GL4RenderingAPI::terminateGLPlatform()
{
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	if (--s_wglUseCount == 0) {
		terminateWGL();
	}
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	if (--s_glxUseCount == 0) {
		terminateGLX();
	}
#endif
}

void GL4RenderingAPI::createGLContext(const RenderingAPICreateInfo& createInfo)
{
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	createContextWGL(m_wglCtx, m_swapchain, createInfo);
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	if (createInfo.platformType == Platform::Xlib) {
		createContextGLX(m_glxCtx, m_swapchain, createInfo);
	}
#endif
}

void GL4RenderingAPI::destroyGLContext()
{
#ifdef RENDERCORE_USE_PLATFORM_WIN32
	destroyContextWGL(m_wglCtx);
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
	if (m_currentPlatform == Platform::Xlib) {
		destroyContextGLX(m_glxCtx);
	}
#endif
}

void GL4RenderingAPI::fillPropertyCache()
{
	glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &m_propertyCache.uboOffsetAlignment);
}

GL4RenderingAPI::Cleanup::Cleanup(GL4RenderingAPI& api) : api(api)
{}

GL4RenderingAPI::Cleanup::~Cleanup()
{
	api.destroyGLContext();
	api.terminateGLPlatform();
}

} // namespace rendercore
