//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-functions.hpp>

#include <rendercore/vk-platform.hpp>

namespace rendercore {

template <typename Func>
static Func vkfunc_cast(PFN_vkVoidFunction func)
{
	return reinterpret_cast<Func>(func);
}

void loadVulkanInstanceProcs(VulkanFunctions& functions, VkInstance instance)
{
	if (instance == nullptr) {
		functions.CreateInstance = vkfunc_cast<PFN_vkCreateInstance>(getInstanceProcAddress(nullptr, "vkCreateInstance"));
		functions.EnumerateInstanceExtensionProperties = vkfunc_cast<PFN_vkEnumerateInstanceExtensionProperties>(getInstanceProcAddress(nullptr, "vkEnumerateInstanceExtensionProperties"));
		functions.EnumerateInstanceLayerProperties = vkfunc_cast<PFN_vkEnumerateInstanceLayerProperties>(getInstanceProcAddress(nullptr, "vkEnumerateInstanceLayerProperties"));
	} else {
		functions.GetDeviceProcAddr = vkfunc_cast<PFN_vkGetDeviceProcAddr>(getInstanceProcAddress(instance, "vkGetDeviceProcAddr"));
		// obtained using
		// typedef \w+ \(VKAPI_PTR \*(PFN_(vk(\w+)))\)\(VkInstance
		// functions.$3 = vkfunc_cast<$1>(getInstanceProcAddress(instance, "$2"));\n
		functions.DestroyInstance = vkfunc_cast<PFN_vkDestroyInstance>(getInstanceProcAddress(instance, "vkDestroyInstance"));
		functions.EnumeratePhysicalDevices = vkfunc_cast<PFN_vkEnumeratePhysicalDevices>(getInstanceProcAddress(instance, "vkEnumeratePhysicalDevices"));
		functions.DestroySurfaceKHR = vkfunc_cast<PFN_vkDestroySurfaceKHR>(getInstanceProcAddress(instance, "vkDestroySurfaceKHR"));
		functions.CreateDisplayPlaneSurfaceKHR = vkfunc_cast<PFN_vkCreateDisplayPlaneSurfaceKHR>(getInstanceProcAddress(instance, "vkCreateDisplayPlaneSurfaceKHR"));
		functions.CreateDebugReportCallbackEXT = vkfunc_cast<PFN_vkCreateDebugReportCallbackEXT>(getInstanceProcAddress(instance, "vkCreateDebugReportCallbackEXT"));
		functions.DestroyDebugReportCallbackEXT = vkfunc_cast<PFN_vkDestroyDebugReportCallbackEXT>(getInstanceProcAddress(instance, "vkDestroyDebugReportCallbackEXT"));
		functions.DebugReportMessageEXT = vkfunc_cast<PFN_vkDebugReportMessageEXT>(getInstanceProcAddress(instance, "vkDebugReportMessageEXT"));

		// obtained using
		// typedef \w+ \(VKAPI_PTR \*(PFN_(vk(\w+)))\)\(VkPhysicalDevice
		// functions.$3 = vkfunc_cast<$1>(getInstanceProcAddress(instance, "$2"));\n
		functions.GetPhysicalDeviceFeatures = vkfunc_cast<PFN_vkGetPhysicalDeviceFeatures>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceFeatures"));
		functions.GetPhysicalDeviceFormatProperties = vkfunc_cast<PFN_vkGetPhysicalDeviceFormatProperties>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceFormatProperties"));
		functions.GetPhysicalDeviceImageFormatProperties = vkfunc_cast<PFN_vkGetPhysicalDeviceImageFormatProperties>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceImageFormatProperties"));
		functions.GetPhysicalDeviceProperties = vkfunc_cast<PFN_vkGetPhysicalDeviceProperties>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceProperties"));
		functions.GetPhysicalDeviceQueueFamilyProperties = vkfunc_cast<PFN_vkGetPhysicalDeviceQueueFamilyProperties>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceQueueFamilyProperties"));
		functions.GetPhysicalDeviceMemoryProperties = vkfunc_cast<PFN_vkGetPhysicalDeviceMemoryProperties>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceMemoryProperties"));
		functions.CreateDevice = vkfunc_cast<PFN_vkCreateDevice>(getInstanceProcAddress(instance, "vkCreateDevice"));
		functions.EnumerateDeviceExtensionProperties = vkfunc_cast<PFN_vkEnumerateDeviceExtensionProperties>(getInstanceProcAddress(instance, "vkEnumerateDeviceExtensionProperties"));
		functions.EnumerateDeviceLayerProperties = vkfunc_cast<PFN_vkEnumerateDeviceLayerProperties>(getInstanceProcAddress(instance, "vkEnumerateDeviceLayerProperties"));
		functions.GetPhysicalDeviceSparseImageFormatProperties = vkfunc_cast<PFN_vkGetPhysicalDeviceSparseImageFormatProperties>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceSparseImageFormatProperties"));
		functions.GetPhysicalDeviceSurfaceSupportKHR = vkfunc_cast<PFN_vkGetPhysicalDeviceSurfaceSupportKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceSurfaceSupportKHR"));
		functions.GetPhysicalDeviceSurfaceCapabilitiesKHR = vkfunc_cast<PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR"));
		functions.GetPhysicalDeviceSurfaceFormatsKHR = vkfunc_cast<PFN_vkGetPhysicalDeviceSurfaceFormatsKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceSurfaceFormatsKHR"));
		functions.GetPhysicalDeviceSurfacePresentModesKHR = vkfunc_cast<PFN_vkGetPhysicalDeviceSurfacePresentModesKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceSurfacePresentModesKHR"));
		functions.GetPhysicalDeviceDisplayPropertiesKHR = vkfunc_cast<PFN_vkGetPhysicalDeviceDisplayPropertiesKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceDisplayPropertiesKHR"));
		functions.GetPhysicalDeviceDisplayPlanePropertiesKHR = vkfunc_cast<PFN_vkGetPhysicalDeviceDisplayPlanePropertiesKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceDisplayPlanePropertiesKHR"));
		functions.GetDisplayPlaneSupportedDisplaysKHR = vkfunc_cast<PFN_vkGetDisplayPlaneSupportedDisplaysKHR>(getInstanceProcAddress(instance, "vkGetDisplayPlaneSupportedDisplaysKHR"));
		functions.GetDisplayModePropertiesKHR = vkfunc_cast<PFN_vkGetDisplayModePropertiesKHR>(getInstanceProcAddress(instance, "vkGetDisplayModePropertiesKHR"));
		functions.CreateDisplayModeKHR = vkfunc_cast<PFN_vkCreateDisplayModeKHR>(getInstanceProcAddress(instance, "vkCreateDisplayModeKHR"));
		functions.GetDisplayPlaneCapabilitiesKHR = vkfunc_cast<PFN_vkGetDisplayPlaneCapabilitiesKHR>(getInstanceProcAddress(instance, "vkGetDisplayPlaneCapabilitiesKHR"));
	}
}

void loadVulkanDeviceProcs(VulkanFunctions& functions, VkDevice device)
{
	// obtained using
	// typedef \w+ \(VKAPI_PTR \*(PFN_(vk(\w+)))\)\(VkDevice
	// functions.$3 = vkfunc_cast<$1>(functions.GetDeviceProcAddr(device, "$2"));\n
	functions.GetDeviceProcAddr = vkfunc_cast<PFN_vkGetDeviceProcAddr>(functions.GetDeviceProcAddr(device, "vkGetDeviceProcAddr"));
	functions.DestroyDevice = vkfunc_cast<PFN_vkDestroyDevice>(functions.GetDeviceProcAddr(device, "vkDestroyDevice"));
	functions.GetDeviceQueue = vkfunc_cast<PFN_vkGetDeviceQueue>(functions.GetDeviceProcAddr(device, "vkGetDeviceQueue"));
	functions.DeviceWaitIdle = vkfunc_cast<PFN_vkDeviceWaitIdle>(functions.GetDeviceProcAddr(device, "vkDeviceWaitIdle"));
	functions.AllocateMemory = vkfunc_cast<PFN_vkAllocateMemory>(functions.GetDeviceProcAddr(device, "vkAllocateMemory"));
	functions.FreeMemory = vkfunc_cast<PFN_vkFreeMemory>(functions.GetDeviceProcAddr(device, "vkFreeMemory"));
	functions.MapMemory = vkfunc_cast<PFN_vkMapMemory>(functions.GetDeviceProcAddr(device, "vkMapMemory"));
	functions.UnmapMemory = vkfunc_cast<PFN_vkUnmapMemory>(functions.GetDeviceProcAddr(device, "vkUnmapMemory"));
	functions.FlushMappedMemoryRanges = vkfunc_cast<PFN_vkFlushMappedMemoryRanges>(functions.GetDeviceProcAddr(device, "vkFlushMappedMemoryRanges"));
	functions.InvalidateMappedMemoryRanges = vkfunc_cast<PFN_vkInvalidateMappedMemoryRanges>(functions.GetDeviceProcAddr(device, "vkInvalidateMappedMemoryRanges"));
	functions.GetDeviceMemoryCommitment = vkfunc_cast<PFN_vkGetDeviceMemoryCommitment>(functions.GetDeviceProcAddr(device, "vkGetDeviceMemoryCommitment"));
	functions.BindBufferMemory = vkfunc_cast<PFN_vkBindBufferMemory>(functions.GetDeviceProcAddr(device, "vkBindBufferMemory"));
	functions.BindImageMemory = vkfunc_cast<PFN_vkBindImageMemory>(functions.GetDeviceProcAddr(device, "vkBindImageMemory"));
	functions.GetBufferMemoryRequirements = vkfunc_cast<PFN_vkGetBufferMemoryRequirements>(functions.GetDeviceProcAddr(device, "vkGetBufferMemoryRequirements"));
	functions.GetImageMemoryRequirements = vkfunc_cast<PFN_vkGetImageMemoryRequirements>(functions.GetDeviceProcAddr(device, "vkGetImageMemoryRequirements"));
	functions.GetImageSparseMemoryRequirements = vkfunc_cast<PFN_vkGetImageSparseMemoryRequirements>(functions.GetDeviceProcAddr(device, "vkGetImageSparseMemoryRequirements"));
	functions.CreateFence = vkfunc_cast<PFN_vkCreateFence>(functions.GetDeviceProcAddr(device, "vkCreateFence"));
	functions.DestroyFence = vkfunc_cast<PFN_vkDestroyFence>(functions.GetDeviceProcAddr(device, "vkDestroyFence"));
	functions.ResetFences = vkfunc_cast<PFN_vkResetFences>(functions.GetDeviceProcAddr(device, "vkResetFences"));
	functions.GetFenceStatus = vkfunc_cast<PFN_vkGetFenceStatus>(functions.GetDeviceProcAddr(device, "vkGetFenceStatus"));
	functions.WaitForFences = vkfunc_cast<PFN_vkWaitForFences>(functions.GetDeviceProcAddr(device, "vkWaitForFences"));
	functions.CreateSemaphore = vkfunc_cast<PFN_vkCreateSemaphore>(functions.GetDeviceProcAddr(device, "vkCreateSemaphore"));
	functions.DestroySemaphore = vkfunc_cast<PFN_vkDestroySemaphore>(functions.GetDeviceProcAddr(device, "vkDestroySemaphore"));
	functions.CreateEvent = vkfunc_cast<PFN_vkCreateEvent>(functions.GetDeviceProcAddr(device, "vkCreateEvent"));
	functions.DestroyEvent = vkfunc_cast<PFN_vkDestroyEvent>(functions.GetDeviceProcAddr(device, "vkDestroyEvent"));
	functions.GetEventStatus = vkfunc_cast<PFN_vkGetEventStatus>(functions.GetDeviceProcAddr(device, "vkGetEventStatus"));
	functions.SetEvent = vkfunc_cast<PFN_vkSetEvent>(functions.GetDeviceProcAddr(device, "vkSetEvent"));
	functions.ResetEvent = vkfunc_cast<PFN_vkResetEvent>(functions.GetDeviceProcAddr(device, "vkResetEvent"));
	functions.CreateQueryPool = vkfunc_cast<PFN_vkCreateQueryPool>(functions.GetDeviceProcAddr(device, "vkCreateQueryPool"));
	functions.DestroyQueryPool = vkfunc_cast<PFN_vkDestroyQueryPool>(functions.GetDeviceProcAddr(device, "vkDestroyQueryPool"));
	functions.GetQueryPoolResults = vkfunc_cast<PFN_vkGetQueryPoolResults>(functions.GetDeviceProcAddr(device, "vkGetQueryPoolResults"));
	functions.CreateBuffer = vkfunc_cast<PFN_vkCreateBuffer>(functions.GetDeviceProcAddr(device, "vkCreateBuffer"));
	functions.DestroyBuffer = vkfunc_cast<PFN_vkDestroyBuffer>(functions.GetDeviceProcAddr(device, "vkDestroyBuffer"));
	functions.CreateBufferView = vkfunc_cast<PFN_vkCreateBufferView>(functions.GetDeviceProcAddr(device, "vkCreateBufferView"));
	functions.DestroyBufferView = vkfunc_cast<PFN_vkDestroyBufferView>(functions.GetDeviceProcAddr(device, "vkDestroyBufferView"));
	functions.CreateImage = vkfunc_cast<PFN_vkCreateImage>(functions.GetDeviceProcAddr(device, "vkCreateImage"));
	functions.DestroyImage = vkfunc_cast<PFN_vkDestroyImage>(functions.GetDeviceProcAddr(device, "vkDestroyImage"));
	functions.GetImageSubresourceLayout = vkfunc_cast<PFN_vkGetImageSubresourceLayout>(functions.GetDeviceProcAddr(device, "vkGetImageSubresourceLayout"));
	functions.CreateImageView = vkfunc_cast<PFN_vkCreateImageView>(functions.GetDeviceProcAddr(device, "vkCreateImageView"));
	functions.DestroyImageView = vkfunc_cast<PFN_vkDestroyImageView>(functions.GetDeviceProcAddr(device, "vkDestroyImageView"));
	functions.CreateShaderModule = vkfunc_cast<PFN_vkCreateShaderModule>(functions.GetDeviceProcAddr(device, "vkCreateShaderModule"));
	functions.DestroyShaderModule = vkfunc_cast<PFN_vkDestroyShaderModule>(functions.GetDeviceProcAddr(device, "vkDestroyShaderModule"));
	functions.CreatePipelineCache = vkfunc_cast<PFN_vkCreatePipelineCache>(functions.GetDeviceProcAddr(device, "vkCreatePipelineCache"));
	functions.DestroyPipelineCache = vkfunc_cast<PFN_vkDestroyPipelineCache>(functions.GetDeviceProcAddr(device, "vkDestroyPipelineCache"));
	functions.GetPipelineCacheData = vkfunc_cast<PFN_vkGetPipelineCacheData>(functions.GetDeviceProcAddr(device, "vkGetPipelineCacheData"));
	functions.MergePipelineCaches = vkfunc_cast<PFN_vkMergePipelineCaches>(functions.GetDeviceProcAddr(device, "vkMergePipelineCaches"));
	functions.CreateGraphicsPipelines = vkfunc_cast<PFN_vkCreateGraphicsPipelines>(functions.GetDeviceProcAddr(device, "vkCreateGraphicsPipelines"));
	functions.CreateComputePipelines = vkfunc_cast<PFN_vkCreateComputePipelines>(functions.GetDeviceProcAddr(device, "vkCreateComputePipelines"));
	functions.DestroyPipeline = vkfunc_cast<PFN_vkDestroyPipeline>(functions.GetDeviceProcAddr(device, "vkDestroyPipeline"));
	functions.CreatePipelineLayout = vkfunc_cast<PFN_vkCreatePipelineLayout>(functions.GetDeviceProcAddr(device, "vkCreatePipelineLayout"));
	functions.DestroyPipelineLayout = vkfunc_cast<PFN_vkDestroyPipelineLayout>(functions.GetDeviceProcAddr(device, "vkDestroyPipelineLayout"));
	functions.CreateSampler = vkfunc_cast<PFN_vkCreateSampler>(functions.GetDeviceProcAddr(device, "vkCreateSampler"));
	functions.DestroySampler = vkfunc_cast<PFN_vkDestroySampler>(functions.GetDeviceProcAddr(device, "vkDestroySampler"));
	functions.CreateDescriptorSetLayout = vkfunc_cast<PFN_vkCreateDescriptorSetLayout>(functions.GetDeviceProcAddr(device, "vkCreateDescriptorSetLayout"));
	functions.DestroyDescriptorSetLayout = vkfunc_cast<PFN_vkDestroyDescriptorSetLayout>(functions.GetDeviceProcAddr(device, "vkDestroyDescriptorSetLayout"));
	functions.CreateDescriptorPool = vkfunc_cast<PFN_vkCreateDescriptorPool>(functions.GetDeviceProcAddr(device, "vkCreateDescriptorPool"));
	functions.DestroyDescriptorPool = vkfunc_cast<PFN_vkDestroyDescriptorPool>(functions.GetDeviceProcAddr(device, "vkDestroyDescriptorPool"));
	functions.ResetDescriptorPool = vkfunc_cast<PFN_vkResetDescriptorPool>(functions.GetDeviceProcAddr(device, "vkResetDescriptorPool"));
	functions.AllocateDescriptorSets = vkfunc_cast<PFN_vkAllocateDescriptorSets>(functions.GetDeviceProcAddr(device, "vkAllocateDescriptorSets"));
	functions.FreeDescriptorSets = vkfunc_cast<PFN_vkFreeDescriptorSets>(functions.GetDeviceProcAddr(device, "vkFreeDescriptorSets"));
	functions.UpdateDescriptorSets = vkfunc_cast<PFN_vkUpdateDescriptorSets>(functions.GetDeviceProcAddr(device, "vkUpdateDescriptorSets"));
	functions.CreateFramebuffer = vkfunc_cast<PFN_vkCreateFramebuffer>(functions.GetDeviceProcAddr(device, "vkCreateFramebuffer"));
	functions.DestroyFramebuffer = vkfunc_cast<PFN_vkDestroyFramebuffer>(functions.GetDeviceProcAddr(device, "vkDestroyFramebuffer"));
	functions.CreateRenderPass = vkfunc_cast<PFN_vkCreateRenderPass>(functions.GetDeviceProcAddr(device, "vkCreateRenderPass"));
	functions.DestroyRenderPass = vkfunc_cast<PFN_vkDestroyRenderPass>(functions.GetDeviceProcAddr(device, "vkDestroyRenderPass"));
	functions.GetRenderAreaGranularity = vkfunc_cast<PFN_vkGetRenderAreaGranularity>(functions.GetDeviceProcAddr(device, "vkGetRenderAreaGranularity"));
	functions.CreateCommandPool = vkfunc_cast<PFN_vkCreateCommandPool>(functions.GetDeviceProcAddr(device, "vkCreateCommandPool"));
	functions.DestroyCommandPool = vkfunc_cast<PFN_vkDestroyCommandPool>(functions.GetDeviceProcAddr(device, "vkDestroyCommandPool"));
	functions.ResetCommandPool = vkfunc_cast<PFN_vkResetCommandPool>(functions.GetDeviceProcAddr(device, "vkResetCommandPool"));
	functions.AllocateCommandBuffers = vkfunc_cast<PFN_vkAllocateCommandBuffers>(functions.GetDeviceProcAddr(device, "vkAllocateCommandBuffers"));
	functions.FreeCommandBuffers = vkfunc_cast<PFN_vkFreeCommandBuffers>(functions.GetDeviceProcAddr(device, "vkFreeCommandBuffers"));
	functions.CreateSwapchainKHR = vkfunc_cast<PFN_vkCreateSwapchainKHR>(functions.GetDeviceProcAddr(device, "vkCreateSwapchainKHR"));
	functions.DestroySwapchainKHR = vkfunc_cast<PFN_vkDestroySwapchainKHR>(functions.GetDeviceProcAddr(device, "vkDestroySwapchainKHR"));
	functions.GetSwapchainImagesKHR = vkfunc_cast<PFN_vkGetSwapchainImagesKHR>(functions.GetDeviceProcAddr(device, "vkGetSwapchainImagesKHR"));
	functions.AcquireNextImageKHR = vkfunc_cast<PFN_vkAcquireNextImageKHR>(functions.GetDeviceProcAddr(device, "vkAcquireNextImageKHR"));
	functions.CreateSharedSwapchainsKHR = vkfunc_cast<PFN_vkCreateSharedSwapchainsKHR>(functions.GetDeviceProcAddr(device, "vkCreateSharedSwapchainsKHR"));
	functions.DebugMarkerSetObjectTagEXT = vkfunc_cast<PFN_vkDebugMarkerSetObjectTagEXT>(functions.GetDeviceProcAddr(device, "vkDebugMarkerSetObjectTagEXT"));
	functions.DebugMarkerSetObjectNameEXT = vkfunc_cast<PFN_vkDebugMarkerSetObjectNameEXT>(functions.GetDeviceProcAddr(device, "vkDebugMarkerSetObjectNameEXT"));

	// obtained using
	// typedef \w+ \(VKAPI_PTR \*(PFN_(vk(\w+)))\)\(VkCommandBuffer
	// functions.$3 = vkfunc_cast<$1>(functions.GetDeviceProcAddr(device, "$2"));\n
	functions.BeginCommandBuffer = vkfunc_cast<PFN_vkBeginCommandBuffer>(functions.GetDeviceProcAddr(device, "vkBeginCommandBuffer"));
	functions.EndCommandBuffer = vkfunc_cast<PFN_vkEndCommandBuffer>(functions.GetDeviceProcAddr(device, "vkEndCommandBuffer"));
	functions.ResetCommandBuffer = vkfunc_cast<PFN_vkResetCommandBuffer>(functions.GetDeviceProcAddr(device, "vkResetCommandBuffer"));
	functions.CmdBindPipeline = vkfunc_cast<PFN_vkCmdBindPipeline>(functions.GetDeviceProcAddr(device, "vkCmdBindPipeline"));
	functions.CmdSetViewport = vkfunc_cast<PFN_vkCmdSetViewport>(functions.GetDeviceProcAddr(device, "vkCmdSetViewport"));
	functions.CmdSetScissor = vkfunc_cast<PFN_vkCmdSetScissor>(functions.GetDeviceProcAddr(device, "vkCmdSetScissor"));
	functions.CmdSetLineWidth = vkfunc_cast<PFN_vkCmdSetLineWidth>(functions.GetDeviceProcAddr(device, "vkCmdSetLineWidth"));
	functions.CmdSetDepthBias = vkfunc_cast<PFN_vkCmdSetDepthBias>(functions.GetDeviceProcAddr(device, "vkCmdSetDepthBias"));
	functions.CmdSetBlendConstants = vkfunc_cast<PFN_vkCmdSetBlendConstants>(functions.GetDeviceProcAddr(device, "vkCmdSetBlendConstants"));
	functions.CmdSetDepthBounds = vkfunc_cast<PFN_vkCmdSetDepthBounds>(functions.GetDeviceProcAddr(device, "vkCmdSetDepthBounds"));
	functions.CmdSetStencilCompareMask = vkfunc_cast<PFN_vkCmdSetStencilCompareMask>(functions.GetDeviceProcAddr(device, "vkCmdSetStencilCompareMask"));
	functions.CmdSetStencilWriteMask = vkfunc_cast<PFN_vkCmdSetStencilWriteMask>(functions.GetDeviceProcAddr(device, "vkCmdSetStencilWriteMask"));
	functions.CmdSetStencilReference = vkfunc_cast<PFN_vkCmdSetStencilReference>(functions.GetDeviceProcAddr(device, "vkCmdSetStencilReference"));
	functions.CmdBindDescriptorSets = vkfunc_cast<PFN_vkCmdBindDescriptorSets>(functions.GetDeviceProcAddr(device, "vkCmdBindDescriptorSets"));
	functions.CmdBindIndexBuffer = vkfunc_cast<PFN_vkCmdBindIndexBuffer>(functions.GetDeviceProcAddr(device, "vkCmdBindIndexBuffer"));
	functions.CmdBindVertexBuffers = vkfunc_cast<PFN_vkCmdBindVertexBuffers>(functions.GetDeviceProcAddr(device, "vkCmdBindVertexBuffers"));
	functions.CmdDraw = vkfunc_cast<PFN_vkCmdDraw>(functions.GetDeviceProcAddr(device, "vkCmdDraw"));
	functions.CmdDrawIndexed = vkfunc_cast<PFN_vkCmdDrawIndexed>(functions.GetDeviceProcAddr(device, "vkCmdDrawIndexed"));
	functions.CmdDrawIndirect = vkfunc_cast<PFN_vkCmdDrawIndirect>(functions.GetDeviceProcAddr(device, "vkCmdDrawIndirect"));
	functions.CmdDrawIndexedIndirect = vkfunc_cast<PFN_vkCmdDrawIndexedIndirect>(functions.GetDeviceProcAddr(device, "vkCmdDrawIndexedIndirect"));
	functions.CmdDispatch = vkfunc_cast<PFN_vkCmdDispatch>(functions.GetDeviceProcAddr(device, "vkCmdDispatch"));
	functions.CmdDispatchIndirect = vkfunc_cast<PFN_vkCmdDispatchIndirect>(functions.GetDeviceProcAddr(device, "vkCmdDispatchIndirect"));
	functions.CmdCopyBuffer = vkfunc_cast<PFN_vkCmdCopyBuffer>(functions.GetDeviceProcAddr(device, "vkCmdCopyBuffer"));
	functions.CmdCopyImage = vkfunc_cast<PFN_vkCmdCopyImage>(functions.GetDeviceProcAddr(device, "vkCmdCopyImage"));
	functions.CmdBlitImage = vkfunc_cast<PFN_vkCmdBlitImage>(functions.GetDeviceProcAddr(device, "vkCmdBlitImage"));
	functions.CmdCopyBufferToImage = vkfunc_cast<PFN_vkCmdCopyBufferToImage>(functions.GetDeviceProcAddr(device, "vkCmdCopyBufferToImage"));
	functions.CmdCopyImageToBuffer = vkfunc_cast<PFN_vkCmdCopyImageToBuffer>(functions.GetDeviceProcAddr(device, "vkCmdCopyImageToBuffer"));
	functions.CmdUpdateBuffer = vkfunc_cast<PFN_vkCmdUpdateBuffer>(functions.GetDeviceProcAddr(device, "vkCmdUpdateBuffer"));
	functions.CmdFillBuffer = vkfunc_cast<PFN_vkCmdFillBuffer>(functions.GetDeviceProcAddr(device, "vkCmdFillBuffer"));
	functions.CmdClearColorImage = vkfunc_cast<PFN_vkCmdClearColorImage>(functions.GetDeviceProcAddr(device, "vkCmdClearColorImage"));
	functions.CmdClearDepthStencilImage = vkfunc_cast<PFN_vkCmdClearDepthStencilImage>(functions.GetDeviceProcAddr(device, "vkCmdClearDepthStencilImage"));
	functions.CmdClearAttachments = vkfunc_cast<PFN_vkCmdClearAttachments>(functions.GetDeviceProcAddr(device, "vkCmdClearAttachments"));
	functions.CmdResolveImage = vkfunc_cast<PFN_vkCmdResolveImage>(functions.GetDeviceProcAddr(device, "vkCmdResolveImage"));
	functions.CmdSetEvent = vkfunc_cast<PFN_vkCmdSetEvent>(functions.GetDeviceProcAddr(device, "vkCmdSetEvent"));
	functions.CmdResetEvent = vkfunc_cast<PFN_vkCmdResetEvent>(functions.GetDeviceProcAddr(device, "vkCmdResetEvent"));
	functions.CmdWaitEvents = vkfunc_cast<PFN_vkCmdWaitEvents>(functions.GetDeviceProcAddr(device, "vkCmdWaitEvents"));
	functions.CmdPipelineBarrier = vkfunc_cast<PFN_vkCmdPipelineBarrier>(functions.GetDeviceProcAddr(device, "vkCmdPipelineBarrier"));
	functions.CmdBeginQuery = vkfunc_cast<PFN_vkCmdBeginQuery>(functions.GetDeviceProcAddr(device, "vkCmdBeginQuery"));
	functions.CmdEndQuery = vkfunc_cast<PFN_vkCmdEndQuery>(functions.GetDeviceProcAddr(device, "vkCmdEndQuery"));
	functions.CmdResetQueryPool = vkfunc_cast<PFN_vkCmdResetQueryPool>(functions.GetDeviceProcAddr(device, "vkCmdResetQueryPool"));
	functions.CmdWriteTimestamp = vkfunc_cast<PFN_vkCmdWriteTimestamp>(functions.GetDeviceProcAddr(device, "vkCmdWriteTimestamp"));
	functions.CmdCopyQueryPoolResults = vkfunc_cast<PFN_vkCmdCopyQueryPoolResults>(functions.GetDeviceProcAddr(device, "vkCmdCopyQueryPoolResults"));
	functions.CmdPushConstants = vkfunc_cast<PFN_vkCmdPushConstants>(functions.GetDeviceProcAddr(device, "vkCmdPushConstants"));
	functions.CmdBeginRenderPass = vkfunc_cast<PFN_vkCmdBeginRenderPass>(functions.GetDeviceProcAddr(device, "vkCmdBeginRenderPass"));
	functions.CmdNextSubpass = vkfunc_cast<PFN_vkCmdNextSubpass>(functions.GetDeviceProcAddr(device, "vkCmdNextSubpass"));
	functions.CmdEndRenderPass = vkfunc_cast<PFN_vkCmdEndRenderPass>(functions.GetDeviceProcAddr(device, "vkCmdEndRenderPass"));
	functions.CmdExecuteCommands = vkfunc_cast<PFN_vkCmdExecuteCommands>(functions.GetDeviceProcAddr(device, "vkCmdExecuteCommands"));
	functions.CmdDebugMarkerBeginEXT = vkfunc_cast<PFN_vkCmdDebugMarkerBeginEXT>(functions.GetDeviceProcAddr(device, "vkCmdDebugMarkerBeginEXT"));
	functions.CmdDebugMarkerEndEXT = vkfunc_cast<PFN_vkCmdDebugMarkerEndEXT>(functions.GetDeviceProcAddr(device, "vkCmdDebugMarkerEndEXT"));
	functions.CmdDebugMarkerInsertEXT = vkfunc_cast<PFN_vkCmdDebugMarkerInsertEXT>(functions.GetDeviceProcAddr(device, "vkCmdDebugMarkerInsertEXT"));

	// obtained using
	// typedef \w+ \(VKAPI_PTR \*(PFN_(vk(\w+)))\)\(VkQueue
	// functions.$3 = vkfunc_cast<$1>(functions.GetDeviceProcAddr(device, "$2"));\n
	functions.QueueSubmit = vkfunc_cast<PFN_vkQueueSubmit>(functions.GetDeviceProcAddr(device, "vkQueueSubmit"));
	functions.QueueWaitIdle = vkfunc_cast<PFN_vkQueueWaitIdle>(functions.GetDeviceProcAddr(device, "vkQueueWaitIdle"));
	functions.QueueBindSparse = vkfunc_cast<PFN_vkQueueBindSparse>(functions.GetDeviceProcAddr(device, "vkQueueBindSparse"));
	functions.QueuePresentKHR = vkfunc_cast<PFN_vkQueuePresentKHR>(functions.GetDeviceProcAddr(device, "vkQueuePresentKHR"));
}

} // namespace rendercore
