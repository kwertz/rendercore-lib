//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-vertex-stream.hpp>
#include <stdexcept>
#include <cstring>

namespace rendercore {

VulkanVertexStream::VulkanVertexStream()
	: m_vmemViewInfos(), m_vmemViewInfoCount(0), m_imemViewInfo()
{}

VulkanVertexStream::VulkanVertexStream(const VertexStreamCreateInfo& createInfo)
	: m_vmemViewInfos(),
	  m_vmemViewInfoCount(createInfo.numVertexMemoryViewInfos),
	  m_imemViewInfo(createInfo.indexMemoryViewInfo)
{
	if (createInfo.numVertexMemoryViewInfos > kMaxVertexMemoryBindPoints) {
		throw std::invalid_argument("numVertexMemoryViewInfos must be <= kMaxVertexMemoryBindPoints");
	}

	// save them to be accessed later
	std::memcpy(m_vmemViewInfos.data(), createInfo.firstVertexMemoryViewInfo, sizeof(VertexMemoryViewInfo) * createInfo.numVertexMemoryViewInfos);
}

VulkanVertexStream::~VulkanVertexStream()
{
	release();
}

VulkanVertexStream::VulkanVertexStream(VulkanVertexStream&& other)
	: m_vmemViewInfos(std::move(other.m_vmemViewInfos)),
	  m_vmemViewInfoCount(other.m_vmemViewInfoCount),
	  m_imemViewInfo(other.m_imemViewInfo)
{}

VulkanVertexStream& VulkanVertexStream::operator=(VulkanVertexStream&& other)
{
	if (this != &other) {
		m_vmemViewInfos = std::move(other.m_vmemViewInfos);
		m_vmemViewInfoCount = other.m_vmemViewInfoCount;
		m_imemViewInfo = other.m_imemViewInfo;
	}
	return *this;
}

void VulkanVertexStream::release()
{
	m_vmemViewInfoCount = 0;
}

} // namespace rendercore
