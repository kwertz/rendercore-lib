//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-graphics-memory.hpp>

#include <GL/gl3w.h>
#include <rendercore/gl4-render-dispatcher.hpp>

namespace rendercore {

GL4GraphicsMemory::GL4GraphicsMemory()
	: m_bindingTarget(GL_ARRAY_BUFFER), m_usagePtn(GL_STATIC_DRAW), m_currentSize(0), m_dispatcher(nullptr), m_obj(0)
{}

GL4GraphicsMemory::GL4GraphicsMemory(GLenum bindingTarget, GLenum usagePattern, GL4RenderDispatcher& dispatcher)
	: m_bindingTarget(bindingTarget), m_usagePtn(usagePattern), m_currentSize(0), m_dispatcher(&dispatcher), m_obj(0)
{
	glGenBuffers(1, &m_obj);
}

GL4GraphicsMemory::~GL4GraphicsMemory()
{
	release();
}

GL4GraphicsMemory::GL4GraphicsMemory(GL4GraphicsMemory&& other)
	: m_bindingTarget(other.m_bindingTarget), m_usagePtn(other.m_usagePtn), m_currentSize(other.m_currentSize),
	  m_dispatcher(other.m_dispatcher), m_obj(other.m_obj)
{
	other.m_obj = 0;
}

GL4GraphicsMemory& GL4GraphicsMemory::operator=(GL4GraphicsMemory&& other)
{
	if (&other != this) {
		m_bindingTarget = other.m_bindingTarget;
		m_usagePtn = other.m_usagePtn;
		m_currentSize = other.m_currentSize;
		m_dispatcher = other.m_dispatcher;
		m_obj = other.m_obj;
		other.m_obj = 0;
	}
	return *this;
}

void GL4GraphicsMemory::reallocate(size_t size)
{
	m_dispatcher->invalidateConstantsMemory(0);
	glBindBuffer(m_bindingTarget, m_obj);
	glBufferData(m_bindingTarget, static_cast<GLsizeiptr>(size), nullptr, m_usagePtn);
	m_currentSize = size;
}

CPUMemoryPointer GL4GraphicsMemory::pin(ptrdiff_t offset, size_t length, int flags)
{
	m_dispatcher->invalidateConstantsMemory(0);
	glBindBuffer(m_bindingTarget, m_obj);
	GLbitfield access = 0;
	access |= (flags & MF_READ) ? GL_MAP_READ_BIT : 0;
	access |= (flags & MF_WRITE) ? GL_MAP_WRITE_BIT : 0;
	access |= (flags & MF_PERSISTENT) ? GL_MAP_PERSISTENT_BIT : 0;
	access |= (flags & MF_COHERENT) ? GL_MAP_COHERENT_BIT : 0;
	access |= (flags & MF_INVALIDATERANGE) ? GL_MAP_INVALIDATE_RANGE_BIT : 0;
	access |= (flags & MF_INVALIDATEALL) ? GL_MAP_INVALIDATE_BUFFER_BIT : 0;
	access |= (flags & MF_UNSYNCHRONIZED) ? GL_MAP_UNSYNCHRONIZED_BIT : 0;
	return glMapBufferRange(m_bindingTarget, static_cast<GLintptr>(offset), static_cast<GLsizeiptr>(length), access);
}

void GL4GraphicsMemory::unpin()
{
	glBindBuffer(m_bindingTarget, m_obj);
	glUnmapBuffer(m_bindingTarget);
}

void GL4GraphicsMemory::updateData(ptrdiff_t offset, size_t length, const void* data)
{
	m_dispatcher->invalidateConstantsMemory(0);
	glBindBuffer(m_bindingTarget, m_obj);
	glBufferSubData(m_bindingTarget, static_cast<GLintptr>(offset), static_cast<GLsizeiptr>(length), data);
}

size_t GL4GraphicsMemory::getCurrentSize() const
{
	return m_currentSize;
}

void GL4GraphicsMemory::release()
{
	if (!empty()) {
		glDeleteBuffers(1, &m_obj);
	}
	m_obj = 0;
}

} // namespace rendercore