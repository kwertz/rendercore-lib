//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-shader.hpp>

#include <cassert>
#include <vector>

namespace rendercore {

GL4ShaderBuildException::GL4ShaderBuildException(const std::string& log)
	: std::runtime_error("Shader build error: " + log), m_log(log)
{}

std::string GL4ShaderBuildException::getBuildLog() const
{
	return m_log;
}

GL4Shader::GL4Shader()
	: m_obj(0)
{}

GL4Shader::GL4Shader(const ShaderCreateInfo& createInfo)
{
	assert(createInfo.code != nullptr);
	assert(createInfo.codeSize > 0);
	assert(createInfo.lang == ShadingLanguage::GLSL);
	GLenum shaderType;
	switch (createInfo.type) {
		case ShaderType::Compute:
			shaderType = GL_COMPUTE_SHADER;
			break;
		case ShaderType::Vertex:
			shaderType = GL_VERTEX_SHADER;
			break;
		case ShaderType::TessCtl:
			shaderType = GL_TESS_CONTROL_SHADER;
			break;
		case ShaderType::TessEval:
			shaderType = GL_TESS_EVALUATION_SHADER;
			break;
		case ShaderType::Geometry:
			shaderType = GL_GEOMETRY_SHADER;
			break;
		case ShaderType::Fragment:
			shaderType = GL_FRAGMENT_SHADER;
			break;
		default:
			assert(0);
			break;
	}
	m_obj = glCreateShader(shaderType);
	GLchar* shaderStr = (GLchar*)createInfo.code;
	GLint length = static_cast<GLint>(createInfo.codeSize - 1); // exclude null terminator
	glShaderSource(m_obj, 1, &shaderStr, &length);

	glCompileShader(m_obj);

	GLint status;
	glGetShaderiv(m_obj, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLength; // includes the null terminator
		glGetShaderiv(m_obj, GL_INFO_LOG_LENGTH, &infoLogLength);

		std::vector<char> infoLog(infoLogLength);
		glGetShaderInfoLog(m_obj, infoLogLength, nullptr, infoLog.data());

		std::string infoLogStr(infoLog.begin(), infoLog.end());
		release();
		throw GL4ShaderBuildException(infoLogStr);
	}
}

GL4Shader::~GL4Shader()
{
	release();
}

GL4Shader::GL4Shader(GL4Shader&& other)
	: m_obj(other.m_obj)
{
	other.m_obj = 0;
}

GL4Shader& GL4Shader::operator=(GL4Shader&& other)
{
	if (&other != this) {
		m_obj = other.m_obj;
		other.m_obj = 0;
	}
	return *this;
}

void GL4Shader::release()
{
	if (!empty()) {
		glDeleteShader(m_obj);
	}
	m_obj = 0;
}

} // namespace rendercore