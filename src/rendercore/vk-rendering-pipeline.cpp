//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-rendering-pipeline.hpp>

#include <vector>
#include <rendercore/vk-descriptor-set.hpp>

namespace rendercore {

VulkanRenderingPipeline::VulkanRenderingPipeline()
	: m_device(VK_NULL_HANDLE), m_vk(nullptr), m_pipeline(VK_NULL_HANDLE)
{}

VulkanRenderingPipeline::VulkanRenderingPipeline(VkDevice device, VulkanFunctions& vk, const RenderingPipelineCreateInfo& createInfo)
	: m_device(device), m_vk(&vk), m_pipeline(VK_NULL_HANDLE)
{}

VulkanRenderingPipeline::~VulkanRenderingPipeline()
{
	release();
}

VulkanRenderingPipeline::VulkanRenderingPipeline(VulkanRenderingPipeline&& other)
	: m_device(other.m_device), m_vk(other.m_vk), m_pipeline(other.m_pipeline)
{
	other.m_pipeline = VK_NULL_HANDLE;
}

VulkanRenderingPipeline& VulkanRenderingPipeline::operator=(VulkanRenderingPipeline&& other)
{
	if (this != &other) {
		m_device = other.m_device;
		m_vk = other.m_vk;
		m_pipeline = other.m_pipeline;
		other.m_pipeline = VK_NULL_HANDLE;
	}
	return *this;
}

void VulkanRenderingPipeline::release()
{
	if (m_pipeline != VK_NULL_HANDLE) {
		m_vk->DestroyPipeline(m_device, m_pipeline, nullptr);
		m_pipeline = VK_NULL_HANDLE;
	}
}

VulkanPipelineLayout::VulkanPipelineLayout()
	: m_device(VK_NULL_HANDLE), m_vk(nullptr),
	  m_layout(VK_NULL_HANDLE)
{}

VulkanPipelineLayout::VulkanPipelineLayout(VkDevice device, VulkanFunctions& vk, const PipelineLayoutCreateInfo& createInfo)
	: m_device(device), m_vk(&vk), m_layout(VK_NULL_HANDLE)
{
	VkPipelineLayoutCreateInfo vkCreateInfo;
	vkCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	vkCreateInfo.pNext = nullptr;
	vkCreateInfo.flags = 0;
	vkCreateInfo.setLayoutCount = createInfo.setLayoutCount;

	std::vector<VkDescriptorSetLayout> setLayouts(createInfo.setLayoutCount);
	for (uint32_t i = 0; i < createInfo.setLayoutCount; ++i) {
		auto setLayout = static_cast<const VulkanDescriptorSetLayout*>(createInfo.setLayouts[i]);
		setLayouts[i] = setLayout->m_layout;
	}

	vkCreateInfo.pSetLayouts = setLayouts.data();
	vkCreateInfo.pushConstantRangeCount = 0;
	vkCreateInfo.pPushConstantRanges = nullptr;

	vk.CreatePipelineLayout(device, &vkCreateInfo, nullptr, &m_layout);
}

VulkanPipelineLayout::~VulkanPipelineLayout()
{
	// TODO: release resource
}

VulkanPipelineLayout::VulkanPipelineLayout(VulkanPipelineLayout&& other)
{

}

VulkanPipelineLayout& VulkanPipelineLayout::operator=(VulkanPipelineLayout&& other)
{
	return *this;
}

} // namespace rendercore
