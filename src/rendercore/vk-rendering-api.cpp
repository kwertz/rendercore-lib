//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/vk-rendering-api.hpp>

#include <stdexcept>
#include <vector>
#include <cassert>
#include <cstring>
#include <array>
#include <sstream>
#include <rendercore/vk-platform.hpp>
#include <rendercore/vk-functions.hpp>
#include <glslang/Public/ShaderLang.h>

namespace rendercore {

static int s_vkUseCount = 0;

VulkanRenderingAPI::VulkanRenderingAPI(const RenderingAPICreateInfo& createInfo)
	: m_cleanup(*this), // fine since we're not calling any members
	  m_functions(), m_validationFound(false),
	  m_instance(VK_NULL_HANDLE), m_debugReportCallback(VK_NULL_HANDLE),
	  m_device(VK_NULL_HANDLE), m_defaultOutputTarget(), m_swapchain()
{
	if (s_vkUseCount++ == 0) {
		initializeVulkan();
		if (!isVulkanSupported()) {
			throw APICreationException("Vulkan is not supported by this system");
		}
		glslang::InitializeProcess();
	}
	loadVulkanInstanceProcs(m_functions, VK_NULL_HANDLE);
	initializeVulkanInstance(createInfo);
	initializeVulkanDevice(createInfo);
}

VulkanRenderingAPI::~VulkanRenderingAPI()
{}

int VulkanRenderingAPI::getPropertyInteger(RenderingAPIProperty which)
{
	throw std::logic_error("Not implemented");
}

RenderQueue* VulkanRenderingAPI::getRenderQueue(RenderQueueType type)
{
	throw std::logic_error("Not implemented");
}

RenderOutputTarget* VulkanRenderingAPI::getDefaultRenderOutputTarget()
{
	return &m_defaultOutputTarget;
}

Swapchain* VulkanRenderingAPI::getSwapchain()
{
	return m_swapchain.get();
}

RenderStorage* VulkanRenderingAPI::createRenderStorage()
{
	throw std::logic_error("Not implemented");
}

GraphicsMemory* VulkanRenderingAPI::createVertexMemory(bool dynamic)
{
	// TODO: implement memory pooling
	return new VulkanGraphicsMemory(m_device, m_functions, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, m_memProps);
}

GraphicsMemory* VulkanRenderingAPI::createIndexMemory(bool dynamic)
{
	return new VulkanGraphicsMemory(m_device, m_functions, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, m_memProps);
}

GraphicsMemory* VulkanRenderingAPI::createShaderConstantsMemory()
{
	return new VulkanGraphicsMemory(m_device, m_functions, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, m_memProps);
}

Shader* VulkanRenderingAPI::createShader(const ShaderCreateInfo& createInfo)
{
	return new VulkanShader(m_device, m_functions, createInfo);
}

RenderingPipeline* VulkanRenderingAPI::createRenderingPipeline(const RenderingPipelineCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

VertexStream* VulkanRenderingAPI::createVertexStream(const VertexStreamCreateInfo& createInfo)
{
	return new VulkanVertexStream(createInfo);
}

Sampler* VulkanRenderingAPI::createSampler(const SamplerCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

Texture* VulkanRenderingAPI::createTexture(const TextureCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

TextureSet* VulkanRenderingAPI::createTextureSet(const TextureSetCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

RenderOutputBuffer* VulkanRenderingAPI::createRenderOutputBuffer(const RenderOutputBufferCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

RenderOutputTarget* VulkanRenderingAPI::createRenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

DescriptorSetLayout* VulkanRenderingAPI::createDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& createInfo)
{
	return new VulkanDescriptorSetLayout(m_device, m_functions, createInfo);
}

PipelineLayout*VulkanRenderingAPI::createPipelineLayout(const PipelineLayoutCreateInfo& createInfo)
{
	return new VulkanPipelineLayout(m_device, m_functions, createInfo);
}

DescriptorPool* VulkanRenderingAPI::createDescriptorPool(const DescriptorPoolCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

RenderPass* VulkanRenderingAPI::createRenderPass(const RenderPassCreateInfo& createInfo)
{
	throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseRenderStorage(RenderStorage* storage)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseVertexMemory(GraphicsMemory* gfxMemory)
{
	delete static_cast<VulkanGraphicsMemory*>(gfxMemory);
}

void VulkanRenderingAPI::releaseIndexMemory(GraphicsMemory* gfxMemory)
{
	delete static_cast<VulkanGraphicsMemory*>(gfxMemory);
}

void VulkanRenderingAPI::releaseShaderConstantsMemory(GraphicsMemory* gfxMemory)
{
	delete static_cast<VulkanGraphicsMemory*>(gfxMemory);
}

void VulkanRenderingAPI::releaseShader(Shader* shader)
{
	delete static_cast<VulkanShader*>(shader);
}

void VulkanRenderingAPI::releaseRenderingPipeline(RenderingPipeline* pipeline)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseVertexStream(VertexStream* stream)
{
	delete static_cast<VulkanVertexStream*>(stream);
}

void VulkanRenderingAPI::releaseSampler(Sampler* sampler)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseTexture(Texture* texture)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseTextureSet(TextureSet* textureSet)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseRenderOutputBuffer(RenderOutputBuffer* buffer)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseRenderOutputTarget(RenderOutputTarget* target)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseDescriptorSetLayout(DescriptorSetLayout* layout)
{
	delete static_cast<VulkanDescriptorSetLayout*>(layout);
}

void VulkanRenderingAPI::releasePipelineLayout(PipelineLayout* layout)
{
	delete static_cast<VulkanPipelineLayout*>(layout);
}

void VulkanRenderingAPI::releaseDescriptorPool(DescriptorPool* pool)
{
	//throw std::logic_error("Not implemented");
}

void VulkanRenderingAPI::releaseRenderPass(RenderPass* pass)
{
	//throw std::logic_error("Not implemented");
}

static bool checkInstanceLayers(uint32_t checkCount, const char* const* checkNames, uint32_t layerCount, VkLayerProperties* layers)
{
	for (uint32_t i = 0; i < checkCount; ++i) {
		bool found = false;
		for (uint32_t j = 0; j < layerCount; ++j) {
			if (std::strcmp(checkNames[i], layers[j].layerName) == 0) {
				found = true;
				break;
			}
		}
		if (!found) {
			return false;
		}
	}
	return true;
}

static std::array<const char*, 1> kInstanceValidationLayerNames = {{ "VK_LAYER_LUNARG_standard_validation" }};

void VulkanRenderingAPI::initializeVulkanInstance(const RenderingAPICreateInfo& createInfo)
{
	VulkanFunctions& vk = m_functions;

	// enable validation if error checking is requested
	if (createInfo.enableErrorChecking) {
		uint32_t propCount;
		VKCHECK(vk.EnumerateInstanceLayerProperties(&propCount, nullptr));

		std::vector<VkLayerProperties> instanceLayerProps(propCount);
		VKCHECK(vk.EnumerateInstanceLayerProperties(&propCount, instanceLayerProps.data()));
		m_validationFound = checkInstanceLayers((uint32_t)kInstanceValidationLayerNames.size(), kInstanceValidationLayerNames.data(), propCount, instanceLayerProps.data());

		if (!m_validationFound) {
			outputDebugMessage("Error checking was requested, but Vulkan validation layers weren't found. Validation cannot be enabled.\n");
		}
	}

	std::vector<const char*> instanceExts;
	getRequiredInstanceExtensions(instanceExts);

	if (m_validationFound) {
		if (isDebugReportExtensionPresent()) {
			instanceExts.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		} else {
			outputDebugMessage("Error checking was requested, but the Vulkan debug report extension is not present. Validation cannot be enabled.\n");
			m_validationFound = false;
		}
	}

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pNext = nullptr;
	appInfo.pApplicationName = createInfo.appInfo.applicationName.c_str();
	appInfo.applicationVersion = createInfo.appInfo.applicationVersion;
	appInfo.pEngineName = createInfo.appInfo.engineName.c_str();
	appInfo.engineVersion = createInfo.appInfo.engineVersion;
	appInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo instanceCreateInfo = {};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pNext = nullptr;
	instanceCreateInfo.flags = 0;
	instanceCreateInfo.pApplicationInfo = &appInfo;
	if (createInfo.enableErrorChecking) {
		instanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(kInstanceValidationLayerNames.size());
		instanceCreateInfo.ppEnabledLayerNames = kInstanceValidationLayerNames.data();
	}
	instanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(instanceExts.size());
	instanceCreateInfo.ppEnabledExtensionNames = instanceExts.data();

	VkDebugReportCallbackCreateInfoEXT dbgCreateInfo = {};
	if (m_validationFound) {
		dbgCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
		dbgCreateInfo.pNext = nullptr;
		// TODO: get a logging callback from the client
		// *INDENT-OFF*
		dbgCreateInfo.pfnCallback = static_cast<PFN_vkDebugReportCallbackEXT>([](VkFlags msgFlags, VkDebugReportObjectTypeEXT objType, uint64_t srcObject, size_t location, int32_t msgCode, const char* pLayerPrefix, const char* pMsg, void* pUserData) -> VkBool32 {
			std::stringstream sstr;
			if (msgFlags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
				sstr << "ERROR: ";
			} else if (msgFlags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
				sstr << "WARNING: ";
			} else if (msgFlags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) {
				sstr << "PERFWARNING: ";
			} else if (msgFlags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) {
				sstr << "INFO: ";
			} else if (msgFlags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) {
				sstr << "DEBUG: ";
			}
			sstr << pLayerPrefix << " Code " << msgCode << " : " << pMsg << "\n";
			outputDebugMessage(sstr.str().c_str());
			return VK_FALSE;
		});
		// *INDENT-ON*
		dbgCreateInfo.pUserData = this;
		dbgCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT | VK_DEBUG_REPORT_INFORMATION_BIT_EXT | VK_DEBUG_REPORT_DEBUG_BIT_EXT;
		// use the debug callback during instance creation
		instanceCreateInfo.pNext = &dbgCreateInfo;
	}

	VKCHECK(vk.CreateInstance(&instanceCreateInfo, nullptr, &m_instance));

	// load Vulkan functions that are bound to an instance
	loadVulkanInstanceProcs(m_functions, m_instance);

	// set the debug report callback
	if (m_validationFound) {
		VKCHECK(vk.CreateDebugReportCallbackEXT(m_instance, &dbgCreateInfo, nullptr, &m_debugReportCallback));
	}
}

void VulkanRenderingAPI::initializeVulkanDevice(const RenderingAPICreateInfo& createInfo)
{
	VulkanFunctions& vk = m_functions;

	uint32_t gpuCount;
	VKCHECK(vk.EnumeratePhysicalDevices(m_instance, &gpuCount, nullptr));
	VkPhysicalDevice gpu;
	{
		std::vector<VkPhysicalDevice> gpus(gpuCount);
		VKCHECK(vk.EnumeratePhysicalDevices(m_instance, &gpuCount, gpus.data()));
		assert(gpuCount > 0);
		// FIXME: just use the first GPU for now
		gpu = gpus[0];
	}

	uint32_t deviceExtensionCount = 0;
	VKCHECK(vk.EnumerateDeviceExtensionProperties(gpu, nullptr, &deviceExtensionCount, nullptr));
	std::vector<const char*> enabledDeviceExtensions;
	bool swapchainExtFound = false;
	{
		std::vector<VkExtensionProperties> deviceExtensions(deviceExtensionCount);
		VKCHECK(vk.EnumerateDeviceExtensionProperties(gpu, nullptr, &deviceExtensionCount, deviceExtensions.data()));

		for (uint32_t i = 0; i < deviceExtensionCount; ++i) {
			if (std::strcmp(VK_KHR_SWAPCHAIN_EXTENSION_NAME, deviceExtensions[i].extensionName) == 0) {
				swapchainExtFound = true;
				enabledDeviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
			}
		}
	}

	if (!swapchainExtFound) {
		throw APICreationException("Swapchain extension not present");
	}

	uint32_t queueFamilyCount;
	vk.GetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);
	uint32_t suitableQueueFamilyIndex = 0xffffffff;
	{
		std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
		vk.GetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, queueFamilyProperties.data());
		for (uint32_t i = 0; i < queueFamilyCount; ++i) {
			if (queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				if (getPhysicalDevicePresentationSupport(m_instance, gpu, i)) {
					suitableQueueFamilyIndex = i;
					break;
				}
			}
		}
	}

	if (suitableQueueFamilyIndex == 0xffffffff) {
		throw APICreationException("Cannot find a suitable queue family");
	}

	vk.GetPhysicalDeviceMemoryProperties(gpu, &m_memProps);

	VkDeviceQueueCreateInfo queueCreateInfo = {};
	queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queueCreateInfo.pNext = nullptr;
	queueCreateInfo.flags = 0;
	queueCreateInfo.queueFamilyIndex = suitableQueueFamilyIndex;
	queueCreateInfo.queueCount = 1;
	float queuePriority = 0.0f;
	queueCreateInfo.pQueuePriorities = &queuePriority;

	VkDeviceCreateInfo deviceCreateInfo = {};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.pNext = nullptr;
	deviceCreateInfo.flags = 0;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
	if (m_validationFound) {
		// TODO: perform layer checking
		deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(kInstanceValidationLayerNames.size());
		deviceCreateInfo.ppEnabledLayerNames = kInstanceValidationLayerNames.data();
	}
	deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(enabledDeviceExtensions.size());
	deviceCreateInfo.ppEnabledExtensionNames = enabledDeviceExtensions.data();
	deviceCreateInfo.pEnabledFeatures = nullptr;

	VKCHECK(vk.CreateDevice(gpu, &deviceCreateInfo, nullptr, &m_device));

	loadVulkanDeviceProcs(m_functions, m_device);

	m_defaultOutputTarget = createDefaultOutputTargetVulkan(m_instance, m_device, m_functions, gpu, suitableQueueFamilyIndex, *createInfo.surface, createInfo.framebufferSettings, m_swapchain);
}

VulkanRenderingAPI::Cleanup::Cleanup(VulkanRenderingAPI& api) : api(api)
{}

VulkanRenderingAPI::Cleanup::~Cleanup()
{
	if (api.m_device != VK_NULL_HANDLE) {
		api.m_functions.DestroyDevice(api.m_device, nullptr);
	}
	if (api.m_debugReportCallback != VK_NULL_HANDLE) {
		api.m_functions.DestroyDebugReportCallbackEXT(api.m_instance, api.m_debugReportCallback, nullptr);
	}
	if (api.m_instance != VK_NULL_HANDLE) {
		api.m_functions.DestroyInstance(api.m_instance, nullptr);
	}
	if (--s_vkUseCount == 0) {
		glslang::FinalizeProcess();
		terminateVulkan();
	}
}

} // namespace rendercore
