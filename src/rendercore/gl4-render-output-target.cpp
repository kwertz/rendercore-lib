//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2015 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#include <rendercore/gl4-render-output-target.hpp>
#include <cassert>
#include <rendercore/gl4-render-output-buffer.hpp>
#include <rendercore/gl4-texture.hpp>
#include <rendercore/gl4-render-dispatcher.hpp>

namespace rendercore {

GL4FramebufferIncompleteException::GL4FramebufferIncompleteException()
	: std::runtime_error("Framebuffer is incomplete")
{}

GL4RenderOutputTarget::GL4RenderOutputTarget()
	: m_isDefaultFramebuffer(false), m_framebuffer(0), m_enableSRGBGammaCorrection(false)
{}

static GLenum translateRenderOutputAttachment(RenderOutputAttachmentType type, uint8_t index)
{
	switch (type) {
		case RenderOutputAttachmentType::ColorAttachment:
			return GL_COLOR_ATTACHMENT0 + index;
		case RenderOutputAttachmentType::DepthAttachment:
			return GL_DEPTH_ATTACHMENT;
		case RenderOutputAttachmentType::StencilAttachment:
			return GL_STENCIL_ATTACHMENT;
		case RenderOutputAttachmentType::DepthStencilAttachment:
			return GL_DEPTH_STENCIL_ATTACHMENT;
	}
	return GL_COLOR_ATTACHMENT0;
}

GL4RenderOutputTarget::GL4RenderOutputTarget(const RenderOutputTargetCreateInfo& createInfo, GL4RenderDispatcher& dispatcher)
	: m_isDefaultFramebuffer(false), m_framebuffer(0), m_enableSRGBGammaCorrection(createInfo.enableSRGBGammaCorrection)
{
	glGenFramebuffers(1, &m_framebuffer);
	dispatcher.invalidateRenderOutputTarget();
	glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
	for (uint32_t i = 0; i < createInfo.numBufferAttachmentInfos; ++i) {
		const RenderOutputBufferAttachmentInfo& attachmentInfo = createInfo.firstBufferAttachmentInfo[i];
		GLuint renderbufferName = static_cast<const GL4RenderOutputBuffer*>(attachmentInfo.buffer)->getRenderbuffer();
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, translateRenderOutputAttachment(attachmentInfo.type, attachmentInfo.colorAttachmentIndex), GL_RENDERBUFFER, renderbufferName);
	}
	for (uint32_t i = 0; i < createInfo.numTextureAttachmentInfos; ++i) {
		const TextureAttachmentInfo& attachmentInfo = createInfo.firstTextureAttachmentInfo[i];
		const GL4Texture* texture = static_cast<const GL4Texture*>(attachmentInfo.texture);
		GLuint textureName = texture->getTexture();
		GLenum bindingTarget = texture->getBindingTarget();
		GLenum attachmentPoint = translateRenderOutputAttachment(attachmentInfo.type, attachmentInfo.colorAttachmentIndex);
		switch (bindingTarget) {
			case GL_TEXTURE_1D: {
				glFramebufferTexture1D(GL_FRAMEBUFFER, attachmentPoint, GL_TEXTURE_1D, textureName, attachmentInfo.mipLevel);
				break;
			}
			case GL_TEXTURE_1D_ARRAY: {
				glFramebufferTextureLayer(GL_FRAMEBUFFER, attachmentPoint, textureName, attachmentInfo.mipLevel, attachmentInfo.layer);
				break;
			}
			case GL_TEXTURE_2D: {
				glFramebufferTexture2D(GL_FRAMEBUFFER, attachmentPoint, GL_TEXTURE_2D, textureName, attachmentInfo.mipLevel);
				break;
			}
			case GL_TEXTURE_2D_ARRAY: {
				glFramebufferTextureLayer(GL_FRAMEBUFFER, attachmentPoint, textureName, attachmentInfo.mipLevel, attachmentInfo.layer);
				break;
			}
			case GL_TEXTURE_3D: {
				glFramebufferTexture3D(GL_FRAMEBUFFER, attachmentPoint, GL_TEXTURE_3D, textureName, attachmentInfo.mipLevel, attachmentInfo.layer);
				break;
			}
			case GL_TEXTURE_CUBE_MAP: {
				glFramebufferTexture2D(GL_FRAMEBUFFER, attachmentPoint, translateCubemapFace(attachmentInfo.face), textureName, attachmentInfo.mipLevel);
				break;
			}
			case GL_TEXTURE_CUBE_MAP_ARRAY: {
				glFramebufferTexture3D(GL_FRAMEBUFFER, attachmentPoint, translateCubemapFace(attachmentInfo.face), textureName, attachmentInfo.mipLevel, attachmentInfo.layer);
				break;
			}
			default:
				assert(0);
				break;
		}
	}

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		release();
		throw GL4FramebufferIncompleteException();
	}
}

GL4RenderOutputTarget::GL4RenderOutputTarget(GL4DefaultFramebufferToken token)
	: m_isDefaultFramebuffer(true), m_framebuffer(0), m_enableSRGBGammaCorrection(token.srgb)
{}

GL4RenderOutputTarget::~GL4RenderOutputTarget()
{
	release();
}

GL4RenderOutputTarget::GL4RenderOutputTarget(GL4RenderOutputTarget&& other)
	: m_isDefaultFramebuffer(other.m_isDefaultFramebuffer), m_framebuffer(other.m_framebuffer),
	  m_enableSRGBGammaCorrection(other.m_enableSRGBGammaCorrection)
{
	other.m_isDefaultFramebuffer = false;
	other.m_framebuffer = 0;
}

GL4RenderOutputTarget& GL4RenderOutputTarget::operator=(GL4RenderOutputTarget&& other)
{
	if (&other != this) {
		m_isDefaultFramebuffer = other.m_isDefaultFramebuffer;
		m_framebuffer = other.m_framebuffer;
		m_enableSRGBGammaCorrection = other.m_enableSRGBGammaCorrection;
		other.m_isDefaultFramebuffer = false;
		other.m_framebuffer = 0;
	}
	return *this;
}

void GL4RenderOutputTarget::release()
{
	if (m_framebuffer != 0) {
		glDeleteFramebuffers(1, &m_framebuffer);
	}
	m_framebuffer = 0;
}

} // namespace rendercore