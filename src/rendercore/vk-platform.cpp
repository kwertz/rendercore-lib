//----------------------------------------------------------------------------
// This source code file is part of
//   RenderCore
// Copyright (c) 2016 - Niklas F. Pluem
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//----------------------------------------------------------------------------

#ifdef RENDERCORE_USE_PLATFORM_WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#endif
#ifdef RENDERCORE_USE_PLATFORM_XLIB
#define VK_USE_PLATFORM_XLIB_KHR
#endif

#include <rendercore/vk-platform.hpp>

#include <cassert>
#include <stdexcept>
#include <cstring>
#include <m0tools/shared-library.hpp>
#include <rendercore/platform-surface.hpp>

namespace rendercore {

struct VulkanInfo {
	m0tools::SharedLibrary library;
	PFN_vkGetInstanceProcAddr GetInstanceProcAddr;

	bool KHR_surface;
	bool KHR_win32_surface;
	bool KHR_xlib_surface;
	bool EXT_debug_report;
};
static VulkanInfo s_vkInfo = {};
#ifdef _WIN32
static const std::string kVkLibraryName = "vulkan-1.dll";
#else
static const std::string kVkLibraryName = "libvulkan.so.1";
#endif

void initializeVulkan()
{
	using namespace m0tools;

	if (s_vkInfo.library != nullptr) {
		return;
	}

	s_vkInfo.library = openSharedLibrary(kVkLibraryName, SHLL_LAZY_BIT);
	if (s_vkInfo.library == nullptr) {
		return;
	}

	s_vkInfo.GetInstanceProcAddr = function_cast<PFN_vkGetInstanceProcAddr>(getSharedLibrarySymbol(s_vkInfo.library, "vkGetInstanceProcAddr"));
	assert(s_vkInfo.GetInstanceProcAddr != nullptr);

	PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionPropertiesFn =
	    function_cast<PFN_vkEnumerateInstanceExtensionProperties>(getSharedLibrarySymbol(s_vkInfo.library, "vkEnumerateInstanceExtensionProperties"));
	assert(vkEnumerateInstanceExtensionPropertiesFn != nullptr);

	uint32_t instanceExtensionCount;
	VkResult err = vkEnumerateInstanceExtensionPropertiesFn(nullptr, &instanceExtensionCount, nullptr);
	if (err != VK_SUCCESS) {
		throw std::runtime_error("Vulkan: Querying the number of instance extensions failed");
	}

	std::vector<VkExtensionProperties> extensionProperties(instanceExtensionCount);
	err = vkEnumerateInstanceExtensionPropertiesFn(nullptr, &instanceExtensionCount, extensionProperties.data());
	if (err != VK_SUCCESS) {
		throw std::runtime_error("Vulkan: Enumerating instance extension properties failed");
	}

	for (uint32_t i = 0; i < instanceExtensionCount; ++i) {
		const VkExtensionProperties& prop = extensionProperties[i];
		if (std::strcmp(prop.extensionName, VK_KHR_SURFACE_EXTENSION_NAME) == 0) {
			s_vkInfo.KHR_surface = true;
		} else if (std::strcmp(prop.extensionName, "VK_KHR_win32_surface") == 0) {
			s_vkInfo.KHR_win32_surface = true;
		} else if (std::strcmp(prop.extensionName, "VK_KHR_xlib_surface") == 0) {
			s_vkInfo.KHR_xlib_surface = true;
		} else if (std::strcmp(prop.extensionName, VK_EXT_DEBUG_REPORT_EXTENSION_NAME) == 0) {
			s_vkInfo.EXT_debug_report = true;
		}
	}
}

void terminateVulkan()
{
	if (s_vkInfo.library != nullptr) {
		m0tools::closeSharedLibrary(s_vkInfo.library);
	}
}

bool isVulkanSupported()
{
	return s_vkInfo.library != nullptr;
}

void getRequiredInstanceExtensions(std::vector<const char*>& extensions)
{
	if (!isVulkanSupported()) {
		throw std::runtime_error("Vulkan: Vulkan support is not present");
	}

	if (s_vkInfo.KHR_surface) {
		extensions.push_back("VK_KHR_surface");
	}
	if (s_vkInfo.KHR_win32_surface) {
		extensions.push_back("VK_KHR_win32_surface");
	}
	if (s_vkInfo.KHR_xlib_surface) {
		extensions.push_back("VK_KHR_xlib_surface");
	}
}

bool isDebugReportExtensionPresent()
{
	return s_vkInfo.EXT_debug_report;
}

PFN_vkVoidFunction getInstanceProcAddress(VkInstance instance, const char* procname)
{
	if (!isVulkanSupported()) {
		throw std::runtime_error("Vulkan: Vulkan support is not present");
	}

	PFN_vkVoidFunction func = s_vkInfo.GetInstanceProcAddr(instance, procname);
	if (func == nullptr) {
		func = m0tools::function_cast<PFN_vkVoidFunction>(m0tools::getSharedLibrarySymbol(s_vkInfo.library, procname));
	}

	return func;
}

bool getPhysicalDevicePresentationSupport(VkInstance instance, VkPhysicalDevice device, uint32_t family)
{
	if (!isVulkanSupported()) {
		throw std::runtime_error("Vulkan: Vulkan support is not present");
	}

#ifdef RENDERCORE_USE_PLATFORM_WIN32
	PFN_vkGetPhysicalDeviceWin32PresentationSupportKHR vkGetPhysicalDeviceWin32PresentationSupportKHRFn =
	    reinterpret_cast<PFN_vkGetPhysicalDeviceWin32PresentationSupportKHR>(getInstanceProcAddress(instance, "vkGetPhysicalDeviceWin32PresentationSupportKHR"));
	if (vkGetPhysicalDeviceWin32PresentationSupportKHRFn == nullptr) {
		return false;
	}
	return vkGetPhysicalDeviceWin32PresentationSupportKHRFn(device, family) == VK_TRUE;
#else
	// FIXME: Xlib support
	return false;
#endif
}

VkResult createSurface(VkInstance instance, const PlatformSurfaceReference& platformSurface, const VkAllocationCallbacks* allocator, VkSurfaceKHR& surface)
{
	if (!isVulkanSupported()) {
		throw std::runtime_error("Vulkan: Vulkan support is not present");
	}

#ifdef RENDERCORE_USE_PLATFORM_WIN32
	PFN_vkCreateWin32SurfaceKHR vkCreateWin32SurfaceKHRFn = reinterpret_cast<PFN_vkCreateWin32SurfaceKHR>(getInstanceProcAddress(instance, "vkCreateWin32SurfaceKHR"));
	if (vkCreateWin32SurfaceKHRFn == nullptr) {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}

	VkWin32SurfaceCreateInfoKHR surfCreateInfo = {};
	surfCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surfCreateInfo.pNext = nullptr;
	surfCreateInfo.flags = 0;
	surfCreateInfo.hinstance = platformSurface.win32.hinstance;
	surfCreateInfo.hwnd = platformSurface.win32.hwnd;

	return vkCreateWin32SurfaceKHRFn(instance, &surfCreateInfo, allocator, &surface);
#else
	// FIXME: Xlib support
	return VK_ERROR_FEATURE_NOT_PRESENT;
#endif
}

} // namespace rendercore
